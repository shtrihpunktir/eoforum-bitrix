<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Схема выставки - EOF 2019");
?>

<div class="main">
    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="/">Главная</a></li>
                <li>Схема выставки</li>
            </ul>
        </div>
        <h1>Схема выставки</h1>
        <div class="exhibition">
            <div class="map">
                <img src="/assets/exhibition-scheme.jpg"/>
                <br><br>
                <p><a target="_blank" download href="/assets/exhibition-scheme.pdf" class="btn">Скачать</a></p>
            </div>
            <div class="about-section">
                <h1>Экспоненты</h1>
                <div class="participants">
                    <p><b>B3-B4</b> Double Medical Technology Inc.</p>
                    <p><b>C4</b> Madison Ortho</p>
                    <p><b>C5</b> Current Medical Technology</p>
                    <p><b>C6</b> Association of Managers of Healthcare Organizations</p>
                    <p><b>С9</b> NIKAMED</p>
                    <p><b>С11</b> LINK Orthopedic East</p>
                    <p><b>C12</b> B. Braun Medical Inc</p>
                    <p><b>С13</b> Neurocor</p>
                    <p><b>C14</b> Solopharm</p>
                    <p><b>С18</b> CSC</p>
                    <p><b>С21</b> IRE-Polus Ltd</p>
                    <p><b>С23</b> Jiangsu BaiDe Medical Instrument Co.,Ltd.</p>
                    <p><b>D2</b> Getinge</p>
                    <p><b>D3</b> RegenLab</p>
                    <p><b>D4</b> TraumaBooks, diagnost-book , medbook</p>
                    <p><b>D5</b> KARL STORZ</p>
                    <p><b>D6-D7</b> Zimmer Biomet</p>
                    <p><b>D8</b> Rompharm Company</p>
                    <p><b>D9-D11</b> Johnson&Johnson</p>
                    <p><b>D12</b> Stryker</p>
                    <p><b>D13</b> BEKA RUS</p>
                    <p><b>D14</b> Sanatmetal Ltd</p>
                    <p><b>D15-D18, E29-30</b> Osteomed</p>
                    <p><b>D19</b> Current Medical Technology</p>
                    <p><b>D27-D28</b> Smith&Nephew</p>
                    <p><b>D29-D30</b> ckr, TITANMED, Shanhai ligatech, Bodjin</p>
                    <p><b>D33</b> Multi-Systems Technology</p>
                    <p><b>D34</b> ChM</p>
                    <p><b>E1</b> Meril Life Sciences Pvt. Ltd</p>
                    <p><b>E2</b> ЕВРОТЕХ</p>
                    <p><b>E5</b> Sante Medical Systems</p>
                    <p><b>E6</b> Ministry of Education of the Republic of Belarus</p>
                    <p><b>E7</b> UNI DOCS</p>
                    <p><b>E8</b> INTERCUS PLUS</p>
                    <p><b>Е11</b> Lepu Medical Technology Co., Ltd.</p>
                    <p><b>E12</b> ConMed</p>
                    <p><b>E15</b> Advanced Medical Technology Education Center</p>
                    <p><b>E16</b> Limited liability company R-Optics</p>
                    <p><b>E17</b> BIOTEHNOS</p>
                    <p><b>E18</b> NPP IMPLANT</p>
                    <p><b>E19</b> implantcast</p>
                    <p><b>E20</b> RusVisc</p>
                    <p><b>E21</b> Changzhou Hengjie Medical Devices Co., Ltd</p>
                    <p><b>E22</b> Boehringer Ingelheim</p>
                    <p><b>E23</b> LOGEEKS MS</p>
                    <p><b>E24</b> Intermedapatit</p>
                    <p><b>E25</b> ZDRAVMEDTEKH</p>
                    <p><b>Е26</b> DIMED</p>
                    <p><b>E27</b> MEDPLANT</p>
                    <p><b>E28</b> S.P.HELPIC</p>
                    <p><b>F1</b> Research Center BIOFORM</p>
                    <p><b>F2</b> ASSOCIATION of AEROSPASE ENGINEERS</p>
                    <p><b>F3</b> YAR-TEZ</p>
                    <p><b>F4</b> Orphan group</p>
                    <p><b>F5</b> Orthopedic Medical Company</p>
                    <p><b>F6</b> ConvaTec</p>
                    <p><b>F7</b> mediCAD Hectec</p>
                    <p><b>F8</b> Lilly Farma</p>
                    <p><b>F10</b> 3B Scientific</p>
                    <p><b>F11</b> «Innovative Technology Company Endoprint»</p>
                    <p><b>F12</b> INTERNATIONAL ACADEMY OF MEDICAL EDUCATION</p>
                    <p><b>F13</b> Research and Development enterprise «Orbita»</p>
                    <p><b>F14</b> CML AT Medical</p>
                    <p><b>F15</b> 3М Russia</p>
                    <p><b>G1</b> IronMen</p>
                    <p><b>G3-G4</b> COLLECTIVE EXPOSITION UNDER THE AUTHORITY OF THE MINISTRY OF INDUSTRY AND TRADE OF THE RUSSIAN FEDERATION</p>
                    <p><b>G6</b> Medical Trading Company</p>
                    <p><b>H1</b> Ortorent</p>
                    <p><b>H2</b> Arthrex</p>
                    <p><b>H3</b> Relaxsan</p>
                    <p><b>H5</b> Medservice</p>
                    <p><b>H6</b> NEVZ-CERAMICS</p>
                    <p><b>H8</b> MedIntegro</p>
                    <p><b>H9</b> Matopat</p>
                    <p><b>H13</b> MCNT</p>
                    <p><b>H15</b> BTL</p>
                    <p><b>H18</b> "MEDSIN-MONIKI"</p>
                    <p><b>H19</b> Art-Medica-M LLC</p>
                    <p><b>H20</b> ИФ АБ Универсал, Современные технологические линии</p>

                </div>
            </div>
            <div style="display: none;" class="about-section">
                <h1>Реклама от спонсоров</h1>
                <div class="sponsors-gallery">
                    <div class="main-frame">
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/d888eje-ca8494ef-85c8-49fa-bfc6-b7fd65590a38.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/d888eje-ca8494ef-85c8-49fa-bfc6-b7fd65590a38.jpg"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/d888eoe-77823bd5-8ded-4463-a512-abf498c56482.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/d888eoe-77823bd5-8ded-4463-a512-abf498c56482.jpg"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/before_the_field_of_fire_by_elegantwaster_d5ur1ga-fullview.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/before_the_field_of_fire_by_elegantwaster_d5ur1ga-fullview.jpg"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg"></a>
                            </div>
                        </div>

                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/d888eje-ca8494ef-85c8-49fa-bfc6-b7fd65590a38.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/d888eje-ca8494ef-85c8-49fa-bfc6-b7fd65590a38.jpg"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/d888eoe-77823bd5-8ded-4463-a512-abf498c56482.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/d888eoe-77823bd5-8ded-4463-a512-abf498c56482.jpg"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/before_the_field_of_fire_by_elegantwaster_d5ur1ga-fullview.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/before_the_field_of_fire_by_elegantwaster_d5ur1ga-fullview.jpg"></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desktop-view">
                                <a href="#"><img
                                            src="assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg"></a>
                            </div>
                            <div class="mobile-view">
                                <a href="#"><img
                                            src="assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg"></a>
                            </div>
                        </div>
                    </div>
                    <div class="control-nav">
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/d888eje-ca8494ef-85c8-49fa-bfc6-b7fd65590a38.jpg)"></span>
                        </div>
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/d888eoe-77823bd5-8ded-4463-a512-abf498c56482.jpg)"></span>
                        </div>
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg)"></span>
                        </div>
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/before_the_field_of_fire_by_elegantwaster_d5ur1ga-fullview.jpg)"></span>
                        </div>
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg)"></span>
                        </div>

                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/d888eje-ca8494ef-85c8-49fa-bfc6-b7fd65590a38.jpg)"></span>
                        </div>
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/d888eoe-77823bd5-8ded-4463-a512-abf498c56482.jpg)"></span>
                        </div>
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg)"></span>
                        </div>
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/before_the_field_of_fire_by_elegantwaster_d5ur1ga-fullview.jpg)"></span>
                        </div>
                        <div class="item">
                            <span class="thumb"
                                  style="background-image: url(assets/images/d888e65-27c1db90-02db-4bab-b1d9-494a67b82c4a.jpg)"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-person">
                <div class="person-block person-block-lg">
                    <div class="photo">
                        <span class="image"
                              style="background-image: url(http://194.177.20.239/upload/iblock/959/959cb59a40578cc14fee64d01d6fcd2b.png)"></span>
                    </div>
                    <h3>Алина Сарайкина</h3>
                    <div class="position">Работа с партнерами и спонсорами
                        <br>
                        <br>
                        saraikina@polylog.ru
                        <br>
                        <br>
                        +7 (916) 601-08-69
                    </div>
                    <div class="person-footer">
                        <div class="contact"><a href="mailto:saraikina@polylog.ru" class="btn btn-white">Написать</a>
                        </div>
                        <div class="contact"><a href="tel:+79166010869" class="btn btn-white-outline">Позвонить</a>
                        </div>
                    </div>
                </div>
                <!--<div class="person-block person-block-lg">
                    <div class="photo">
                        <span class="image"
                              style="background-image: url(http://194.177.20.239/upload/iblock/959/959cb59a40578cc14fee64d01d6fcd2b.png)"></span>
                    </div>
                    <h3>Евгения Лупачева</h3>
                    <div class="position">По вопросам участия в выставке
                        <br>
                        <br>
                        lupacheva@polylog.ru
                        <br>
                        <br>
                        +7 (964) 435-59-95
                    </div>
                    <div class="person-footer">
                        <div class="contact"><a href="mailto:lupacheva@polylog.ru" class="btn btn-white">Написать</a></div>
                        <div class="contact"><a href="tel:+79644355995" class="btn btn-white-outline">Позвонить</a></div>
                    </div>
                </div>
                <div class="person-block person-block-lg">
                    <div class="photo">
                        <span class="image"
                              style="background-image: url(http://194.177.20.239/upload/iblock/959/959cb59a40578cc14fee64d01d6fcd2b.png)"></span>
                    </div>
                    <h3>Екатерина Маненкова</h3>
                    <div class="position">По вопросам информационного партнерства
                    <br>
                    <br>
                        press@eoforum.ru
                    <br>
                    <br>
                        +7 (921) 007-26-73
                    </div>
                    <div class="person-footer">
                        <div class="contact"><a href="mailto:press@eoforum.ru" class="btn btn-white">Написать</a></div>
                        <div class="contact"><a href="tel:+79210072673" class="btn btn-white-outline">Позвонить</a></div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
