<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;
global $USER;
$APPLICATION->SetTitle("EOF 2019");


?>

    <div class="container-page main-page">
        <header class="header">
            <div class="custom-container">
                <div class="box-wrapper">
                    <div class="logo">
                        <a href="#head-section"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/logo.svg" alt="logo"></a>
                    </div>
                    <div class="nav-wrapper">
                        <nav>
                            <ul class="nav">
                                <li><a href="/about/">О форуме</a></li>
                                <li><a href="#events-section">Программа</a></li>
                                <li><a href="#news-section">Новости</a></li>
                                <li><a href="#information-participants-section">Участникам</a></li>
                                <li><a href="#exhibition-section">Выставка</a></li>
                                <li><a href="#media-section">Медиа</a></li>
                                <li><a href="#contacts-section">Контакты</a></li>
                                <li><a href="/en/"><i class="fas fa-globe-americas"></i>&nbsp;Eng</a></li>
                            </ul>
                        </nav>
                        <div class="user-panel">
                            <div class="btns-group">
                                <? if (!$USER->IsAuthorized()) { ?>
                                    <a href="/login/">Вход</a>
                                    <a href="/signup/?type=speaker">Спикерам</a>
                                <? } else { ?>
                                    <a href="/profile/">Профиль</a>
                                <? } ?>
                            </div>
                            <? if ($USER->IsAuthorized()) { ?>
                                <!--<div class="avatar">
                                        <div class="img">
                                            <img
                                                <? /* if ($USER->IsAuthorized()){ */ ?>title="Вы вошли как <? /*= $USER->GetFullName() */ ?>" <? /* }; */ ?>
                                                src="<? /*= SITE_TEMPLATE_PATH */ ?>/assets/images/avatar.png" alt="avatar">
                                        </div>
                                    </div>-->
                            <? }; ?>
                        </div>
                    </div>
                    <button class="open-mobile-menu"></button>
                </div>
            </div>
        </header>

        <div class="main-slider">
            <div class="section welcome-section first-slide">
                <div class="welcome-content">
                    <div class="custom-container">
                        <div class="wrapper-info">
                            <div class="address"><span>экспоцентр</span></div>
                            <div class="date">28-29 июня 2019</div>
                        </div>
                        <div class="logo-side">
                            <div class="logo">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/ws_logo.png" alt="logo">
                            </div>
                        </div>
                        <div class="info-side">
                            <h1 class="title">
                          <span>
                              евразийский ортопедический форум <span>eurasian orthopedic forum</span>
                          </span>
                            </h1>
                            <div class="tagline">объединяя знания</div>
                            <div class="btns-group">
                                <a class="buy-ticket" href="#information-participants-section">Купить билет</a>
                                <a class="about-forum" href="/about/">о eof 2019</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section welcome-section second-slide">
                <div class="welcome-content">
                    <div class="custom-container">
                        <div class="wrapper-info">
                            <div class="address"><span>экспоцентр</span></div>
                            <div class="date">28-29 июня 2019</div>
                        </div>
                        <div class="logo-side">
                            <div class="logo">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/ws_second-logo.png" alt="logo">
                            </div>
                        </div>
                        <div class="info-side">
                            <h1 class="title">
                <span>
                  вертебрологический</br> конгресс</br> brics spine summit
                </span>
                            </h1>
                            <div class="btns-group">
                                <a class="buy-ticket" href="#information-participants-section">Купить билет</a>
                                <a class="about-forum" href="/about/">о eof 2019</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="events-section" class="section events-section">
            <div class="custom-container">
                <div class="title-section">Программа мероприятия</div>

                <div class="events-list">
                    <div class="events-item science-program">
                        <div class="wrapper">
                            <h2 class="name">научная программа</h2>
                            <div class="desc">
                                5 международных конгрессов<br><br>
                                Уникальный образовательный курс<br><br>
                                12 тематических секций :<br><br>
                                <ul>
                                    <li>Эндопротезирование</li>
                                    <li>Травма верхней и нижней конечности</li>
                                    <li>Детская травматология и ортопедия</li>
                                    <li>Реконструктивно-восстановительная и пластическая хирургия</li>
                                    <li>Медицина катастроф и лечение раненых</li>
                                    <li>Онкология и заболевания костной ткани</li>
                                    <li>Анестезиология и реаниматология в травматологии и ортопедии</li>
                                    <li>Мультидисциплинарный подход в реабилитации</li>
                                    <li>Технологии 3D-печати в медицине</li>
                                </ul>
                            </div>
                        </div>

                        <div class="btns-group">
                            <a class="transparent" href="/science-programm/">Расписание</a>
                            <a class="white" href="#information-participants-section">Купить билет</a>
                        </div>
                    </div>

                    <div class="events-item business-program">
                        <div class="wrapper">
                            <h2 class="name">Программа Бизнес-форума</h2>
                            <div class="desc">
                                "Факторы роста медицинского рынка"<br><br>
                                Промышленный конгресс:<br><br>
                                <ul>
                                    <li>Развитие медицинской промышленности</li>
                                    <li>Финансирование инновационных разработок</li>
                                    <li>Регулирование обращения медицинских изделий в ЕАЭС</li>
                                </ul>
                                <br><br>Практические мероприятия:<br><br>
                                <ul>
                                    <li>Юридические консультации для практикующих врачей</li>
                                    <li>Мастер-класс по созданию личного бренда врача</li>
                                    <li>Презентация цифровых технологий для медицины</li>
                                    <li>Заседания профильных комитетов и комиссий федеральных органов власти</li>
                                </ul>
                            </div>
                        </div>

                        <div class="btns-group">
                            <a class="transparent" href="/business-programm/">Расписание</a>
                            <a class="white" href="#information-participants-section">Купить билет</a>
                        </div>
                    </div>

                    <!-- <div class="events-item submit-report">
                        <div class="wrapper">
                            <h2 class="name">Подать заявку на выступление</h2>
                            <div class="desc">
                                Подать заявку на выступление с докладом в рамках научной программы ЕОФ<br><br>

                                Оргкомитет ЕОФ рассмотрит заявку и сообщит решение о включении доклада в программу. <br><br>
                                Прием заявок завершается 13 мая 2019 года.
                            </div>
                        </div>

                        <div class="btns-group">
                             <a class="white" href="/.registration-of-speakers/">Подать заявку</a>

                        </div>
                     </div>-->
                </div>
            </div>
        </div>
        <div id="news-section" class="section news-section">
            <div class="custom-container">
                <div class="title-section">Новости</div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "eof-news-main",
                    array(
                        "COMPONENT_TEMPLATE" => "eof-news-inner",
                        "IBLOCK_TYPE" => "news",
                        "IBLOCK_ID" => "1",
                        "NEWS_COUNT" => "8",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "STRICT_SECTION_CHECK" => "N",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "PAGER_TEMPLATE" => "visual",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => ""
                    ),
                    false
                ); ?>

                <div class="btn-more-news">
                    <a href="/news/">Больше новостей</a>
                </div>
            </div>
        </div>
        <div id="information-participants-section" class="section information-participants-section">
            <div class="custom-container">
                <div class="title-section">Участникам</div>

                <div class="price-list">
                    <div class="price-item student">
                        <div class="price student">
                            <div class="name">STUDENT</div>
                            <div class="cost">1 000 ₽</div>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Доступ на выставку EOF 2019</li>
                                <li>Доступ на все мероприятия научной программы и бизнес-форума<br><em>(кроме
                                        образовательного курса и пленарного заседания)</em></li>
                                <li>Пакет участника</li>
                                <li>Доступ к электронной библиотеке материалов по итогам EOF 2019</li>
                            </ul>
                            <div class="wrapper">
                                <div class="signature" style="font-size: 12px;">
                                    Организатор оставляет за собой право на отказ во входе в случае не предоставления документа, удостоверяющего статус студента
                                </div>

                                <?php
                                $APPLICATION->IncludeComponent(
                                    "eoforum:subscription.buy",
                                    ".default",
                                    array(
                                        "REGISTER_FORM_URL" => "/local/api/registration-form.php",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "SUBSCRIPTION_ID" => "56"
                                    ),
                                    false
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="price-item standart">
                        <div class="price standart">
                            <div class="name">standart</div>
                            <div class="cost">2 500 ₽</div>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Доступ на выставку EOF 2019</li>
                                <li class="tooltip" title="Кроме образовательного курса и пленарного заседания"
                                    style="cursor: help; text-decoration: underline;">Доступ на все
                                    мероприятия научной программы и бизнес-форума
                                </li>
                                <li>Пакет участника</li>
                                <li>Доступ к электронной библиотеке материалов по итогам EOF 2019</li>
                            </ul>
                            <div class="wrapper">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "eoforum:subscription.buy",
                                    ".default",
                                    array(
                                        "REGISTER_FORM_URL" => "/local/api/registration-form.php",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "SUBSCRIPTION_ID" => "34"
                                    ),
                                    false
                                );
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="price-item level-up">
                        <div class="price level-up">
                            <div class="name">level up</div>
                            <div class="cost">10 000 ₽</div>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Доступ на выставку EOF 2019</li>
                                <li class="tooltip" title="Кроме доступа на пленарное заседание"
                                    style="cursor: help; text-decoration: underline;">Доступ на все
                                    мероприятия научной программы и бизнес-форума
                                </li>
                                <li>Пакет участника</li>
                                <li>Доступ к электронной библиотеке материалов по итогам EOF 2019</li>
                                <li>Доступ на образовательный курс по хирургии стопы и голеностопного сустава</li>
                            </ul>
                            <div class="wrapper">


                                    <div class="signature">

                                            <b>Билетов нет</b><br><br>
                                        Образовательный курс по хирургии стопы и голеностопного сустава доступен по билету PREMIUM
                                    </div>

                                <?php/*
                                $APPLICATION->IncludeComponent(
                                    "eoforum:subscription.buy",
                                    ".default",
                                    array(
                                        "REGISTER_FORM_URL" => "/local/api/registration-form.php",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "SUBSCRIPTION_ID" => "33"
                                    ),
                                    false
                                );
                               */ ?>
                            </div>
                        </div>
                    </div>

                    <div class="price-item premium">
                        <div class="price premium">
                            <div class="name">premium</div>
                            <div class="cost">12 000 ₽</div>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Доступ на выставку EOF 2019</li>
                                <li>Доступ на все мероприятия научной программы и бизнес-форума</li>
                                <li>Пакет участника</li>
                                <li>Доступ к электронной библиотеке материалов по итогам EOF 2019</li>
                                <li>Доступ на образовательный курс по хирургии стопы и голеностопного сустава</li>
                                <li>Доступ на пленарное заседание EOF 2019</li>
                                <li>Доступ в VIP зону
                                    «Гостиная EOF 2019»
                                </li>
                                <li>Приглашение на одно лицо на закрытый торжественный прием по случаю открытия EOF
                                    2019
                                </li>
                            </ul>


                            <div class="wrapper">
                                <?
                                if (CModule::IncludeModule("sale")):?>
                                    <?php
                                    $rsOrder = CSaleOrder::GetList(array('ID' => 'DESC'), array('BASKET_PRODUCT_ID' => 35, 'PAYED' => 'Y'));
                                    $count_premium = 0;
                                    while ($arOrder = $rsOrder->Fetch()) {
                                        $count_premium++;
                                    };
                                    ?>

                                    <div class="signature">
                                        <? if ($count_premium < 100) { ?>
                                            <b>Осталось билетов: <?= 72- $count_premium; ?></b>
                                        <? }; ?>
                                    </div>
                                <?php
                                endif;
                                ?>
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "eoforum:subscription.buy",
                                    ".default",
                                    array(
                                        "REGISTER_FORM_URL" => "/local/api/registration-form.php",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "SUBSCRIPTION_ID" => "35"
                                    ),
                                    false
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="extra-info">
                    <div class="box-info location">
                        <div class="box-icon"><img src="/assets/location.png"></div>
                        <div class="box-desc">
                            <div class="title">Место проведения</div>
                            <div class="btns-group">
                                <a target="_blank" href="https://goo.gl/maps/kANkdEDPFq62" class="google-maps">google
                                    maps</a>
                                <a target="_blank"
                                   href="https://yandex.ru/maps/213/moscow/?ll=37.544894%2C55.750974&mode=poi&poi%5Bpoint%5D=37.546012%2C55.751660&poi%5Buri%5D=ymapsbm1%3A%2F%2Forg%3Foid%3D221323378627&z=16.1"
                                   class="yandex-maps">яндекс.карты</a>
                            </div>
                        </div>
                    </div>
                    <div class="box-info flight">
                        <div class="box-icon"><img src="/assets/fly.png"></div>
                        <div class="box-desc">
                            <div class="title">Перелет и проживание</div>
                            <div class="btns-group">
                                <a href="mailto:support@corptravel.ru" class="google-maps">Связаться с туроператором</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="exhibition-section" class="section exhibition-section">


            <div class="custom-container">
                <div class="title-section">Выставка</div>

                <div class="box-wrapper map">
                    <div class="map-side">
                        <div class="img">
                            <img class="delay" data-src="/assets/map.png"
                                 src="/assets/pixel.png" alt="map">
                        </div>

                        <div class="btns-group">
                            <a href="/exhibition/" class="read-more">Подробнее</a>
                            <a target="_blank" href="/assets/exhibition-scheme.pdf" class="download">Скачать</a>
                        </div>
                    </div>


                </div>
                <div class="box-wrapper">
                    <div class="contact-side">
                        <div class="manager-box">
                            <div class="manager-avatar">
                                <img class="delay"
                                     data-src="/assets/saraykina-main.png"
                                     src="/assets/pixel.png" alt="manager">
                            </div>
                            <div class="contact-descr">
                                <div class="name">Алина Сарайкина</div>
                                <div class="position">Работа с партнерами и спонсорами</div>
                            </div>
                            <div class="btns-group">
                                <a href="mailto:saraikina@polylog.ru" class="white">Написать</a>
                                <a href="tel:+79166010869" class="tranparent">Позвонить</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="media-section" class="section media-section">
            <!-- <div class="overlay">Раздел в разработке</div> -->
            <div class="custom-container">
                <div class="title-section">Медиа</div>

                <div class="media-list">
                    <div class="media-box news">
                        <div class="box-icon"><img src="/assets/news.png"></div>
                        <div class="box-desc">
                            <div class="title">Новости</div>
                            <a href="/news/" class="btn">
                                <span class="no-class">Перейти</span>
                            </a>
                        </div>
                    </div>

                    <div class="media-box media">
                        <div class="box-icon"><img src="/assets/media.png"></div>
                        <div class="box-desc">
                            <div class="title">Аккредитация СМИ</div>
                            <a href=" <? if (!$USER->IsAuthorized()) {
                                echo '/signup/?type=media';
                            } else {
                                echo '/profile/';
                            }; ?>" class="btn">
                                <span class="no-class">Перейти</span>
                            </a>
                        </div>
                    </div>

                    <div class="media-box partnership">
                        <div class="box-icon"><img src="/assets/photo.png"></div>
                        <div class="box-desc">
                            <div class="title">Фото и видео</div>
                            <a href="/gallery/" class="btn">
                                <span class="no-class">Перейти</span>
                            </a>
                        </div>
                    </div>

                    <div class="media-box totals">
                        <div class="box-icon"><img src="/assets/partners.png"></div>
                        <div class="box-desc">
                            <div class="title">Инфопартнерам</div>
                            <div class="subtitle"><span class="name">Екатерина Маненкова</span><br><br>По вопросам
                                информационного
                                <br>партнерства<br><br><a href="mailto:press@eoforum.ru">press@eoforum.ru</a><br><br>+7
                                (921) 007-26-73
                            </div>
                            <div class="btns-group">
                                <a href="tel:+79210072673">Позвонить</a>
                                <a class="second" href="mailto:press@eoforum.ru">Написать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="section footer-section partners">
            <div class="custom-container">
                <div class="title-section">Партнеры</div>
                <div class="partners-grid">
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">Генеральный спонсор <span class="hotfix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            </h3>
                            <a target="_blank" href="http://www.osteomed.ru/" class="image">
                                <img src="/assets/logos/logo2.png">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">Генеральный информационный партнер</h3>
                            <a target="_blank" href="http://opinionleader.tilda.ws/main" class="image">
                                <img src="/assets/logos/logo3.png">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">Золотой спонсор</h3>
                            <a target="_blank" href="http://doublemedical.ru/" class="image">
                                <img src="/assets/logos/logo6.png">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">Инновационный спонсор</h3>
                            <a target="_blank" href="https://www.arthrex-russia.ru/" class="image">
                                <img src="/assets/logos/logo1.jpg">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">Серебряный спонсор</h3>
                            <a target="_blank" href="http://www.zimmerbiomet.ru/" class="image">
                                <img src="/assets/logos/logo5.png">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="inner">
                            <h3 class="heading">Серебряный спонсор</h3>
                            <a target="_blank" href="https://jnj.ru/" class="image">
                                <img src="/assets/logos/logo4.png">
                            </a>
                    </div>
                </div>


                <div class="btns-group">
                    <a id="btn-sponsors" onclick="showSponsors(this);" class="read-more">Спонсоры и экспоненты</a>
                    <a id="btn-strategic" onclick="showStrategic(this);" class="download">Стратегические партнеры</a>
                    <a id="btn-info" onclick="showInfo(this);" class="download">Информационные партнеры</a>
                </div>
                <div class="hide" id="info">
                    <? $APPLICATION->IncludeComponent("bitrix:news.list", "infopartners", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "N",    // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                        "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                        "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",    // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                        "FIELD_CODE" => array(    // Поля
                            0 => "NAME",
                            1 => "PREVIEW_PICTURE",
                            2 => "",
                        ),
                        "FILTER_NAME" => "",    // Фильтр
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "8",    // Код информационного блока
                        "IBLOCK_TYPE" => "contacts",    // Тип информационного блока (используется только для проверки)
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                        "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "NEWS_COUNT" => "99",    // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                        "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости",    // Название категорий
                        "PARENT_SECTION" => "14",    // ID раздела
                        "PARENT_SECTION_CODE" => "",    // Код раздела
                        "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                        "PROPERTY_CODE" => array(    // Свойства
                            0 => "PARTNER_LINK",
                            1 => "PARTNER_LINK_VIEW",
                        ),
                        "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                        "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                        "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                        "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                        "SHOW_404" => "N",    // Показ специальной страницы
                        "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                    ),
                        false
                    ); ?>
                </div>
                <div class="" id="sponsors">
                    <? $APPLICATION->IncludeComponent("bitrix:news.list", "infopartners", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "N",    // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                        "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                        "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",    // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                        "FIELD_CODE" => array(    // Поля
                            0 => "NAME",
                            1 => "PREVIEW_PICTURE",
                            2 => "",
                        ),
                        "FILTER_NAME" => "",    // Фильтр
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "8",    // Код информационного блока
                        "IBLOCK_TYPE" => "contacts",    // Тип информационного блока (используется только для проверки)
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                        "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "NEWS_COUNT" => "99",    // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                        "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости",    // Название категорий
                        "PARENT_SECTION" => "15",    // ID раздела
                        "PARENT_SECTION_CODE" => "",    // Код раздела
                        "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                        "PROPERTY_CODE" => array(    // Свойства
                            0 => "PARTNER_LINK",
                            1 => "PARTNER_LINK_VIEW",
                        ),
                        "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                        "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                        "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                        "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                        "SHOW_404" => "N",    // Показ специальной страницы
                        "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                    ),
                        false
                    ); ?>
                </div>
                <div class="hide" id="strategic">
                    <? $APPLICATION->IncludeComponent("bitrix:news.list", "infopartners", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "N",    // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                        "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                        "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",    // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                        "FIELD_CODE" => array(    // Поля
                            0 => "NAME",
                            1 => "PREVIEW_PICTURE",
                            2 => "",
                        ),
                        "FILTER_NAME" => "",    // Фильтр
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "8",    // Код информационного блока
                        "IBLOCK_TYPE" => "contacts",    // Тип информационного блока (используется только для проверки)
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                        "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "NEWS_COUNT" => "99",    // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                        "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости",    // Название категорий
                        "PARENT_SECTION" => "16",    // ID раздела
                        "PARENT_SECTION_CODE" => "",    // Код раздела
                        "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                        "PROPERTY_CODE" => array(    // Свойства
                            0 => "PARTNER_LINK",
                            1 => "PARTNER_LINK_VIEW",
                        ),
                        "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                        "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                        "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                        "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                        "SHOW_404" => "N",    // Показ специальной страницы
                        "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                    ),
                        false
                    ); ?>
                </div>
            </div>
        </footer>
        <footer id="contacts-section" class="section footer-section">
            <div class="custom-container">


                <div class="title-section">Контакты</div>
                <? $APPLICATION->IncludeComponent("bitrix:news.list", "eof-contacts", Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                    "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                    "AJAX_MODE" => "N",    // Включить режим AJAX
                    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                    "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
                    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",    // Учитывать права доступа
                    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                    "CACHE_TYPE" => "A",    // Тип кеширования
                    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
                    "DISPLAY_DATE" => "N",    // Выводить дату элемента
                    "DISPLAY_NAME" => "Y",    // Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "N",    // Выводить текст анонса
                    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                    "FIELD_CODE" => array(    // Поля
                        0 => "ID",
                        1 => "NAME",
                        2 => "SORT",
                        3 => "PREVIEW_PICTURE",
                        4 => "",
                    ),
                    "FILTER_NAME" => "",    // Фильтр
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                    "IBLOCK_ID" => "6",    // Код информационного блока
                    "IBLOCK_TYPE" => "contacts",    // Тип информационного блока (используется только для проверки)
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
                    "INCLUDE_SUBSECTIONS" => "N",    // Показывать элементы подразделов раздела
                    "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                    "NEWS_COUNT" => "20",    // Количество новостей на странице
                    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                    "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                    "PAGER_TITLE" => "Новости",    // Название категорий
                    "PARENT_SECTION" => "",    // ID раздела
                    "PARENT_SECTION_CODE" => "",    // Код раздела
                    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                    "PROPERTY_CODE" => array(    // Свойства
                        0 => "work",
                        1 => "email",
                        2 => "phone",
                        3 => "phone_format",
                    ),
                    "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                    "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                    "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                    "SET_STATUS_404" => "N",    // Устанавливать статус 404
                    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                    "SHOW_404" => "N",    // Показ специальной страницы
                    "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                    "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                    "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                    "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                    "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                ),
                    false
                ); ?>

                <div class="bottom-side">
                    <div class="copyrights">© Все права защищены. Евразийский Ортопедический Форум 2019<br><br>
                        <a style="text-decoration: underline" target="_blank" href="https://www.polylog.ru/ru/company/privacy-policy#consent">Политика конфиденциальности</a><br>
                        <a style="text-decoration: underline" target="_blank" href="https://yadi.sk/i/Q-bWT36uR04CCQ">Пользовательское соглашение</a><br>
                        <a style="text-decoration: underline" target="_blank" href="https://yadi.sk/i/lIFnCuwVUMy58A">Договор-оферты</a></div>

                    <div class="social-networks">
                        <div class="icon">
                            <a href="https://www.facebook.com/eofmoscow/" title="Мы на Facebook" target="_blank"><img
                                        src="<?= SITE_TEMPLATE_PATH ?>/assets/images/facebook-icon.png" alt="facebook"></a>
                        </div>
                        <div class="icon">
                            <a href="https://vk.com/eofmoscow" title="Мы в ВК" target="_blank"><img
                                        src="<?= SITE_TEMPLATE_PATH ?>/assets/images/vk-icon.png" alt="facebook"></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>



<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>