<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");
?>
    <div class="center">
    <div class="title-404">404</div>
    <div class="subtitle-404">Извините, мы не можем найти эту страницу</div>
        <div class="desc">Она никогда не существовала или была перенесена.</div>
        <a class="btn btn-primary" href="/">Вернуться на главную</a>
    </div>


<style>
    .center{
        margin: auto;
    padding: 2rem;
    }
    .title-404 {
        font-size:  17rem;
        line-height: 15rem;
    }
    .subtitle-404 {
        font-size: 4rem;
        margin-bottom: 2rem;
    }
    .desc{
        margin-bottom: 3rem;
    }
    .btn-primary{
        font-size: 1.5rem;
    }
</style>
    <script>
        $(function(){
            document.getElementsByTagName("body")[0].setAttribute('style', 'background: url(/local/templates/eoforum-public-inner/assets/images/medicine_silhouettes_03.jpg); background-size: 1000px auto;')
        });
    </script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>