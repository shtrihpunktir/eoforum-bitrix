<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("О форуме - EOF 2019");
?>

<div class="container-page about-forum-page">

    <div class="section welcome-section">
        <header class="header">
            <div class="custom-container">
                <div class="box-wrapper">
                    <div class="logo">
                        <a href="/">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/logo.svg" alt="logo">
                        </a>
                    </div>
                    <div class="nav-wrapper">
                        <div class="menu-logo"></div>
                        <nav>
                            <ul class="nav">
                                <li><a href="/">Главная</a></li>
                                <li <?if ($APPLICATION->GetCurPage() == '/about/') echo 'class="active"'?> ><a href="/about/">О форуме</a></li>
                                <li <?if ($APPLICATION->GetCurPage() == '/exhibition/') echo 'class="active"'?> ><a href="/exhibition/">Выставка</a></li>
                                <li <?if ($APPLICATION->GetCurPage() == '/science-programm/') echo 'class="active"'?>><a href="/science-programm/">Научная программа</a></li>
                                <li <?if ($APPLICATION->GetCurPage() == '/business-programm/') echo 'class="active"'?>><a href="/business-programm/">Бизнес-форум</a></li>
                                <li <?if ($APPLICATION->GetCurPage() == '/gallery/') echo 'class="active"'?>><a href="/gallery/">Фото и видео</a></li>
                                <li <?if ($APPLICATION->GetCurPage() == '/news/') echo 'class="active"'?>><a href="/news/">Новости</a></li>
                                <li><a href="/en/"><i class="fas fa-globe-americas"></i>&nbsp;Eng</a></li>
                            </ul>
                        </nav>
                        <div class="user-panel">
                            <div class="btns-group">
                                <a href="/login/">Вход</a>
                                <a href="/signup/">Регистрация</a>
                            </div>
                        </div>
                    </div>
                    <button class="open-mobile-menu"></button>
                </div>
            </div>
        </header>

        <div class="welcome-content">
            <div class="custom-container">
                <div class="content-box">
            <span class="welcome-img">
              <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/afp_welcome-img.png" alt="welcome image" />
            </span>
                    <div class="title-box">
                        <div class="img"></div>
                        <div class="title">
                            Евразийский ортопедический форум – крупнейшее в Евразии событие в сфере ортопедии, травматологии и
                            экономики здравоохранения.
                        </div>
                    </div>
                    <div class="description">
                        <div class="info-box">
                            <div class="heading">
                                Форум во второй раз пройдет в Москве, в Экспоцентре 28 – 29 июня 2019 года и соберет более 5 000
                                медицинских специалистов из 80 стран.
                            </div>
                            <div class="info">
                                <p>
                                    ЕОФ – единственное мероприятие такого масштаба с упором на мультидисциплинарный подход. Форум
                                    охватывает все аспекты лечения заболеваний травматолого-ортопедического профиля, включая
                                    фармакотерапию, рентгенологию, анестезиологию, организационные и юридические вопросы защиты врачей.
                                    Широкое представительство различных национальных школ на ЕОФ даст возможность участникам на одной
                                    площадке изучить опыт коллег из Европы и Сингапура, США и Южной Кореи.
                                </p>
                                <p>
                                    12 тематических секций, 5 крупных конгрессов и 43 практических симпозиума направлены на освоение
                                    врачами самых прогрессивным разработок для разных областей травматологии-ортопедии, позволяющих
                                    сократить травматичность операции и сроки восстановления пациентов. Все научные секции ЕОФ
                                    предполагают разбор клинических случаев и дискуссии по итогам.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section organizations-section">
        <div class="custom-container">

            <div class="title">
                Организационный комитет ЕОФ
            </div>

            <div class="organizations-list">
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-1.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-1.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Организация травматологов-ортопедов России
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-2.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-2.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Ассоциация травматологов-ортопедов Москвы
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-3.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-3.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Консалтинговая группа “Полилог”
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-4.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-4.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Главный военный клинический госпиталь им. Н. Н. Бурденко
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-5.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-5.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Главное военно- медицинское управление Министерства Обороны РФ
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-6.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-6.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            ФГУП “ЦИТО”
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-7.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-7.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Центральный институт травматологии и ортопедии им. Н. Н. Приорова
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-8.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-8.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Ассоциация организаций оборонно-промышленного комплекса-производителей медицинских изделий и
                            оборудования (АПИ-ОПК)
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-9.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-9.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Ассоциация специалистов по 3D печати в медицине
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-10.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-10.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Российский национальный исследовательский медицинский университет им. Н. И. Пирогова
                        </div>
                    </div>
                </div>
            </div>

            <div class="title">
                Профильные министерства и государственные структуры
            </div>

            <div class="organizations-list">
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-11.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-11.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Министерство здравоохранения Российской Федерации
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-12.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-12.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Минпромторг России
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-13.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-13.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Министерство труда и социальной защиты РФ
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-14.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-14.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Государственная Дума Федерального собрания РФ
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-15.png" alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-15.png" alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Совет Федерации Федерального Собрания РФ
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="section committee-eof-section">
        <div class="custom-container">
            <div class="title">Научный комитет ЕОФ</div>

            <div class="list-co-chairs">
                <div class="box-wrapper">
                    <div class="box">
                        <div class="box-info">
                            <div class="photo">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/co-chair-1.png" alt="Сергей Mиронов" />
                            </div>
                            <div class="info">
                                <div class="name">Сергей Миронов</div>
                                <div class="position">Сопредседатель научного комитета Евразийского ортопедического форума, академик
                                    РАН, президент Ассоциации травматологов-ортопедов России, главный внештатный специалист
                                    травматолог-ортопед Министерства здравоохранения Российской Федерации</div>
                            </div>
                        </div>
                        <div class="box-message">
                            <div class="message">
                                Уважаемые коллеги, приглашаю вас участвовать в Евразийском ортопедическом форуме, который пройдет
                                28-29 июня 2019 года в Москве.
                            </div>
                            <div class="description">
                                <p>ЕОФ 2017 получил отраслевое признание и заложил основы для объединения травматологов, ортопедов,
                                    представителей смежных специальностей и индустриальных партнеров со всего мира. Мы рады, что местом
                                    проведения такого мероприятия стала именно Россия и благодарны всем, кто принимал активное участие в
                                    подготовке форума.</p>
                                <p>Тогда в ЕОФ приняли участие 3 750 человек: медицинские специалисты, представители органов власти,
                                    индустриальные и промышленные партнеры из 70 стран. Теперь мы развиваем инициативу и продвигаем ЕОФ
                                    как комфортную и эффективную площадку для международного сотрудничества по вопросам развития
                                    высокотехнологичной медицинской помощи и шире – по вопросам здравоохранения. Надеюсь, что
                                    Евразийский ортопедический форум позволит объединить знания и расширить взаимодействие специалистов
                                    на евразийском пространстве.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-wrapper">
                    <div class="box">
                        <div class="box-info">
                            <div class="photo">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/co-chair-2.png" alt="Сергей Mиронов" />
                            </div>
                            <div class="info">
                                <div class="name">Николай Загородний</div>
                                <div class="position">Сопредседатель научного комитета Евразийского ортопедического форума,
                                    член-корреспондент РАН, д.м.н., директор ФГБУ «Национальный медицинский исследовательский центр
                                    травматологии и ортопедии имени Н.Н. Приорова, заведующий кафедрой травматологии и ортопедии
                                    Российского университета дружбы народов, национальный делегат от России в SICOT</div>
                            </div>
                        </div>
                        <div class="box-message">
                            <div class="message">
                                Дорогие коллеги! Евразия – крупнейший континент Земли, где проживает свыше 5 миллиардов человек.
                            </div>
                            <div class="description">
                                <p>Здесь случается более половины всех травм на планете, и накоплен огромный опыт их лечения с
                                    использованием разных подходов, причем далеко не все хорошо изучены. Цель Евразийского
                                    ортопедического форума состоит в том, чтобы анализировать сложившийся опыт разных стран и выявлять
                                    лучшие практики, достойные тиражирования на международном уровне.</p>
                                <p>Конечно, самое подходящее место для проведения столь значимого мероприятия – это столица России,
                                    самого большого по протяженности государства Евразии. В Москве сконцентрировано множество
                                    высококлассных лечебных учреждений, здесь работает передовой отряд травматологов-ортопедов нашей
                                    страны. В Москве внедряются в практику самые современные технологии, и инициатива по проведению
                                    форума будет способствовать инновационным процессам не только в России и Евразии, но и в мировой
                                    травматологии-ортопедии. Уверен, что Европейский ортопедический форум позволит нам поддерживать
                                    тесную связь с коллегами на всем евразийском пространстве.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?$APPLICATION->IncludeComponent("bitrix:news.list", "speakers", Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
                "AJAX_MODE" => "N",	// Включить режим AJAX
                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_TYPE" => "A",	// Тип кеширования
                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                "FIELD_CODE" => array(	// Поля
                    0 => "NAME",
                    1 => "PREVIEW_PICTURE",
                    2 => "PREVIEW_TEXT",
                ),
                "FILTER_NAME" => "",	// Фильтр
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                "IBLOCK_ID" => "9",	// Код информационного блока
                "IBLOCK_TYPE" => "SPEAKERS",	// Тип информационного блока (используется только для проверки)
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
                "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                "NEWS_COUNT" => "99",	// Количество новостей на странице
                "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                "PAGER_TITLE" => "Новости",	// Название категорий
                "PARENT_SECTION" => "",	// ID раздела
                "PARENT_SECTION_CODE" => "",	// Код раздела
                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                "PROPERTY_CODE" => array(	// Свойства

                ),
                "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                "SET_STATUS_404" => "N",	// Устанавливать статус 404
                "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                "SHOW_404" => "N",	// Показ специальной страницы
                "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
            ),
                false
            );?>

        </div>
    </div>

    <div class="section latest-forum-section">
        <div class="custom-container">
            <div class="title-section">Евразийский ортопедический форум 2017</div>

            <div class="list-info">
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/participants-icon.png" alt="icon">
                        </div>
                        <div class="count">3750</div>
                        <div class="name">участников</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/country-icon.png" alt="icon">
                        </div>
                        <div class="count">70</div>
                        <div class="name">стран</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/halls-icon.png" alt="icon">
                        </div>
                        <div class="count">13</div>
                        <div class="name">залов</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/committee-members-icon.png" alt="icon">
                        </div>
                        <div class="count">>70</div>
                        <div class="name">членов научного комитета</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/speakers-icon.png" alt="icon">
                        </div>
                        <div class="count">>550</div>
                        <div class="name">докладчиков</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/exhibitors-icon.png" alt="icon">
                        </div>
                        <div class="count">>150</div>
                        <div class="name">экспонентов</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/event-area-icon.png" alt="icon">
                        </div>
                        <div class="count">4000<span>2</span></div>
                        <div class="name">площадь мероприятия</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/another-icon.png" alt="icon">
                        </div>
                        <div class="count">45</div>
                        <div class="name">индустриальных симпозиумов, мастер-классов и презентаций</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
