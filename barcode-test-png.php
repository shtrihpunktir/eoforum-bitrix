<?php
include 'barcode.php';

$generator = new barcode_generator();

/* Output directly to standard output. */


$input_data = '829016609602'; // получаем код из 12 символов

$ida = str_split($input_data); // ida = input data array, разбиваем строку на массив
$step1 = $ida[1] + $ida[3] + $ida[5] + $ida[7] + $ida[9] + $ida[11]; // складываем четные разрядыы
$step2 = $step1 * 3; // умножаем результат на 3
$step3 = $ida[0] + $ida[2] + $ida[4] + $ida[6] + $ida[8] + $ida[10]; // складываем нечетные разряды
$step4 = $step2 + $step3; // складываем 2 и 3 шаги
if ($step4%10 != 0) { // нам нужна последняя цифра, если она не 0
    $data_end = 10 - $step4%10; // находим цифру, которая дополнит её до 10, это и будет контрольная цифра
} else {
    $data_end = 0; // если это 0, то он так и остается 0
}


$format = 'png';
$symbology = 'ean-13';
$data = $input_data . $data_end;
$options = array(
    'sf' => 4, //масштаб баркода
    'ts' => 19, // размер текста
    'th' => 20, // отступ между текстом и низом барода
    //'tf' => 'Arial', //шрифт для svg
    //'w' => 250,
    //'h' => 350,
    //'wq' => 0,

);


/* Create bitmap image. */
$image = $generator->render_image($symbology, $data, $options);

header('Content-Type: image/png');

imagepng($image);
imagedestroy($image);


/* Generate SVG markup. */
//$svg = $generator->render_svg($symbology, $data, $options);
//echo $svg;