<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;
global $USER;
$APPLICATION->SetTitle("EOF 2019");


?>

    <div class="container-page main-page">
        <header class="header">
            <div class="custom-container">
                <div class="box-wrapper">
                    <div class="logo">
                        <a href="#head-section"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/logo.svg" alt="logo"></a>
                    </div>
                    <div class="nav-wrapper">
                        <nav>
                            <ul class="nav">
                                <li><a href="/en/about/">About Forum</a></li>
                                <li><a href="#events-section">Agenda</a></li>
                                <li><a href="#news-section">News</a></li>
                                <li><a href="#information-participants-section">For participants</a></li>
                                <li><a href="#exhibition-section">Exposition</a></li>
                                <li><a href="#media-section">Media</a></li>
                                <li><a href="#contacts-section">Contacts</a></li>
                                <li><a href="/"><i class="fas fa-globe-americas"></i>&nbsp;Руc</a></li>
                            </ul>
                        </nav>
                        <div class="user-panel">
                            <div class="btns-group">
                                <? if (!$USER->IsAuthorized()) { ?>
                                    <a href="/en/login/">Login</a>
                                    <a href="/en/signup/?type=speaker">For speaker</a>
                                <? } else { ?>
                                    <a href="/en/profile/">Profile</a>
                                <? } ?>
                            </div>
                            <? if ($USER->IsAuthorized()) { ?>
                                <!--<div class="avatar">
                                        <div class="img">
                                            <img
                                                <? /* if ($USER->IsAuthorized()){ */ ?>title="Вы вошли как <? /*= $USER->GetFullName() */ ?>" <? /* }; */ ?>
                                                src="<? /*= SITE_TEMPLATE_PATH */ ?>/assets/images/avatar.png" alt="avatar">
                                        </div>
                                    </div>-->
                            <? }; ?>
                        </div>
                    </div>
                    <button class="open-mobile-menu"></button>
                </div>
            </div>
        </header>

        <div id="head-section" class="main-slider">
            <div class="section welcome-section first-slide">
                <div class="welcome-content">
                    <div class="custom-container">
                        <div class="wrapper-info">
                            <div class="address"><span>Expocenter</span></div>
                            <div class="date">28-29 June 2019</div>
                        </div>
                        <div class="logo-side">
                            <div class="logo">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/ws_logo.png" alt="logo">
                            </div>
                        </div>
                        <div class="info-side">
                            <h1 class="title">
                          <span>
                              eurasian orthopedic <br>forum <span>евразийский ортопедический форум</span>
                          </span>
                            </h1>
                            <div class="tagline">uniting knowledge</div>
                            <div class="btns-group">
                                <a class="buy-ticket" href="#information-participants-section">Buy ticket</a>
                                <a class="about-forum" href="/en/about/">about eof 2019</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section welcome-section second-slide">
                <div class="welcome-content">
                    <div class="custom-container">
                        <div class="wrapper-info">
                            <div class="address"><span>Expocenter</span></div>
                            <div class="date">28-29 June 2019</div>
                        </div>
                        <div class="logo-side">
                            <div class="logo">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/ws_second-logo.png" alt="logo">
                            </div>
                        </div>
                        <div class="info-side">
                            <h1 class="title">
                <span>
                  vertebrological </br>Congress</br> brics spine summit
                </span>
                            </h1>
                            <div class="btns-group">
                                <a class="buy-ticket" href="#information-participants-section">Buy ticket</a>
                                <a class="about-forum" href="/en/about/">about eof 2019</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="events-section" class="section events-section">
            <div class="custom-container">
                <div class="title-section">Event Programme</div>

                <div class="events-list">
                    <div class="events-item science-program">
                        <div class="wrapper">
                            <h2 class="name">SCIENTIFIC PROGRAMME</h2>
                            <div class="desc">
                                Five international congresses<br><br>
                                Unique educational course in foot surgery<br><br>
                                12 topical sections:<br><br>
                                <ul>
                                    <li>Arthroplasty</li>
                                    <li>Injury of the Upper and Lower Limbs</li>
                                    <li>Pediatric Traumatology and Orthopedics</li>
                                    <li>Reconstructive and Plastic Surgery</li>
                                    <li>Disaster Medicine and Treatment of the Wounded</li>
                                    <li>Oncology and Bone Diseases</li>
                                    <li>Anesthesiology and Resuscitation in Traumatology and Orthopedics</li>
                                    <li>Multidisciplinary Approach to Rehabilitation</li>
                                    <li>3D Printing Technologies in Medicine</li>
                                </ul>
                            </div>
                        </div>

                        <div class="btns-group">
                            <a class="transparent" href="/en/science-programm/">Schedule</a>
                            <a class="white" href="#information-participants-section">Buy ticket</a>
                        </div>
                    </div>

                    <div class="events-item business-program">
                        <div class="wrapper">
                            <h2 class="name">BUSINESS FORUM PROGRAMME</h2>
                            <div class="desc">
                                Medical Market Growth Factors<br><br>
                                Industrial Congress:<br><br>
                                <ul>
                                    <li>Development of the Medical Industry</li>
                                    <li>Financing of Innovative Technologies</li>
                                    <li>Regulation of the Medicinal Product Circulation in the EEU</li>
                                </ul>
                                <br><br>Practical events:<br><br>
                                <ul>
                                    <li>Legal Advice to Practicing Doctors</li>
                                    <li>Workshop on Creating a Doctor’s Personal Brand</li>
                                    <li>Presentation of Digital Technologies for Medicine</li>
                                    <li>Meetings of Specialized Committees and Commissions of Federal Authorities</li>
                                </ul>
                            </div>
                        </div>

                        <div class="btns-group">
                            <a class="transparent" href="/en/business-programm/">Schedule</a>
                            <a class="white" href="#information-participants-section">Buy ticket</a>
                        </div>
                    </div>

                    <!-- <div class="events-item submit-report">
                        <div class="wrapper">
                            <h2 class="name">APPLY TO MAKE A PRESENTATION</h2>
                            <div class="desc">
                                If you have a relevant topic you would like to discuss at sections of the EOF 2019,
                                please submit an application with a brief summary of your presentation.<br><br>

                                The EOF Organizing Committee will consider your application and inform you whether your
                                report will be included in the programme. <br><br>
                                Please submit your request before 13 May 2019.
                            </div>
                        </div>

                        <div class="btns-group">
                            <a class="white" href="/en/registration-of-speakers/">Submit an application</a>

                        </div>
                     </div>-->
                </div>
            </div>
        </div>
        <div id="news-section" class="section news-section">
            <div class="custom-container">
                <div class="title-section">News</div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "eof-news-main",
                    array(
                        "COMPONENT_TEMPLATE" => "eof-news-inner",
                        "IBLOCK_TYPE" => "news",
                        "IBLOCK_ID" => "13",
                        "NEWS_COUNT" => "8",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "STRICT_SECTION_CHECK" => "N",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "PAGER_TEMPLATE" => "visual",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => ""
                    ),
                    false
                ); ?>

                <div class="btn-more-news">
                    <a href="/en/news/">More news</a>
                </div>
            </div>
        </div>
        <div id="information-participants-section" class="section information-participants-section">
            <div class="custom-container">
                <div class="title-section">For participants</div>

                <div class="price-list">
                    <div class="price-item student">
                        <div class="price student">
                            <div class="name">STUDENT</div>
                            <div class="cost">1 000 ₽</div>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Access to the EOF 2019 exhibition</li>
                                <li class="tooltip" title="Except for the educational course and plenary session"
                                    style="cursor: help; text-decoration: underline;">Access to all the events of the
                                    scientific programme and business forum
                                </li>
                                <li>Participant’s package</li>
                                <li>Access to the electronic library with the materials of the EOF 2019</li>
                            </ul>

                            <div class="wrapper">
                                <div class="signature" style="font-size: 12px;">
                                    The organizer reserves the right to refuse to enter in case of failure to provide a document certifying student status
                                </div>
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "eoforum:subscription.buy",
                                    ".default",
                                    array(
                                        "REGISTER_FORM_URL" => "/local/api/registration-form.php",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "SUBSCRIPTION_ID" => "56"
                                    ),
                                    false
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="price-item standart">
                        <div class="price standart">
                            <div class="name">standart</div>
                            <div class="cost">2 500 ₽</div>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Access to the EOF 2019 exhibition</li>
                                <li class="tooltip" title="Except for the educational course and plenary session"
                                    style="cursor: help;  text-decoration: underline;">Access to all the events of the
                                    scientific programme and business forum
                                </li>
                                <li>Participant’s package</li>
                                <li>Access to the electronic library with the materials of the EOF 2019</li>
                            </ul>
                            <div class="wrapper">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "eoforum:subscription.buy",
                                    ".default",
                                    array(
                                        "REGISTER_FORM_URL" => "/local/api/registration-form.php",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "SUBSCRIPTION_ID" => "34"
                                    ),
                                    false
                                );
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="price-item level-up">
                        <div class="price level-up">
                            <div class="name">level up</div>
                            <div class="cost">10 000 ₽</div>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Access to the EOF 2019 exhibition</li>
                                <li class="tooltip" title="Except for the plenary session"
                                    style="cursor: help; text-decoration: underline;">Access to all the events of the
                                    scientific programme and business forum
                                </li>
                                <li>Participant’s package</li>
                                <li>Access to the electronic library with the materials of EOF 2019</li>
                                <li>Access to the educational course on foot and ankle surgery</li>
                            </ul>
                            <div class="wrapper">


                                    <div class="signature">

                                            <b>Tickets left: 0</b>

                                    </div>

                                <?php/*
                                $APPLICATION->IncludeComponent(
                                    "eoforum:subscription.buy",
                                    ".default",
                                    array(
                                        "REGISTER_FORM_URL" => "/local/api/registration-form.php",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "SUBSCRIPTION_ID" => "33"
                                    ),
                                    false
                                );
                                */?>
                            </div>
                        </div>
                    </div>

                    <div class="price-item premium">
                        <div class="price premium">
                            <div class="name">premium</div>
                            <div class="cost">12 000 ₽</div>
                        </div>
                        <div class="description">
                            <ul>
                                <li>Access to the EOF 2019 exhibition</li>
                                <li>Access to all the events of the scientific programme and business forum</li>
                                <li>Participant’s package</li>
                                <li>Access to the electronic library with the materials of EOF 2019</li>
                                <li>Access to the educational course on foot and ankle surgery</li>
                                <li>Access to the EOF 2019 plenary session</li>
                                <li>Access to the EOF 2019 Lounge
                                    VIP-zone
                                </li>
                                <li>Invitation for one person to the private reception on the occasion of the EOF 2019
                                    opening.
                                </li>
                            </ul>


                            <div class="wrapper">
                                <?
                                if (CModule::IncludeModule("sale")):?>
                                    <?php
                                    $rsOrder = CSaleOrder::GetList(array('ID' => 'DESC'), array('BASKET_PRODUCT_ID' => 35, 'PAYED' => 'Y'));
                                    $count_premium = 0;
                                    while ($arOrder = $rsOrder->Fetch()) {
                                        $count_premium++;
                                    };
                                    ?>

                                    <div class="signature">
                                       <!-- <?=$count_premium?>-->
                                        <? if ($count_premium < 100) { ?>
                                            <b>Tickets left: <?= 72 - $count_premium; ?></b>
                                        <? }; ?>
                                    </div>
                                <?php
                                endif;
                                ?>
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "eoforum:subscription.buy",
                                    ".default",
                                    array(
                                        "REGISTER_FORM_URL" => "/local/api/registration-form.php",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "SUBSCRIPTION_ID" => "35"
                                    ),
                                    false
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="extra-info">
                    <div class="box-info location">
                        <div class="box-icon"><img src="/assets/location.png"></div>
                        <div class="box-desc">
                            <div class="title">Venue</div>
                            <div class="btns-group">
                                <a target="_blank" href="https://goo.gl/maps/kANkdEDPFq62"
                                   class="google-maps">Google.Maps</a>
                                <a target="_blank"
                                   href="https://yandex.ru/maps/213/moscow/?ll=37.544894%2C55.750974&mode=poi&poi%5Bpoint%5D=37.546012%2C55.751660&poi%5Buri%5D=ymapsbm1%3A%2F%2Forg%3Foid%3D221323378627&z=16.1"
                                   class="yandex-maps">Yandex.Maps</a>
                            </div>
                        </div>
                    </div>
                    <div class="box-info flight">
                        <div class="box-icon"><img src="/assets/fly.png"></div>
                        <div class="box-desc">
                            <div class="title">Air tickets and accommodation</div>
                            <div class="btns-group">
                                <a href="mailto:support@corptravel.ru" class="google-maps">Contact a tour operator</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div id="exhibition-section" class="section exhibition-section">

            <div class="custom-container">
                <div class="title-section">Exposition</div>

                <div class="box-wrapper map">
                    <div class="map-side">
                        <div class="img">
                            <img class="delay" data-src="/assets/map.png"
                                 src="/assets/pixel.png" alt="map">
                        </div>

                        <div class="btns-group">
                            <a href="/en/exhibition/" class="read-more">More detailed</a>
                            <a target="_blank" href="/assets/exhibition-scheme.pdf" class="download">Download</a>
                        </div>
                    </div>


                </div>
                <div class="box-wrapper">
                    <div class="contact-side">
                        <div class="manager-box">
                            <div class="manager-avatar">
                                <img class="delay"
                                     data-src="/assets/saraykina-main.png"
                                     src="/assets/pixel.png" alt="manager">
                            </div>
                            <div class="contact-descr">
                                <div class="name">Alina Saraykina</div>
                                <div class="position">Partner and Sponsor Relation
                                    Manager
                                </div>
                            </div>
                            <div class="btns-group">
                                <a href="mailto:saraikina@polylog.ru" class="white">Send a message</a>
                                <a href="tel:+79166010869" class="tranparent">Call</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="media-section" class="section media-section">
            <!-- <div class="overlay">Раздел в разработке</div> -->
            <div class="custom-container">
                <div class="title-section">MASS MEDIA</div>

                <div class="media-list">
                    <div class="media-box news">
                        <div class="box-icon"><img src="/assets/news.png"></div>
                        <div class="box-desc">
                            <div class="title">News</div>
                            <a href="/en/news/" class="btn">
                                <span class="no-class">Go</span>
                            </a>
                        </div>
                    </div>

                    <div class="media-box media">
                        <div class="box-icon"><img src="/assets/media.png"></div>
                        <div class="box-desc">
                            <div class="title">Mass Media Certification</div>
                            <a href=" <? if (!$USER->IsAuthorized()) {
                                echo '/en/signup/?type=media';
                            } else {
                                echo '/en/profile/';
                            }; ?>" class="btn">
                                <span class="no-class">Go</span>
                            </a>
                        </div>
                    </div>

                    <div class="media-box partnership">
                        <div class="box-icon"><img src="/assets/photo.png"></div>
                        <div class="box-desc">
                            <div class="title">Photo and video</div>
                            <a href="/en/gallery/" class="btn">
                                <span class="no-class">Go</span>
                            </a>
                        </div>
                    </div>

                    <div class="media-box totals">
                        <div class="box-icon"><img src="/assets/partners.png"></div>
                        <div class="box-desc">
                            <div class="title">Info for Partners</div>
                            <div class="subtitle"><span class="name">Ekaterina Manenkova</span><br><br>Information
                                partnership<br><br><a href="mailto:press@eoforum.ru">press@eoforum.ru</a><br><br>+7
                                (921) 007-26-73
                            </div>
                            <div class="btns-group">
                                <a href="tel:+79210072673">Call</a>
                                <a class="second" href="mailto:press@eoforum.ru">Send a message</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="section footer-section partners">
            <div class="custom-container">
                <div class="title-section">Information partners</div>
                <div class="partners-grid">
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">General sponsor</h3>
                            <a target="_blank" href="http://www.osteomed.ru/" class="image">
                                <img src="/assets/logos/logo2.png">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">General information partner</h3>
                            <a target="_blank" href="http://opinionleader.tilda.ws/main" class="image">
                                <img src="/assets/logos/logo3.png">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">Golden sponsor</h3>
                            <a target="_blank" href="http://doublemedical.ru/" class="image">
                                <img src="/assets/logos/logo6.png">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">Innovative sponsor</h3>
                            <a target="_blank" href="https://www.arthrex-russia.ru/" class="image">
                                <img src="/assets/logos/logo1.jpg">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="inner">
                            <h3 class="heading">Silver sponsor</h3>
                            <a target="_blank" href="http://www.zimmerbiomet.ru/" class="image">
                                <img src="/assets/logos/logo5.png">
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="inner">
                            <h3 class="heading">Silver sponsor</h3>
                            <a target="_blank" href="https://jnj.ru/" class="image">
                                <img src="/assets/logos/logo4.png">
                            </a>
                    </div>
                </div>


                <div class="btns-group">
                    <a id="btn-sponsors" onclick="showSponsors(this);" class="read-more">Sponsors and exhibitors</a>
                    <a id="btn-strategic" onclick="showStrategic(this);" class="download">Strategic partner</a>
                    <a id="btn-info" onclick="showInfo(this);" class="download">Information partner</a>
                </div>
                <div class="hide" id="info">
                    <? $APPLICATION->IncludeComponent("bitrix:news.list", "infopartners", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "N",    // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                        "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                        "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",    // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                        "FIELD_CODE" => array(    // Поля
                            0 => "NAME",
                            1 => "PREVIEW_PICTURE",
                            2 => "",
                        ),
                        "FILTER_NAME" => "",    // Фильтр
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "8",    // Код информационного блока
                        "IBLOCK_TYPE" => "contacts",    // Тип информационного блока (используется только для проверки)
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                        "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "NEWS_COUNT" => "99",    // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                        "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости",    // Название категорий
                        "PARENT_SECTION" => "14",    // ID раздела
                        "PARENT_SECTION_CODE" => "",    // Код раздела
                        "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                        "PROPERTY_CODE" => array(    // Свойства
                            0 => "PARTNER_LINK",
                            1 => "PARTNER_LINK_VIEW",
                        ),
                        "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                        "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                        "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                        "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                        "SHOW_404" => "N",    // Показ специальной страницы
                        "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                    ),
                        false
                    ); ?>
                </div>
                <div class="" id="sponsors">
                    <? $APPLICATION->IncludeComponent("bitrix:news.list", "infopartners", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "N",    // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                        "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                        "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",    // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                        "FIELD_CODE" => array(    // Поля
                            0 => "NAME",
                            1 => "PREVIEW_PICTURE",
                            2 => "",
                        ),
                        "FILTER_NAME" => "",    // Фильтр
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "8",    // Код информационного блока
                        "IBLOCK_TYPE" => "contacts",    // Тип информационного блока (используется только для проверки)
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                        "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "NEWS_COUNT" => "99",    // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                        "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости",    // Название категорий
                        "PARENT_SECTION" => "15",    // ID раздела
                        "PARENT_SECTION_CODE" => "",    // Код раздела
                        "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                        "PROPERTY_CODE" => array(    // Свойства
                            0 => "PARTNER_LINK",
                            1 => "PARTNER_LINK_VIEW",
                        ),
                        "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                        "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                        "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                        "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                        "SHOW_404" => "N",    // Показ специальной страницы
                        "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                    ),
                        false
                    ); ?>
                </div>
                <div class="hide" id="strategic">
                    <? $APPLICATION->IncludeComponent("bitrix:news.list", "infopartners", Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                        "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                        "AJAX_MODE" => "N",    // Включить режим AJAX
                        "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                        "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                        "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                        "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                        "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                        "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                        "DISPLAY_NAME" => "Y",    // Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                        "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                        "FIELD_CODE" => array(    // Поля
                            0 => "NAME",
                            1 => "PREVIEW_PICTURE",
                            2 => "",
                        ),
                        "FILTER_NAME" => "",    // Фильтр
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                        "IBLOCK_ID" => "8",    // Код информационного блока
                        "IBLOCK_TYPE" => "contacts",    // Тип информационного блока (используется только для проверки)
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                        "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                        "NEWS_COUNT" => "99",    // Количество новостей на странице
                        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                        "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                        "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                        "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                        "PAGER_TITLE" => "Новости",    // Название категорий
                        "PARENT_SECTION" => "16",    // ID раздела
                        "PARENT_SECTION_CODE" => "",    // Код раздела
                        "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                        "PROPERTY_CODE" => array(    // Свойства
                            0 => "PARTNER_LINK",
                            1 => "PARTNER_LINK_VIEW",
                        ),
                        "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                        "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                        "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                        "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                        "SET_STATUS_404" => "N",    // Устанавливать статус 404
                        "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                        "SHOW_404" => "N",    // Показ специальной страницы
                        "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
                        "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                        "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
                        "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                        "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                    ),
                        false
                    ); ?>
                </div>
            </div>
        </footer>
        <footer id="contacts-section" class="section footer-section">
            <div class="custom-container">
                <div class="title-section">Contacts</div>

                <? $APPLICATION->IncludeComponent("bitrix:news.list", "eof-contacts", Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                    "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                    "AJAX_MODE" => "N",    // Включить режим AJAX
                    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                    "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
                    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "N",    // Учитывать права доступа
                    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                    "CACHE_TYPE" => "A",    // Тип кеширования
                    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
                    "DISPLAY_DATE" => "N",    // Выводить дату элемента
                    "DISPLAY_NAME" => "Y",    // Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "N",    // Выводить текст анонса
                    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                    "FIELD_CODE" => array(    // Поля
                        0 => "ID",
                        1 => "NAME",
                        2 => "SORT",
                        3 => "PREVIEW_PICTURE",
                        4 => "",
                    ),
                    "FILTER_NAME" => "",    // Фильтр
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                    "IBLOCK_ID" => "6",    // Код информационного блока
                    "IBLOCK_TYPE" => "contacts",    // Тип информационного блока (используется только для проверки)
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
                    "INCLUDE_SUBSECTIONS" => "N",    // Показывать элементы подразделов раздела
                    "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                    "NEWS_COUNT" => "20",    // Количество новостей на странице
                    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                    "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                    "PAGER_TITLE" => "Новости",    // Название категорий
                    "PARENT_SECTION" => "",    // ID раздела
                    "PARENT_SECTION_CODE" => "",    // Код раздела
                    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                    "PROPERTY_CODE" => array(    // Свойства
                        0 => "work",
                        1 => "email",
                        2 => "phone",
                        3 => "phone_format",
                    ),
                    "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                    "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                    "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                    "SET_STATUS_404" => "N",    // Устанавливать статус 404
                    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                    "SHOW_404" => "N",    // Показ специальной страницы
                    "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                    "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                    "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                    "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                    "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                ),
                    false
                ); ?>

                <div class="bottom-side">
                    <div class="copyrights">© All rights reserved. The Eurasian Orthopedic Forum 2019</div>
                    <div class="social-networks">
                        <div class="icon">
                            <a href="https://www.facebook.com/eofmoscow/" title="We're on Facebook" target="_blank"><img
                                        src="<?= SITE_TEMPLATE_PATH ?>/assets/images/facebook-icon.png" alt="facebook"></a>
                        </div>
                        <div class="icon">
                            <a href="https://vk.com/eofmoscow" title="We in VK" target="_blank"><img
                                        src="<?= SITE_TEMPLATE_PATH ?>/assets/images/vk-icon.png" alt="facebook"></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>