<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;

?>
    <nav class="navbar pt-5 flex-shrink-0">
        <div class="container">
            <div></div>
            <div>
                <a href="/en" title="<?= $msg['on_main']; ?>">
                    <div class="logo"></div>
                </a>
            </div>
            <div></div>
        </div>
    </nav>
    <div class="flex-grow-1 d-flex flex-column justify-content-center pb-3">
        <?php
        $APPLICATION->IncludeComponent("eoforum:subscription.registration.end", "", [],
            false
        );
        ?>
    </div>
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
