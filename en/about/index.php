<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("О форуме - EOF 2019");
?>

<div class="container-page about-forum-page">

    <div class="section welcome-section">
        <header class="header">
            <div class="custom-container">
                <div class="box-wrapper">
                    <div class="logo">
                        <a href="/en/">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/logo.svg" alt="logo">
                        </a>
                    </div>
                    <div class="nav-wrapper">
                        <div class="menu-logo"></div>
                        <nav>
                            <ul class="nav">
                                <li><a href="/en/">Main page</a></li>
                                <li <? if ($APPLICATION->GetCurPage() == '/en/about/') echo 'class="active"' ?> ><a
                                            href="/en/about/">About Forum</a></li>
                                <li <? if ($APPLICATION->GetCurPage() == '/en/exhibition/') echo 'class="active"' ?> ><a
                                            href="/en/exhibition/">Exposition</a></li>
                                <li <? if ($APPLICATION->GetCurPage() == '/en/science-programm/') echo 'class="active"' ?>>
                                    <a href="/en/science-programm/">Scientific programme</a></li>
                                <li <? if ($APPLICATION->GetCurPage() == '/en/business-programm/') echo 'class="active"' ?>>
                                    <a href="/en/business-programm/">Business forum </a></li>
                                <li <? if ($APPLICATION->GetCurPage() == '/en/gallery/') echo 'class="active"' ?>><a
                                            href="/en/gallery/">Photo and video</a></li>
                                <li <? if ($APPLICATION->GetCurPage() == '/en/news/') echo 'class="active"' ?>><a
                                            href="/en/news/">News</a></li>
                                <li><a href="/"><i class="fas fa-globe-americas"></i>&nbsp;Руc</a></li>


                            </ul>
                        </nav>
                        <div class="user-panel">
                            <div class="btns-group">
                                <? if (!$USER->IsAuthorized()) { ?>
                                    <a href="/en/login/">Login</a>
                                    <a href="/en/signup/">Registration</a>
                                <? } else { ?>
                                    <a href="/en/profile/">Profile</a>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                    <button class="open-mobile-menu"></button>
                </div>
            </div>
        </header>

        <div class="welcome-content">
            <div class="custom-container">
                <div class="content-box">
            <span class="welcome-img">
              <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/afp_welcome-img.png" alt="welcome image"/>
            </span>
                    <div class="title-box">
                        <div class="img"></div>
                        <div class="title">
                            The Eurasian Orthopedic Forum is Eurasia’s largest event related to orthopedics,
                            traumatology, and health care economics.
                        </div>
                    </div>
                    <div class="description">
                        <div class="info-box">
                            <div class="heading">
                                The Forum will be held for the second time in Moscow, Expocenter, on 28-29 June 2019 and
                                is expected to bring together more than 5,000 medical experts from 80 countries.
                            </div>
                            <div class="info">
                                <p>
                                    The EOF is the only large-scale event that has a focus on a multidisciplinary
                                    approach. The Forum covers all the aspects of orthopedic traumatology, including
                                    pharmacotherapy, radiology, anesthesiology, organizational and legal issues related
                                    to doctors’ protection. The wide representation of various national schools at the
                                    EOF will enable the participants to learn from the experience of their colleagues
                                    from Europe and Singapore, the USA and South Korea, all of them interacting on one
                                    platform.
                                </p>
                                <p>
                                    12 dedicated sections, 5 major congresses, and 43 practical symposia will present
                                    the cutting-edge achievements in various areas of orthopedic traumatology allowing
                                    doctors to reduce the degree of surgery invasiveness and cut the patient recovery
                                    time. All the scientific sections of the EOF provide for clinical case studies and
                                    follow-up discussions.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section organizations-section">
        <div class="custom-container">

            <div class="title">
                Organizers
            </div>

            <div class="organizations-list">
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-1.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-1.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Organization of traumatologists and orthopedists of Russia
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-2.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-2.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Association of traumatologists-orthopedists of Moscow
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-3.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-3.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Consulting group “Polylog”
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-4.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-4.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            The main military clinical hospital. N. N. Burdenko
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-5.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-5.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Main military medical Department of the Ministry of Defence of the Russian Federation
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-6.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-6.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            FSUE “CITO”
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-7.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-7.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Central Institute of traumatology and orthopedics. N. N. Priorov
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-8.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-8.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Association of organizations of the military-industrial complex-manufacturers of medical devices and equipment
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-9.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-9.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Association of specialists in 3D printing in medicine
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-10.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-10.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Russian national research medical University. N. I. Pirogov
                        </div>
                    </div>
                </div>
            </div>

            <div class="title">
                Relevant ministries and government agencies
            </div>

            <div class="organizations-list">
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-11.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-11.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            The Ministry of health of the Russian Federation
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-12.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-12.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Ministry of industry and trade of the Russian Federation
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-13.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-13.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Ministry of labour and social protection of the Russian Federation
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-14.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-14.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            State Duma of the Federal Assembly of the Russian Federation
                        </div>
                    </div>
                </div>
                <div class="organization-box-wrapper">
                    <div class="organization-box">
                        <div class="organization-logo">
                            <div class="logo-box">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/bw_organization-15.png"
                                     alt="organization logo">
                            </div>
                            <div class="logo-box scale">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/organization-15.png"
                                     alt="organization logo">
                            </div>
                        </div>
                        <div class="organization-name">
                            Federation Council of the Federal Assembly of the Russian Federation
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="section committee-eof-section">
        <div class="custom-container">
            <div class="title">Scientific committee</div>

            <div class="list-co-chairs">
                <div class="box-wrapper">
                    <div class="box">
                        <div class="box-info">
                            <div class="photo">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/co-chair-1.png" alt="Сергей Mиронов"/>
                            </div>
                            <div class="info">
                                <div class="name">Sergey Mironov</div>
                                <div class="position">Co-Chairmаn of the Eurasian Orthopedic Forum's Scientific
                                    Committee, Academician of the Russian Academy of Sciences, President of the Trauma
                                    Orthopedists Association of Russia, Senior External Expert on Trauma Orthopedics of
                                    the Healthcare Ministry of Russia.
                                </div>
                            </div>
                        </div>
                        <div class="box-message">
                            <div class="message">
                                Dear colleagues, let me invite you to the Eurasian Orthopedic Forum, which will take
                                place in Moscow on 28-29 July 2019.
                            </div>
                            <div class="description">
                                <p>The EOF-2017 won industry recognition and set the foundation for uniting trauma
                                    surgeons, orthopedists, specialists of related professions and industry partners
                                    from all over the world. We are happy Russia is the host to this unique event and
                                    are grateful to all those involved for their proactive participation in preparing
                                    the forum. </p>
                                <p>The last EOF was attended by 3,750 professionals: health specialists, government
                                    representatives, industry and manufacturing partners from 70 countries. We are now
                                    expanding our initiative, promoting the EOF as a comfortable and efficient platform
                                    for international cooperation in the area of providing hi-tech medical assistance
                                    and other more general healthcare-related issues. I hope that the Eurasian
                                    Orthopedic Forum will expand the interaction between specialists in the common
                                    Eurasian space and help them share their knowledge. </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-wrapper">
                    <div class="box">
                        <div class="box-info">
                            <div class="photo">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/co-chair-2.png" alt="Сергей Mиронов"/>
                            </div>
                            <div class="info">
                                <div class="name">Nikolay Zagorodny</div>
                                <div class="position">Co-Chairmаn of the Eurasian Orthopedic Forum's Scientific
                                    Committee, Corresponding Member of the Russian Academy of Sciences, holder of
                                    post-doctoral degree in medicine, Director of N.N. Priorov National Medical Research
                                    Center of Traumatology and Orthopedics, Head of the Department of Traumatology and
                                    Orthopedics of the Peoples' Friendship University of Russia, National Delegate for
                                    Russia at SICOT.
                                </div>
                            </div>
                        </div>
                        <div class="box-message">
                            <div class="message">
                                Dear colleagues, Eurasia is the largest continent of the Earth, home to over five
                                billion people.
                            </div>
                            <div class="description">
                                <p>More than half of all injuries in the world happen here, and a wealth of experience
                                    has been gained in treating these injuries using different approaches, while not all
                                    of these approaches have been thoroughly studied. The goal of the Eurasian
                                    Orthopedic Forum is to analyze the expertise of doctors from different countries and
                                    to identify the best practices that should be replicated on a global level.</p>
                                <p>Moscow, the capital city of Russia, is the best place for holding such an event,
                                    since Russia is the largest state of Eurasia in terms of length. Moscow is home to a
                                    great number of high-class medical institutions employing Russia’s most experienced
                                    orthopedic traumatologists. State-of-the-art technologies are being constantly
                                    integrated into practice here, and the forum will surely contribute to the
                                    innovation processes not only in Russia and Eurasia, but in the global orthopedic
                                    traumatology as well. I am sure that the European Orthopedic Forum will allow us to
                                    maintain close communication with our colleagues from all the Eurasian nations.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?/*$APPLICATION->IncludeComponent("bitrix:news.list", "speakers", Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                "AJAX_MODE" => "N",    // Включить режим AJAX
                "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                "CACHE_TYPE" => "A",    // Тип кеширования
                "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                "DISPLAY_NAME" => "Y",    // Выводить название элемента
                "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                "FIELD_CODE" => array(    // Поля
                    0 => "NAME",
                    1 => "PREVIEW_PICTURE",
                    2 => "PREVIEW_TEXT",
                    3 => "DETAIL_TEXT",
                ),
                "FILTER_NAME" => "",    // Фильтр
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                "IBLOCK_ID" => "9",    // Код информационного блока
                "IBLOCK_TYPE" => "SPEAKERS",    // Тип информационного блока (используется только для проверки)
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",    // Включать инфоблок в цепочку навигации
                "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                "NEWS_COUNT" => "99",    // Количество новостей на странице
                "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                "PAGER_TITLE" => "Новости",    // Название категорий
                "PARENT_SECTION" => "",    // ID раздела
                "PARENT_SECTION_CODE" => "",    // Код раздела
                "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                "PROPERTY_CODE" => array(    // Свойства
                    0 => "NAME_EN",
                ),
                "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                "SET_STATUS_404" => "N",    // Устанавливать статус 404
                "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                "SHOW_404" => "N",    // Показ специальной страницы
                "SORT_BY1" => "SORT",    // Поле для первой сортировки новостей
                "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                "SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
                "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
            ),
                false
            ); */?>

        </div>
    </div>

    <div class="section latest-forum-section">
        <div class="custom-container">
            <div class="title-section">Results of the EOF 2017</div>

            <div class="list-info">
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/participants-icon.png" alt="icon">
                        </div>
                        <div class="count">3750</div>
                        <div class="name">participants</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/country-icon.png" alt="icon">
                        </div>
                        <div class="count">70</div>
                        <div class="name">countries</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/halls-icon.png" alt="icon">
                        </div>
                        <div class="count">13</div>
                        <div class="name">halls</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/committee-members-icon.png" alt="icon">
                        </div>
                        <div class="count">>70</div>
                        <div class="name">members of the scientific Committee</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/speakers-icon.png" alt="icon">
                        </div>
                        <div class="count">>550</div>
                        <div class="name">speakers</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/exhibitors-icon.png" alt="icon">
                        </div>
                        <div class="count">>150</div>
                        <div class="name">exhibitors</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/event-area-icon.png" alt="icon">
                        </div>
                        <div class="count">4000<span>2</span></div>
                        <div class="name">the area of the event</div>
                    </div>
                </div>
                <div class="wrapper-box">
                    <div class="box-info">
                        <div class="icon">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/another-icon.png" alt="icon">
                        </div>
                        <div class="count">45</div>
                        <div class="name">industrial symposia, workshops, and presentations</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
