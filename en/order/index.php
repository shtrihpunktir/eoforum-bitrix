<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Cost and options for participation");
?>

<nav class="navbar pt-5 mb-5">
    <div class="container">
        <div>
            <a href="/en/" title="Back to main page"><div class="logo"></div>
        </div>
    </div>
</nav>

<div class="container">
    <div>
        <div class="breadcrumb mb-5">
            <li class="breadcrumb-item"><a href="/en/">Main page</a></li>
            <li class="breadcrumb-item"><a href="/en/profile">Personal account</a></li>
            <li class="breadcrumb-item active">Cost and options for participation</li>
        </div>
    </div>
    <div class="d-flex mb-4">
        <div class="pr-5 pt-1"><a href="/en/profile" class="btn btn-outline-secondary"><i class="fa fa-long-arrow-left"></i>
                &nbsp; Back</a></div>
        <h1>Cost and options for participation</h1>
    </div>
	<div class="pt-3">
    <? $APPLICATION->IncludeComponent(
		"eoforum:eoforum.subscription",
		"",
		[]
	); ?>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?> 