<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Sign up");
?><?$APPLICATION->IncludeComponent(
    "bitrix:main.register",
    "eo",
    Array(
        "AUTH" => "N",
        "REQUIRED_FIELDS" => array("EMAIL","NAME","LAST_NAME","PERSONAL_CITY","PERSONAL_PHONE"),
        "SET_TITLE" => "N",
        "SHOW_FIELDS" => array("EMAIL","NAME","LAST_NAME","WORK_COMPANY","WORK_POSITION","PERSONAL_CITY","PERSONAL_PHONE", "UF_TYPE", "UF_ACADEMIC", "UF_ACADEMIC_ANOTHER", "PERSONAL_PHOTO"),
        "SUCCESS_PAGE" => "?success=Y",
        "USER_PROPERTY" => array(),
        "USER_PROPERTY_NAME" => "",
        "USE_BACKURL" => "Y"
    )
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>