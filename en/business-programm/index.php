<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Business forum - EOF 2019");
?>

<div class="main">
    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="/en/">Main page</a></li>
                <li>Business forum</li>
            </ul>
        </div>
        <h1>Business forum</h1>

        <div class="programm-banner business">
            <a href="/eof-business-03.06-en.pdf" class="btn">Download the programme</a>
            <!--  <a href="/en/registration-of-speakers/" class="btn">Submit an application</a>-->
            <a href="/en/science-programm/" class="btn">Scientific programme</a>

        </div>

        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "programms",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "NAME",
                    1 => "SORT",
                    2 => "PREVIEW_TEXT",
                    3 => "DETAIL_TEXT",
                    5 => "HOW_MANY_PEOPLE",
                ),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "7",
                "IBLOCK_TYPE" => "EVENT_PROGRAMM",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "99",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "12",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "EVENT_TIME",
                    1 => "EVENT_DATE",
                    2 => "EVENT_PALCE",
                    3 => "MODERATORS",
                    4 => "NAME_EN",
                    5 => "SUBTITLE_EN",
                    6 => "EVENT_TIME_EN",
                    7 => "EVENT_DATE_EN",
                    8 => "EVENT_PALCE_EN",
                    9 => "SEPARATOR",
                    10 => "PREVIEW_TEXT_EN",
                    11 => "DETAIL_TEXT_EN",
                    12 => "MODERATORS_EN",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N",
                "COMPONENT_TEMPLATE" => "programms"
            ),
            false
        ); ?>
    </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
