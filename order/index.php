<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Ортопедический форум");
?>

<nav class="navbar pt-5 mb-5">
    <div class="container">
        <div>
            <div class="logo"></div>
        </div>
    </div>
</nav>

<div class="container">
    <div>
        <div class="breadcrumb mb-5">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/profile">Личный кабинет</a></li>
            <li class="breadcrumb-item active">Стоимость и варианты участия</li>
        </div>
    </div>
    <div class="d-flex mb-4">
        <div class="pr-5 pt-1"><a href="/profile" class="btn btn-outline-secondary"><i class="fa fa-long-arrow-left"></i>
                &nbsp; Назад</a></div>
        <h1>Стоимость и варианты участия</h1>
    </div>
	<div class="pt-3">
    <? $APPLICATION->IncludeComponent(
		"eoforum:eoforum.subscription",
		"",
		[]
	); ?>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?> 