<?php

use \Bitrix\Main\Security;
use \Bitrix\Main\Context;


class SubscriptionRegistrationEnd extends \CBitrixComponent
{

    const MESSAGES = [
        'ru' => [
            'invalid_code' => 'Неверная ссылка',
            'thank_you' => 'Спасибо за покупку!',
            'payment_required' => 'Заказ не оплачен',
            'no_action_required' => 'Данные уже заполнены ранее',
            'name' => 'Имя',
            'last_name' => 'Фамилия',
            'city' => 'Город',
            'phone' => 'Телефон',
            'academic' => 'Название ВУЗа',
            'work_place' => 'Место работы',
            'position' => 'Должность',
            'error_required' => 'Заполните поле',
            'save' => 'Сохранить',
            'checkout_email' => 'Билет отправлен Вам на почту',
            'login' => 'Войти',
        ],
        'en' => [
            'invalid_code' => 'Invalid url',
            'thank_you' => 'Thank you!',
            'payment_required' => 'Payment required',
            'no_action_required' => 'All data already valid',
            'name' => 'First Name',
            'last_name' => 'Last Name',
            'city' => 'City',
            'phone' => 'Phone',
            'academic' => 'Academic',
            'work_place' => 'Work place',
            'position' => 'Position',
            'error_required' => 'Required',
            'save' => 'Submit',
            'checkout_email' => 'Ticket sent at your email',
            'login' => 'Login',
        ],
    ];
    private $signer;
    public $arResult;

    function getSigner()
    {
        if (!$this->signer)
            $this->signer = new Security\Sign\Signer();
        return $this->signer;
    }

    function getRequest()
    {
        return Context::getCurrent()->getRequest();
    }

    public function executeComponent()
    {
        $request = $this->getRequest();
        list($uid, $oid) = $this->unpack($request->getQuery('HASH'));
        $service = OrderServiceProvider::provide();
        $order = $service->getOrdersRepo()->load($uid);
        $formConfig = $this->getFormConfig($order->getProduct()->name);
        $user = \CUser::GetByID($uid)->Fetch();
        if ($order->getId() != $oid) {
            $this->arResult = [
                'ACTION' => 'MESSAGE',
                'MESSAGE' => $this->getMessage('invalid_code')
            ];
        } elseif ($request->isPost()) {
            $formData = $request->getPostList()->toArray();
            $isValid = $this->validate($formConfig, $formData);
            if ($isValid) {
                $user = new \CUser;
                $data = $request->getPostList()->toArray();

                if ($this->isStudentOrder($order->getProduct()->name)) {
                    $data['UF_TYPE'] = 7;
                }
                $result = $user->Update($uid, $data);
                $this->arResult = $result;
                $this->arResult = [
                    'ACTION' => 'DONE'
                ];
            } else {
                $this->arResult = [
                    'ACTION' => 'SHOW_FORM',
                    'ORDER' => $order,
                    'USER_ID' => $uid,
                    'FORM' => $formConfig,
                    'SHOW_VALIDATION' => true
                ];
            }
        } else {
            if (!$order->getIsPaid()) {
                $this->arResult = [
                    'ACTION' => 'MESSAGE',
                    'MESSAGE' => $this->getMessage('payment_required')
                ];
            } else {
                $isValid = $this->validate($formConfig, $user);
                if ($isValid) {
                    $this->arResult = [
                        'ACTION' => 'DONE'
                    ];
                } else {
                    $this->arResult = [
                        'ACTION' => 'SHOW_FORM',
                        'ORDER' => $order,
                        'USER_ID' => $uid,
                        'FORM' => $formConfig,
                        'SHOW_VALIDATION' => false
                    ];
                }
            }
        }
        $this->arResult['USER'] = CUser::GetByID($uid)->Fetch();
        $this->arResult['MESSAGE_STRINGS'] = self::MESSAGES[LANGUAGE_ID];
        $this->arResult['LOGIN_URL'] = LANGUAGE_ID === 'ru' ? '/login' : ("/" . LANGUAGE_ID . "/login");
        $this->includeComponentTemplate();
    }

    private function unpack(string $hash)
    {
        $signer = $this->getSigner();
        return $signer->unpack($signer->unsign($hash));
    }

    private function getFormConfig($product)
    {
        $basicFields = [
            ['NAME' => 'NAME', 'LABEL' => $this->getMessage('name')],
            ['NAME' => 'LAST_NAME', 'LABEL' => $this->getMessage('last_name')],
            ['NAME' => 'PERSONAL_CITY', 'LABEL' => $this->getMessage('city')],
            ['NAME' => 'PERSONAL_PHONE', 'LABEL' => $this->getMessage('phone')],
        ];

        if ($this->isStudentOrder($product)) {
            $specificFields = [
                ['NAME' => 'UF_ACADEMIC_ANOTHER', 'LABEL' => $this->getMessage('academic')],
            ];
        } else {
            $specificFields = [
                ['NAME' => 'WORK_COMPANY', 'LABEL' => $this->getMessage('work_place')],
                ['NAME' => 'WORK_POSITION', 'LABEL' => $this->getMessage('position')],
            ];
        }
        return array_merge($basicFields, $specificFields);
    }

    private function validate(&$formConfig, $formData)
    {
        $isValid = true;
        foreach ($formConfig as &$field) {

            if (empty($formData[$field['NAME']])) {
                $field['ERROR'] = $this->getMessage('error_required');
                $isValid = false && $isValid;
            } else {
                $field['VALUE'] = $formData[$field['NAME']];
            }
        }
        return $isValid;
    }

    private function getMessage($code)
    {
        return self::MESSAGES[LANGUAGE_ID][$code];
    }

    private function isStudentOrder($product)
    {
        return 'student' === strtolower($product);
    }
}