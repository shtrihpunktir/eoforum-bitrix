<div class="container">
    <?php
    $action = $arResult['ACTION'];
    $showValidation = $arResult['SHOW_VALIDATION'] ?? false;
    if ($action === 'MESSAGE'):
        ?>
        <h1 class="text-center mb-5"><?= $arResult['MESSAGE'] ?></h1>
    <?php elseif ($action === 'SHOW_FORM') : ?>

        <div class="row">
            <div class="col col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12 offset-0">
                <div class="p-5 bg-white shadow-sm mb-5">
                    <div class="px-sm-5 pt-5">

                        <form method="post">
                            <?php foreach ($arResult['FORM'] as $field): ?>
                                <?php
                                $inputClass = $showValidation ? ($field['ERROR'] ? 'is-invalid' : '') : '';
                                ?>
                                <div class="form-group">
                                    <div class="input-fl <?= $field['VALUE'] ? 'not-empty' : '' ?>">
                                        <label class="f-label"
                                               for="<?= $field['NAME'] ?>"><?= $field['LABEL'] ?></label>
                                        <input class="form-control form-control-lg <?= $inputClass ?>"
                                               id="<?= $field['NAME'] ?>"
                                               name="<?= $field['NAME'] ?>" value="<?= $field['VALUE'] ?>">
                                        <?php if ($showValidation && $field['ERROR']): ?>
                                            <div class="invalid-feedback"><?= $field['ERROR'] ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                            <div class="form-group row">
                                <div class="col-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                                        <?= $arResult['MESSAGE_STRINGS']['save'] ?>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($action === 'DONE') : ?>
        <h1 class="text-center mb-5"><?= $arResult['MESSAGE_STRINGS']['thank_you'] ?></h1>
        <p class="text-center mb-5">
            <?= $arResult['MESSAGE_STRINGS']['checkout_email'] ?>
        </p>
        <div class="text-center mb-5">
            <a href="<?= $arResult['LOGIN_URL'] ?>" class="btn btn-primary">
                <?= $arResult['MESSAGE_STRINGS']['login'] ?>
            </a>
        </div>
    <?php else: ?>
        <h1 class="text-center mb-5">Произошла неизвестная ошибка</h1>
    <?php endif; ?>
</div>
