<?

namespace Ru\Eoforum\Subscription\Infrastructure;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'domain.php';

use Ru\Eoforum\Subscription\Domain\OrderRepository;
use Ru\Eoforum\Subscription\Domain\PaymentRepository;
use Ru\Eoforum\Subscription\Domain\ProductsRepository;
use Ru\Eoforum\Subscription\Domain\Order;
use Ru\Eoforum\Subscription\Domain\Product;
use Ru\Eoforum\Subscription\Domain\Price;
use Ru\Eoforum\Subscription\Domain\ProductGradient;
use Bitrix\Main\Diag\Debug;

class OrderNotExistsException extends \Exception
{
}

class UserNotDefinedException extends \Exception
{
}

class InvalidUserSiteException extends \Exception
{
}

class BitrixOrmProductRepository implements ProductsRepository
{
    function findAll(): iterable
    {
        $arFilter = array("IBLOCK_ID" => IntVal($this->getIblockId()), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        $res = \CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter);
        $elements = [];
        while ($ob = $res->GetNextElement()) {
            $elements[] = $this->populateProduct($ob);
        }
        return $elements;
    }

    function findById($id): \Ru\Eoforum\Subscription\Domain\Product
    {
        if ($ob = \CIBlockElement::GetByID($id)->GetNextElement()) {
            return $this->populateProduct($ob);
        }
    }

    function findUpgrades($id)
    {
        $products = $this->findAll();
        $found = null;
        for ($i = 0; $i < count($products); $i++) {
            $product = $products[$i];
            // print_r(['product' => $product, 'id' => $id]);
            $found = $product->id == $id ? $product : $found;
            if ($found && ($product->id != $found->id) && $found->name != 'STUDENT') {
                $product->price->setDiscount($found->price->getAmount());
                $upgrades[] = $product;
            }
        }
        return $upgrades;
    }


    private function getIblockId()
    {
        return 3;
    }

    private function populateProduct($ob)
    {
        $arFields = $ob->GetFields();
        $price = GetCatalogProductPrice(
            $arFields['ID'],
            \CCatalogGroup::GetBaseGroup()['ID']
        );
        $product = new Product($arFields['ID'], $arFields['NAME'], new Price($price['PRICE'], $price['CURRENCY']));
        $product->detailText = $arFields['DETAIL_TEXT'];
        $product->previewText = $arFields['PREVIEW_TEXT'];


        $props = $ob->GetProperties();

        $gradient = new ProductGradient();
        $gradient->start = $props['BACKGROUND_GRADIENT_START']['VALUE'] ? $props['BACKGROUND_GRADIENT_START']['VALUE'] : $props['BACKGROUND_GRADIENT_START']['DEFAULT_VALUE'];
        $gradient->end = $props['BACKGROUND_GRADIENT_END']['VALUE'] ? $props['BACKGROUND_GRADIENT_END']['VALUE'] : $props['BACKGROUND_GRADIENT_END']['DEFAULT_VALUE'];;
        $product->gradient = $gradient;
        return $product;
    }
}

class BitrixOrmOrderRepository implements OrderRepository
{
    private $productsRepo;

    function __construct(ProductsRepository $productsRepo)
    {
        $this->productsRepo = $productsRepo;
    }

    function load($userId): Order
    {
        if (!isset($userId)) {
            throw new UserNotDefinedException();
        }
        $filter = [
            'filter' => [
                'USER_ID' => $userId
            ],
            'order' => ['ID']
        ];

        $userOrders = \Bitrix\Sale\Order::loadByFilter($filter);
        if (count($userOrders) < 1) {
            throw new OrderNotExistsException();
        }
        $bxOrder = $userOrders[count($userOrders) - 1];
        $product = $this->loadOrderProduct($bxOrder);

        $order = new Order($bxOrder->getId(), $product, $bxOrder->isPaid());
        $order->price = $bxOrder->getPrice();
        return $order;
    }

    function save(Order $order): void
    {
        print_r($order);
        // throw new \Exception('not implemented yet');
    }

    private function loadOrderProduct($order)
    {
        $orderItem = $order->getBasket()->getBasketItems()[0];
        $productId = $orderItem->getField('PRODUCT_ID');
        return $this->productsRepo->findById($productId);
    }
}

class BitrixPaysystemRepositiry implements PaymentRepository
{
    function getForm(Order $order): string
    {
        $order = \Bitrix\Sale\Order::load($order->getId());
        $iterator = $order->getPaymentCollection()->getIterator();
        $payment = $iterator->current();
        $service = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        return $service->initiatePay($payment, $context->getRequest(), \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING)
            ->getTemplate();
    }
}

class OrderService
{
    /**
     * @property OrderRepository
     */
    private $ordersRepo;
    /**
     * @property ProductsRepository
     */
    private $productsRepo;

    /**
     * @return mixed
     */
    public function getOrdersRepo(): OrderRepository
    {
        return $this->ordersRepo;
    }

    /**
     * @return mixed
     */
    public function getProductsRepo()
    {
        return $this->productsRepo;
    }

    static protected $instance;

    static function instantiate(
        OrderRepository $ordersRepo,
        ProductsRepository $productsRepo
    ): self
    {
        if (!self::$instance) {
            self::$instance = new self($ordersRepo, $productsRepo);
        }
        return self::$instance;
    }

    private function __construct(OrderRepository $ordersRepo, ProductsRepository $productsRepo)
    {
        $this->ordersRepo = $ordersRepo;
        $this->productsRepo = $productsRepo;
    }


    function getOrderForm($userId)
    {
        try {
            $order = $this->ordersRepo->load($userId);
            $form = [
                'order' => $order,
                'paysystem' => new BitrixPaysystemRepositiry(),
            ];
            if ($order->getIsPaid()) {
                $form['products'] = $this->productsRepo->findUpgrades($order->getProduct()->id);
                $form['isUpgrade'] = true;
            }
            return $form;
        } catch (OrderNotExistsException $e) {
            return [
                'products' => $this->productsRepo->findAll()
            ];
        }
    }

    /**
     * @param $userId
     * @param array $data
     *  PRODUCT_ID string|int
     *  NAME string
     *  PRICE string|int
     *  CURRENCY string
     * @return array|bool|void
     * @throws InvalidUserSiteException
     */
    function buyOrder($userId, $data)
    {
        try {
            $order = $this->ordersRepo->load($userId);
            $product = $order->getProduct();
            if ($product->id == $data['PRODUCT_ID']) {
                return;
            }

            return $this->createOrder($userId, $data, true);
        } catch (OrderNotExistsException $e) {
            return $this->createOrder($userId, $data);
        }
    }

    function cancelOrder($userId, $orderId)
    {
        \Bitrix\Sale\Order::delete($orderId);
    }

    private function createOrder($userId, $data, $isUpgrade = false)
    {

        $siteId = $this->resolveUserSiteId($userId);

        $basket = \Bitrix\Sale\Basket::create($siteId);

        $basketUserId = \CSaleBasket::GetBasketUserID();
        \CSaleBasket::DeleteAll($basketUserId);

        $item = $basket->createItem("catalog", $data["PRODUCT_ID"]);
        $item->setFields([
            'PRODUCT_ID' => $data['PRODUCT_ID'],
            'NAME' => $data['NAME'],
            'PRICE' => $data['PRICE'],
            'CURRENCY' => $data['CURRENCY'],
            'QUANTITY' => 1
        ]);
        $order = \Bitrix\Sale\Order::create($siteId, $userId);
        $order->setBasket($basket);

        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem(
        // @TODO Set paysystem from component params
            \Bitrix\Sale\PaySystem\Manager::getObjectById(2)
        );

        $payment->setField("SUM", $order->getPrice());
        $payment->setField("CURRENCY", $order->getCurrency());
        $order->setField("COMMENTS", $isUpgrade ? 'Апгрейд' : '');

        $result = $order->save();
        if (!$result->isSuccess()) {
            return false;
        } else {
            return [
                'order' => $order,
                'payment' => $payment
            ];
        }
    }

    private function resolveUserSiteId($userId)
    {
        $rsUser = \CUser::GetByID($userId);
        $arUser = $rsUser->Fetch();
        $siteId = null;
        if ($arUser['LID']) {
            $siteId = $arUser['LID'];
        } else {
            $site = \CSite::GetByID(SITE_ID)->Fetch();
            if (!$site) {
                $siteId = 's1';
            } else {
                $siteId = $site['LID'];
            }
        }

        if (empty($siteId)) {
            throw new InvalidUserSiteException();
        }
        return $siteId;
    }
}
