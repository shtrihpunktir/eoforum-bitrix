<?
namespace Ru\Eoforum\Subscription\Domain;

class OrderIsPaidException extends \Exception
{ }

interface ProductsRepository
{
    /**
     * @return \Ru\Eoforum\Subscription\Domain\Product[]
     */
    function findAll(): iterable;

    function findById($id): Product;
}

interface OrderRepository
{
    function load($userId): Order;
    function save(Order $order): void;
}

interface PaymentRepository
{
    function getForm(Order $order): string;
}
class Price
{
    private $amount;
    private $currency;
    private $discount = 0;
    function __construct($amount, $currency)
    {
        $this->amount = (float)$amount;
        $this->currency = $currency;
    }

    function format(callable $formatter)
    {
        return $formatter($this->getAmount(), $this->currency);
    }
    function getAmount()
    {
        return $this->amount - $this->discount;
    }
    function getCurrency()
    {
        return $this->currency;
    }
    function setDiscount($discount)
    {
        $this->discount = $discount;
    }
}

class ProductGradient
{
    public $start;
    public $end;
}
class Product
{
    public $id;
    /**
     * @property string
     */
    public $name;
    /**
     * @property string
     */
    public $previewText;
    /**
     * @property string
     */
    public $detailText;
    /**
     * @property Price
     */
    public $price;
    /**
     * @property ProductGradient
     */
    public $gradient;
    function __construct($id, $name, Price $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }
}

class Order
{
    /**
     * @property int;
     */
    private $id;
    /**
     * @property bool
     */
    private $isPaid;

    /**
     * @property Product
     */
    private $product;

    public $price = 0;
    function __construct($id, Product $product, $isPaid = false)
    {
        $this->id = $id;
        $this->isPaid = $isPaid;
        $this->product = $product;
    }

    function getId()
    {
        return $this->id;;
    }

    function getIsPaid()
    {
        return $this->isPaid;
    }

    function setProduct(Product $product)
    {
        $this->product = $product;
    }

    function getProduct(): Product
    {
        return $this->product;
    }
}


class OrderPaymentBuilder
{
    function getPaymentForm(Order $order)
    { }
}
