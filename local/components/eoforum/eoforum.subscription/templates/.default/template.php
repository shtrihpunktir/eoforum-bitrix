<?
$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['buy'] = 'Buy';
    $msg['upgrade_btn'] = 'Upgrade';
    $msg['upgrade_notice'] = 'Upgrade to';
    $msg['your_ticket'] = 'Your ticket';
    $msg['we_remember'] = '<em>We remember that you previously chose this ticket. If you change your mind, click the button below.</em>';
    $msg['pick_another'] = 'Choose another ticket';
    $msg['avalible_after_moderation'] = '<p>The purchase of this ticket is available only to users of the "Participant - Student" type after moderation.<br><br>Moderation takes <nobr>1-3 days</nobr>.</p>';
    $msg['not_avalible'] = '<p>The purchase of this ticket is available only to users of the "Participant - Student" type after moderation.<br><br>Tickets are available for you:<br><b>STANDART<br>LEVEL UP<br>PREMIUM</b>.</p>';
} else {
    $msg['buy'] = 'Купить';
    $msg['upgrade_btn'] = 'Улучшить';
    $msg['upgrade_notice'] = 'Улучшить до';
    $msg['your_ticket'] = 'Ваш билет';
    $msg['we_remember'] = '<em>Мы помним, что ранее Вы выбирали этот билет. Если Вы передумали, нажмите на кнопку ниже</em>';
    $msg['pick_another'] = 'Выбрать другой билет';
    $msg['avalible_after_moderation'] = '<p>Покупка этого билета доступна только пользователям типа "Участник - студент" после прохождения модерации<br><br>Модерация производится в течение <nobr>1-3 дней</nobr>.</p>';
    $msg['not_avalible'] = '<p>Покупка этого билета доступна только пользователям типа "Участник - студент" после прохождения модерации<br><br>Для Вас доступны билеты:<br><b>STANDART<br>LEVEL UP<br>PREMIUM</b>.</p>';
};



?>
<div class="row justify-content-center">
    <?
    if (isset($arResult['order'])) :
        /**
         * @var Ru\Eoforum\Subscription\Domain\Order;
         */
        $order = $arResult['order'];
        /**
         * @var Ru\Eoforum\Subscription\Domain\Product;
         */
        $product = $order->getProduct();
        ?>
        <div class="col col-12 col-md-6 col-lg-4 d-flex flex-column mb-5">
            <div class="pricing-block pricing-ordinary bg-white rounded shadow-lg flex-grow-1 d-flex flex-column">
                <div class="pricing-block-heading rounded d-flex align-items-center py-3" style="background: linear-gradient(to right, <?= $product->gradient->start ?>, <?= $product->gradient->end ?>)">
                    <h3 class="pt-1 w-100">
                        <?= $msg['your_ticket'] ?>
                        <strong class="pt-3">

                            <?= $product->name ?>

                        </strong>
                    </h3>
                </div>
                <div class="pricing-block-content px-5 pt-5 pb-4 flex-grow-1">
                    <? if (strpos($page, 'en/')) {
                        echo $product->detailText;
                    } else {
                        echo $product->previewText;
                    }; ?>
                </div>
                <? if (!$order->getIsPaid()) : ?>
                    <div class='pb-5 text-center'>
                        <div class="pricing-block-content px-5 pt-5 pb-4 flex-grow-1"><?= $msg['we_remember'] ?>
                        </div>
                        <form method='POST' action='./'>
                            <input type='hidden' name='action' value='CANCEL'>
                            <input type='hidden' name='ORDER_ID' value='<?= $order->getId(); ?>'>
                            <button class="btn btn-primary" type='submit'><?= $msg['pick_another'] ?></button>
                        </form>
                    </div>
                <? endif; ?>
            </div>
        </div>
        <? if (!$order->getIsPaid()) : ?>
            <div class="col col-12 col-md-6 col-lg-4 d-flex flex-column mb-5">
                <div class="pricing-block pricing-ordinary bg-white rounded shadow-lg flex-grow-1 d-flex flex-column">
                    <div class="pricing-block-content px-5 pt-5 pb-4 flex-grow-1 d-flex justify-content-center align-items-center text-center payment-form">
                        <?
                        ?>
                        <!--Start <? echo $product->name ?> -->

                        <? if ($arResult['userIsStudent']) : ?>
                            <? if ($arResult['userModerationSuccess']) {
                                echo $arResult['paysystem']->getForm($arResult['order']);
                            } else {
                                echo $msg['avalible_after_moderation'];
                            } ?>

                        <? else : ?>
                            <?
                            if ($product->name == "STUDENT") { //Если пользователь выбрал билет типа "STUDENT"
                                echo $msg['not_avalible'];
                            } else { //Если пользователь выбрал любой другой билет (не STUDENT)
                                echo $arResult['paysystem']->getForm($arResult['order']); //выводим форму для оплаты
                            }
                            ?>
                        <? endif; ?>
                        <!--End -->

                    </div>
                </div>
            </div>
        <? endif; ?>

    <? endif; ?>
    <? foreach ($arResult['products'] as $product) : ?>
        <div class="col col-12 col-md-6 col-lg-<?= count($arResult['products']) < 3 ? 4 : 3 ?> d-flex flex-column mb-5">
            <div class="pricing-block pricing-ordinary bg-white rounded shadow-lg flex-grow-1 d-flex flex-column">
                <div class="pricing-block-heading rounded d-flex align-items-center py-3" style="background: linear-gradient(to right, <?= $product->gradient->start ?>, <?= $product->gradient->end ?>)">
                    <h3 class="pt-1 w-100">
                        <?= $arResult['isUpgrade'] ? $msg['upgrade_notice'] : '' ?>
                        <?= $product->name ?>
                        <strong class="pt-3">
                            <?= CCurrencyLang::CurrencyFormat($product->price->getAmount(), $product->price->getCurrency()); ?>
                            <i class="fa fa-ruble"></i>
                        </strong>
                    </h3>
                </div>
                <div class="pricing-block-content px-1 pt-5 pb-4 flex-grow-1">
                    <? if (strpos($page, 'en/')) {
                        echo $product->detailText;
                    } else {
                        echo $product->previewText;
                    }; ?>
                </div>
                <div class="pb-5 text-center">
                    <form method='POST' action='./'>
                        <input type='hidden' name='action' value='BUY'>
                        <input type='hidden' name='PRODUCT_ID' value='<?= $product->id ?>'>
                        <input type='hidden' name='NAME' value='<?= $product->name ?>'>
                        <input type='hidden' name='PRICE' value='<?= $product->price->getAmount() ?>'>
                        <input type='hidden' name='CURRENCY' value='<?= $product->price->getCurrency() ?>'>
                        <button class="btn btn-primary" type='submit'><?= $arResult['isUpgrade'] ? $msg['upgrade_btn'] : $msg['buy'] ?></button>
                    </form>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>
<? if ($arResult['action'] === 'view') : ?>
    <div class="row">
        <? foreach ($arResult['elements'] as $item) : ?>
            <div class="col col-12 col-md-6 col-lg-3 d-flex flex-column mb-5">
                <div class="pricing-block pricing-ordinary bg-white rounded shadow-lg flex-grow-1 d-flex flex-column">
                    <div class="pricing-block-heading rounded d-flex align-items-center py-3" style="background: linear-gradient(to right, <?= $item['PROPERTIES']['BACKGROUND_GRADIENT_START'] ?>, <?= $item['PROPERTIES']['BACKGROUND_GRADIENT_END'] ?>)">
                        <h3 class="pt-1 w-100">
                            <?= $item['NAME'] ?>
                            <strong class="pt-3">
                                <?= FormatCurrency($item['CATALOG_PRICE']['PRICE'], $item['CATALOG_PRICE']["CURRENCY"]) ?>
                                <i class="fa fa-ruble"></i>
                            </strong>
                        </h3>
                    </div>
                    <div class="pricing-block-content px-1 pt-5 pb-4 flex-grow-1">
                        <? if (strpos($page, 'en/')) {
                            echo $item['DETAIL_TEXT'];
                        } else {
                            echo $item['PREVIEW_TEXT'];
                        }; ?>
                    </div>
                    <div class="pb-5 text-center">
                        <form method='POST' action='./'>
                            <input type='hidden' name='action' value='BUY'>
                            <input type='hidden' name='PRODUCT_ID' value='<?= $item['ID'] ?>'>
                            <input type='hidden' name='NAME' value='<?= $item['NAME'] ?>'>
                            <input type='hidden' name='PRICE' value='<?= $item['CATALOG_PRICE']['PRICE'] ?>'>
                            <input type='hidden' name='CURRENCY' value='<?= $item['CATALOG_PRICE']['CURRENCY'] ?>'>
                            <button class="btn btn-primary" type='submit'><?= $msg['buy'] ?></button>
                        </form>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? elseif ($arResult['action'] === 'info') : ?>

    <div class="row justify-content-center">
        <div class="col col-12 col-md-6 col-lg-4 d-flex flex-column mb-5">
            <div class="pricing-block pricing-ordinary bg-white rounded shadow-lg flex-grow-1 d-flex flex-column">
                <div class="pricing-block-heading rounded d-flex align-items-center py-3" style="background: linear-gradient(to right, <?= $arResult['product']['PROPERTIES']['BACKGROUND_GRADIENT_START'] ?>, <?= $arResult['product']['PROPERTIES']['BACKGROUND_GRADIENT_END'] ?>)">
                    <h3 class="pt-1 w-100">
                        <?= $msg['your_ticket'] ?>
                        <strong class="pt-3">

                            <?= $arResult['product']['NAME'] ?>
                            <? FormatCurrency($arResult['product']['CATALOG_PRICE']['PRICE'], $arResult['product']['CATALOG_PRICE']["CURRENCY"]) ?>

                        </strong>
                    </h3>
                </div>
                <div class="pricing-block-content px-5 pt-5 pb-4 flex-grow-1">
                    <? if (strpos($page, 'en/')) {
                        echo $arResult['product']['DETAIL_TEXT'];
                    } else {
                        echo $arResult['product']['PREVIEW_TEXT'];
                    }; ?>
                </div>
                <? if ($arResult['payment']) : ?>
                    <div class='pb-5 text-center'>
                        <div class="pricing-block-content px-5 pt-5 pb-4 flex-grow-1">
                            <?= $msg['we_remember'] ?>
                        </div>

                        <form method='POST' action='./'>
                            <input type='hidden' name='action' value='CANCEL'>
                            <input type='hidden' name='ORDER_ID' value='<?= $arResult['order'] ?>'>
                            <button class="btn btn-primary" type='submit'><?= $msg['pick_another'] ?></button>
                        </form>
                    </div>
                <? endif; ?>
            </div>
        </div>
        <? if ($arResult['paymentForm']) : ?>
            <div class="col col-12 col-md-6 col-lg-4 d-flex flex-column mb-5">
                <div class="pricing-block pricing-ordinary bg-white rounded shadow-lg flex-grow-1 d-flex flex-column">
                    <div class="pricing-block-content px-5 pt-5 pb-4 flex-grow-1 d-flex justify-content-center align-items-center text-center payment-form">
                        <?
                        ?>
                        <!--Start <? echo $arResult['product']['NAME']; ?> -->
                        <?

                        if ($arResult['product']['NAME'] == "STUDENT") { //Если пользователь выбрал билет типа "STUDENT"
                            global $USER;
                            $rsUser = CUser::GetByID($USER->GetID());
                            $arUser = $rsUser->Fetch();
                            $user_type = $arUser["UF_TYPE"];
                            $user_moderation = $arUser["UF_MODERATION"];
                            echo '<!--User type=' . $user_type . '/User moderation=' . $user_moderation . '.-->';

                            if ($user_type == 7) { // Если для пользователя установлен тип "Участник-студент" (id=7)

                                if ($user_moderation == 19) { //Если модерация студента пройдена (id=19)
                                    print_r($arResult['paymentForm']); //выводим форму для оплаты
                                } else {
                                    echo $msg['avalible_after_moderation'];
                                }
                            } else { // Для всех остальных типов пользователей, для билета типа STUDENT показываем сообщение:
                                echo $msg['not_avalible'];
                            }
                        } else { //Если пользователь выбрал любой другой билет (не STUDENT)
                            print_r($arResult['paymentForm']); //выводим форму для оплаты
                        }
                        ?>
                        <!--End -->

                    </div>
                </div>
            </div>
        <? endif; ?>
    </div>
<? endif; ?>
<!--
<? print_r($arResult) ?>
-->