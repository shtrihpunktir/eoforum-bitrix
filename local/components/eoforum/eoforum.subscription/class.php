<?

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc; 



if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
if (!\Bitrix\Main\Loader::includeModule("iblock")) return;

if (!Loader::includeModule("sale")) {
    ShowError(Loc::getMessage("SOA_MODULE_NOT_INSTALL"));
    return;
}

if (!Loader::includeModule('catalog')) {
    ShowError(Loc::getMessage("SOA_MODULE_NOT_INSTALL"));
    return;
}

if (!Loader::includeModule('iblock')) {
    ShowError(Loc::getMessage("IBLOCK_MODULE_NOT_INSTALL"));
    return;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'infrastructure.php';
use Ru\Eoforum\Subscription\Infrastructure\OrderService;
use Ru\Eoforum\Subscription\Infrastructure\BitrixOrmOrderRepository;
use Ru\Eoforum\Subscription\Infrastructure\BitrixOrmProductRepository;

Loc::loadMessages(__FILE__); 
class SubscriptionComponent extends \CBitrixComponent
{
    function executeComponent()
    {
        $productsRepo = new BitrixOrmProductRepository();
        $service = OrderService::instantiate(new BitrixOrmOrderRepository($productsRepo), $productsRepo);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            switch ($_POST['action']) {
                case 'CANCEL':
                    $service->cancelOrder($this->getUserId(), $_POST['ORDER_ID']);
                    break;
                case 'BUY':
                    $service->buyOrder($this->getUserId(), $_POST);
                    break;
            }
        }
        $form = $service->getOrderForm($this->getUserId());
        $this->arResult = $form;
        $rsUser = CUser::GetByID($this->getUserId());
        $arUser = $rsUser->Fetch();
        $this->arResult['userIsStudent'] = $arUser["UF_TYPE"] == 7;
        $this->arResult['userModerationSuccess'] = $arUser["UF_MODERATION"] == 19;
        $this->IncludeComponentTemplate();
    }
    function __executeComponent()
    {

        global $USER;
        if (!$USER->IsAuthorized()) {
            return LocalRedirect('/profile');
        }

        $this->arResult = [];
        $order = $this->getExistedOrder($this->getUserId());

        if ($order && !$_GET['force_new']) {
            $this->arResult['order'] = $order->getId();
            $this->arResult['action'] = 'info';
            $this->arResult['user'] = $this->getUserId();
            $this->arResult['isPaid'] = $order->isPaid();
            $this->arResult['product'] = $this->getOrderProduct($order);
            if (!$order->isPaid()) {

                if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['action'] === 'CANCEL') {
                    \Bitrix\Sale\Order::delete($order->getId());
                    $this->arResult['action'] = 'view';
                    $this->arResult['elements'] = $this->getIblockItems(
                        $this->getIblockId()
                    );
                } else {
                    $iterator = $order->getPaymentCollection()->getIterator();
                    $payment = $iterator->current();
                    $this->arResult['payment'] = $payment;
                    $this->arResult['paymentForm'] = $this->initiatePayment($payment);
                }
            }
        } else {
            if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['action'] === 'BUY') {
                $res = $this->doOrder($_POST);
                $order = $res['order'];
                $payment = $res['payment'];
                $this->arResult['order'] = $order->getId();
                $this->arResult['action'] = 'info';
                $this->arResult['user'] = $this->getUserId();
                $this->arResult['isPaid'] = $order->isPaid();
                $this->arResult['product'] = $this->getOrderProduct($order);
                $this->arResult['payment'] = $payment;
                $this->arResult['paymentForm'] = $this->initiatePayment($payment);
            } else {
                $this->arResult['action'] = 'view';
                $this->arResult['elements'] = $this->getIblockItems(
                    $this->getIblockId()
                );
            }
        }
        $this->IncludeComponentTemplate();
    }

    protected function getExistedOrder($userId)
    {
        $filter = [
            'filter' => [
                'USER_ID' => $userId
            ]
        ];

        $userOrders = \Bitrix\Sale\Order::loadByFilter($filter);
        return count($userOrders) > 0 ? $userOrders[0] : null;
    }

    protected function getOrderProduct($order)
    {

        $orderItem = $order->getBasket()->getBasketItems()[0];
        $productId = $orderItem->getField('PRODUCT_ID');
        if ($ob = CIBlockElement::GetByID($productId)->GetNextElement()) {
            $arFields = $ob->GetFields();
            $this->fillPrice($arFields);
            $this->fillProperties($arFields, $ob);
            return $arFields;
        }
        return null;
    }

    protected function doOrder($data)
    {
        $basket = Bitrix\Sale\Basket::create(SITE_ID);

        $basketUserId =  CSaleBasket::GetBasketUserID();
        CSaleBasket::DeleteAll($basketUserId);

        $item = $basket->createItem("catalog", $data["PRODUCT_ID"]);
        $item->setFields([
            'PRODUCT_ID' => $data['PRODUCT_ID'],
            'NAME' => $data['NAME'],
            'PRICE' => $data['PRICE'],
            'CURRENCY' => $data['CURRENCY'],
            'QUANTITY' => 1
        ]);
        $order = Bitrix\Sale\Order::create(SITE_ID, $this->getUserId());
        $order->setBasket($basket);

        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem(
            // @TODO Set paysystem from component params
            Bitrix\Sale\PaySystem\Manager::getObjectById(2)
        );

        $payment->setField("SUM", $order->getPrice());
        $payment->setField("CURRENCY", $order->getCurrency());

        $result = $order->save();
        if (!$result->isSuccess()) {
            return null;
        } else {
            return [
                'order' => $order,
                'payment' => $payment
            ];
        }
    }

    protected function initiatePayment(\Bitrix\Sale\Payment $payment)
    {
        $service = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        return $service->initiatePay($payment, $context->getRequest(), Bitrix\Sale\PaySystem\BaseServiceHandler::STRING)
            ->getTemplate();
    }

    protected function getIblockItems($iblockId)
    {
        $arSelect = array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_*");
        $arFilter = array("IBLOCK_ID" => IntVal($iblockId), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter);
        $elements = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $this->fillPrice($arFields);
            $this->fillProperties($arFields, $ob);
            $elements[] = $arFields;
        }
        return $elements;
    }

    protected function fillPrice(&$item)
    {
        $item['CATALOG_PRICE'] = GetCatalogProductPrice(
            $item['ID'],
            CCatalogGroup::GetBaseGroup()['ID']
        );
    }
    protected function fillProperties(&$item, $ob)
    {
        $props = $ob->GetProperties();
        foreach ($props as $key => $value) {
            $item['PROPERTIES'][$key] = $value['VALUE'] ? $value['VALUE'] : $value['DEFAULT_VALUE'];
        }
    }
    protected function getUserId()
    {
        global $USER;
        return $USER->GetId();
    }

    protected function getIblockId()
    {

        // @TODO Set iblock from component params
        return 3;
    }
}
