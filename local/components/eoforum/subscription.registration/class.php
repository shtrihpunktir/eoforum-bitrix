<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
require_once __DIR__ . '/../eoforum.subscription/core/infrastructure.php';

use Bitrix\Main\Engine\Contract\Controllerable;
use Ru\Eoforum\Subscription\Infrastructure\BitrixOrmOrderRepository;
use Ru\Eoforum\Subscription\Infrastructure\BitrixOrmProductRepository;
use Ru\Eoforum\Subscription\Infrastructure\OrderService;
Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
class SubscriptionRegistration extends \CBitrixComponent implements Controllerable
{
    public $arResult;

    // Обязательный метод
    public function configureActions()
    {
        // Сбрасываем фильтры по-умолчанию (ActionFilter\Authentication и ActionFilter\HttpMethod)
        // Предустановленные фильтры находятся в папке /bitrix/modules/main/lib/engine/actionfilter/
        return [
            'register' => [ // Ajax-метод
                'prefilters' => [],
            ],
        ];
    }

    // Ajax-методы должны быть с постфиксом Action
    public function registerAction($email, $password, $confirm_password, $subscriptionId)
    {
        $messages = $this->getMessages();

        $userRs = \CUser::GetByLogin($email);
        $errors = [];
        $user = $userRs->Fetch();

        if ($user) {
            $errors['email'] = $messages[LANGUAGE_ID]['invalid_email'];
        }
        if ($password !== $confirm_password) {
            $errors['confirm_password'] = $messages[LANGUAGE_ID]['does_not_match'];;
        }
        if (!empty($errors)) {
            return compact('errors');
        }
        $user = new \CUser;
        $ID = $user->Add([
            'EMAIL' => $email,
            'LOGIN' => $email,
            'PASSWORD' => $password,
            'CONFIRM_PASSWORD' => $confirm_password,
            'UF_TYPE' => 3,
            'LID' => SITE_ID
        ]);
        if (!$ID) {
            throw new \Exception($user->LAST_ERROR);
        }

        $productsRepo = new BitrixOrmProductRepository();
        $service = OrderService::instantiate(new BitrixOrmOrderRepository($productsRepo), $productsRepo);
        $product = $productsRepo->findById($subscriptionId);
        $service->buyOrder($ID, [
            'PRODUCT_ID' => $product->id,
            'NAME' => $product->name,
            'PRICE' => $product->price->getAmount(),
            'CURRENCY' => $product->price->getCurrency()
        ]);
        $form = $service->getOrderForm($ID);
        return [
            'ID' => $ID,
            'order' => $form['order'],
            'paymentForm' => $form['paysystem']->getForm($form['order'])
        ];
//        return $user;
    }

    public function executeComponent()
    {
        $this->arResult['ID'] = $_REQUEST['id'];
        $this->arResult['lang'] = $_REQUEST['lang'];
        $this->arResult['messages'] = $this->getMessages();
        $this->includeComponentTemplate();
    }

    private function getMessages()
    {
        return [
            'en' => [
                'email' => 'E-mail',
                'password' => 'Password',
                'password_again' => 'Password again',
                'signup' => 'Sign up',
                'sign_in' => 'Sign in',
                'registration' => 'Registration',
                'invalid_email'=> 'Email already exists',
                'does_not_match'=> 'Password does not match',
            ],
            'ru' => [
                'email' => 'E-mail',
                'password' => 'Пароль',
                'password_again' => 'Пароль повторно',
                'signup' => 'Зарегистрироваться',
                'sign_in' => 'Войти',
                'registration' => 'Регистрация',
                'invalid_email'=> 'Пользователь с таким E-mail уже существует',
                'does_not_match'=> 'Пароли не совпадают',
            ]
        ];
    }
}
