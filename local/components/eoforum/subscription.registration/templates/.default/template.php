<?php
global $APPLICATION;
CUtil::InitJSCore(array('ajax', 'popup'));
$popupId = "subscription_registration_form";


$lang = $arResult['lang'];
$messages = $arResult['messages'];

$subscriptionId = $arResult['ID'];
?>
<script>

    $(function () {
        $('.input-fl .form-control').on('focus', function () {
            $(this).parent().addClass('not-empty');
        }).on('focusout', function () {
            if ($(this).val() == '') {
                $(this).parent().removeClass('not-empty');
            }
        });
    });

    var SUBSCRIPTION_REGISTRATION_FORM = 'subscription_registration_form';
    if (!window.buyTicket) {
        function buyTicket(subscriptionId, event) {
            event.preventDefault();
            BX.ajax
                .runComponentAction('eoforum:subscription.registration', 'register', {
                    mode: 'class',
                    data: {
                        email: event.target.querySelector('[name="EMAIL"]').value,
                        password: event.target.querySelector('[name="PASSWORD"]').value,
                        confirm_password: event.target.querySelector('[name="CONFIRM_PASSWORD"]').value,
                        subscriptionId: subscriptionId
                    }
                })
                .then((result) => {
                    if (result.data.errors) {
                        var errorsContainer = event.target.getElementsByClassName('error')[0];
                        errorsContainer.innerHTML = '';
                        Object.values(result.data.errors).forEach(error => {
                            var errorEl = document.createElement('p');
                            errorEl.innerText = error;
                            errorsContainer.append(errorEl);
                        })
                    } else {
                        console.log(result.data)
                        if (popup = BX.PopupWindowManager.getCurrentPopup()) {
                            popup.destroy();
                        }
                        payment = BX.PopupWindowManager.create('payment', window.body, {
                            content: "<div class='bootstrap'><div class='p-5 text-center'>" + result.data.paymentForm + "</div></div>",
                            autoHide: false,
                            lightShadow: true,
                            closeIcon: true,
                            closeByEsc: false,
                            overlay: {
                                backgroundColor: '#000',
                                opacity: 75
                            }
                        });
                        // payment.setContent(result.data.paymentForm);
                        payment.show();
                        payment.adjustPosition();
                    }
                })
                .catch(result => {
                    console.log(result);
                    event.target.getElementsByClassName('error')[0].innerHTML = result.errors[0].message;
                });
            return false;
        }
    }
</script>
<h1 class="text-center mb-5 pt-3"><?= $messages[$lang]['registration'] ?></h1>
<form onsubmit="buyTicket('<?= $subscriptionId ?>', event)">
    <div class="error text-danger"></div>
    <div class="form-group">
        <div class="input-fl">
            <input class="form-control" name="EMAIL" id="email">
            <label class="f-label" for="email"><?= $messages[$lang]['email']; ?></label>
        </div>
    </div>
    <div class="form-row">
        <div class="col-12 col-sm-6">
            <div class="form-group">
                <div class="input-fl">
                    <input type="password"
                           name="PASSWORD"
                           id="password"
                           autocomplete="off"
                           required
                           class="form-control form-control-lg"
                           placeholder="">
                    <label for="password" class="f-label"><?= $messages[$lang]['password']; ?>*</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group">
                <div class="input-fl">
                    <input type="password"
                           name="CONFIRM_PASSWORD"
                           id="confirm_password"
                           autocomplete="off"
                           required
                           class="form-control form-control-lg"
                           placeholder="">
                    <label for="confirm_password" class="f-label"><?= $messages[$lang]['password_again']; ?>*</label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" required id="customCheck1">
            <label class="custom-control-label" for="customCheck1">Настоящим подтверждаю, что я ознакомлен(а) и согласен(на)<br>
                с  условиями <a target="_blank" href="https://www.polylog.ru/ru/company/privacy-policy#consent">политики конфиденциальности</a></label>
        </div>
    </div>
    <div class="form-group">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" required id="customCheck2">
            <label class="custom-control-label" for="customCheck2">Настоящим подтверждаю, что я ознакомлен(а) и согласен(на)<br>
                с  <a target="_blank" href="https://yadi.sk/i/Q-bWT36uR04CCQ">пользовательским соглашением</a></label>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" name="register_submit_button"
                class="btn btn-primary btn-lg btn-block">
            <?= $messages[$lang]['signup']; ?>
        </button>
    </div>
    <div class="text-center">
        <a href="/<?= $lang === 'en' ? 'en/' : '' ?>login"><?= $messages[$lang]['sign_in'] ?></a>
    </div>
</form>