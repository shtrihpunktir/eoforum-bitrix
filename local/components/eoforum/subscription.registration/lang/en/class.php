<?
global $MESS;
$MESS["SALE_HPS_ROBOXCHANGE_TEMPL_TITLE"] = "You want to pay through the system <b>www.roboxchange.net</b>";
$MESS["SALE_HPS_ROBOXCHANGE_TEMPL_ORDER"] = "Account number";
$MESS["SALE_HPS_ROBOXCHANGE_TEMPL_TO_PAY"] = "Amount to pay by invoice:";
$MESS["SALE_HPS_ROBOXCHANGE_TEMPL_WARN"] = "<i>Внимание!</i> Возврат средств по платежной системе www.roboxchange.net невозможен, пожалуйста, будьте внимательны при оплате заказа.</font>";
$MESS["SALE_HPS_ROBOXCHANGE_TEMPL_BUTTON"] = "Pay";

$MESS["SALE_RES_NUMBER"] = "Номер счёта в магазине";
$MESS["SALE_RES_DATEPAY"] = "Дата платежа";
$MESS["SASP_RES_PAYED"] = "Оплачен";
$MESS["SASP_RES_PAY_TYPE"] = "Способ оплаты";
?>