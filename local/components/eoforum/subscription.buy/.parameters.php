<?php

CModule::IncludeModule("iblock");;

$ob = \CIBlockElement::GetList(['SORT' => 'ASC'], ['ACTIVE' => 'Y', 'IBLOCK_CODE' => 'services_s1']);
$elements = [];
while ($rs = $ob->getNextElement()) {
    $el = $rs->GetFields();
    $elements[$el['ID']] = $el['NAME'];
}
$arComponentParameters = [
    "PARAMETERS" => [
        "REGISTER_FORM_URL" => [
            "NAME" => "Страница с формой быстрой регистрации",
            "TYPE" => "FILE",
            "DEFAULT" => "/local/api/registration-form.php",
            "PARENT" => "DATA_SOURCE"
        ],
        "SUBSCRIPTION_ID" => [
            "NAME" => "Тип подписки",
            "TYPE" => "LIST",
            "PARENT" => "DATA_SOURCE",
            "VALUES" => $elements
        ]
    ]
];