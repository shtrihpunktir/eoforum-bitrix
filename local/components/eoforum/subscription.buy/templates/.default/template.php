<?php
global $APPLICATION;
CUtil::InitJSCore(array('ajax', 'popup'));
$popupId = "subscription_registration_form";
$subscriptionId = $arParams['SUBSCRIPTION_ID'];
$lang = 'ru';
$currPage = trim($APPLICATION->GetCurPage(), '/');

if (explode('/', $currPage)[0] === 'en') {
    $lang = 'en';
}
$messages = [
    'ru' => [
        'buy' => 'Купить билет'
    ],
    'en' => [
        'buy' => 'Buy ticket'
    ]
];
?>
<script>
    var SUBSCRIPTION_REGISTRATION_FORM = 'subscription_registration_form';
    if (!BX.PopupWindowManager.isPopupExists(SUBSCRIPTION_REGISTRATION_FORM)) {
        var popup = BX.PopupWindowManager.create(SUBSCRIPTION_REGISTRATION_FORM, window.body, {
            content: '',
            autoHide: true,
            lightShadow: true,
            closeIcon: true,
            closeByEsc: true,
            overlay: {
                backgroundColor: '#000',
                opacity: 75
            }
        });
    }
    if (!window.showRegistrationPopup) {
        function showRegistrationPopup(subscriptionId) {
            popup.show();
            var req = BX.ajax.insertToNode("<?=$arParams['REGISTER_FORM_URL']?>?id=" + subscriptionId + '&lang=<?=$lang?>', popup.popupContainer);
            req.addEventListener('load', function () {
                popup.adjustPosition();
            })
        }
    }
</script>
<div class="buy-ticket">
    <button class="btn btn-primary" type="button"
            onclick="showRegistrationPopup('<?= $subscriptionId ?>')">
        <?= $messages[$lang]['buy'] ?>
    </button>
</div>