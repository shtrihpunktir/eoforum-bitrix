<?php
require_once __DIR__ . '/../components/eoforum/eoforum.subscription/core/infrastructure.php';

use Bitrix\Main\Diag\Debug;
use Ru\Eoforum\Subscription\Infrastructure\OrderService;
use Ru\Eoforum\Subscription\Infrastructure\BitrixOrmOrderRepository;
use Ru\Eoforum\Subscription\Infrastructure\BitrixOrmProductRepository;

CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
AddEventHandler("main", "OnBeforeUserUpdate", array("UsersModerationHandlers", "BeforeSpeakerUpdate"));
AddEventHandler("main", "OnBeforeUserUpdate", array("UsersModerationHandlers", "BeforeMediaUpdate"));

class UserWrapper
{
    protected $user;

    function __construct($id)
    {
        $rsUser = CUser::GetByID($id);
        $this->user = $rsUser->Fetch();
    }

    public function getId()
    {
        return $this->user['ID'];
    }

    public function isSpeaker()
    {
        return $this->user['UF_TYPE'] == 4;
    }

    public function isMedia()
    {
        return $this->user['UF_TYPE'] == 5;
    }

    public function isPendingModeration()
    {
        return $this->user["UF_MODERATION"] == 18;
    }
}


class UsersModerationHandlers
{
    protected $changes;

    static function debug($var, $varname)
    {
        Debug::dumpToFile($var, $varname, 'local' . DIRECTORY_SEPARATOR . 'moderation.log');
    }

    function __construct($changes)
    {
        $this->changes = $changes;
    }

    private function getProductByCode($code)
    {
        return ProductFinder::getProductByCode($code);
    }

    static function getOrdersService()
    {
        $productsRepo = new BitrixOrmProductRepository();
        return OrderService::instantiate(new BitrixOrmOrderRepository($productsRepo), $productsRepo);
    }

    function isModerationDone()
    {
        return $this->changes["UF_MODERATION"] == 19;
    }

    function createPrepaidOrder($product)
    {
        $productData = $this->getProductByCode($product);
        $result = self::getOrdersService()->buyOrder($this->changes['ID'], $productData);
        if (!$result) return;
        $order = $result['order'];
        $payment = $result['payment'];
        $payment->setPaid("Y");
        $order->save();
    }

    static function BeforeSpeakerUpdate(&$arFields)
    {
        $user = new UserWrapper($arFields['ID']);
        if (!$user->isSpeaker() || !$user->isPendingModeration()) return;
        self::debug('speaker waiting moderation', $user->getId());

        $handler = new self($arFields);
        if (!$handler->isModerationDone()) return;

        try {
            $handler->createPrepaidOrder('speaker');
        } catch (\Exception $e) {
            Debug::dumpToFile($e, null, 'local' . DIRECTORY_SEPARATOR . 'moderation.log');
        }
    }

    static function BeforeMediaUpdate(&$arFields)
    {
        self::debug($arFields, 'MEDIA update input');
        $user = new UserWrapper($arFields['ID']);
        self::debug($user, 'MEDIA update');
        if (!$user->isMedia() || !$user->isPendingModeration()) return;
        self::debug($user->getId(), 'MEDIA waiting moderation');

        $handler = new self($arFields);
        if (!$handler->isModerationDone()) return;

        try {
            $handler->createPrepaidOrder('media');
        } catch (\Exception $e) {
            Debug::dumpToFile($e, null, 'local' . DIRECTORY_SEPARATOR . 'moderation.log');
        }
    }
}
