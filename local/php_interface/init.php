<?php

use Bitrix\Main\Diag\Debug;
use \Bitrix\Main\Security\Sign\Signer;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'local' . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'ticket' . DIRECTORY_SEPARATOR . 'index.php';
require __DIR__.DIRECTORY_SEPARATOR.'moderation.php';
define('TICKET_FONTS_PATH', $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'local' . DIRECTORY_SEPARATOR . 'ticket-fonts' . DIRECTORY_SEPARATOR);

use Ru\Eoforum\Subscription\Infrastructure;

CModule::IncludeModule('bx_phpmailer');

// swap lines to assign new ticket functionality
//AddEventHandler("sale", "OnSaleOrderPaid", array("AppEventHandlers", "OnSaleOrderPaid"));
AddEventHandler("sale", "OnSaleOrderPaid", array("TicketAssigner", "handlePaid"));

AddEventHandler("main", "OnBeforeUserRegister", array("AppEventHandlers", "OnBeforeUserRegisterHandler"));
AddEventHandler("main", "OnBeforeUserUpdate", array("AppEventHandlers", "OnBeforeUserUpdateHandler"));
AddEventHandler("main", "OnAfterUserUpdate", array("AppEventHandlers", "OnAfterUserUpdateHandler"));
AddEventHandler("main", "OnAfterUserAdd", Array("AppEventHandlers", "OnAfterUserAddHandler"));

class UserProfileEmptyException extends \Exception
{
}

class AppEventHandlers
{
    function OnAfterUserAddHandler($arFields)
    {
        if (!empty($arFields['TICKET'])) {
            try {
                // найти подписку
                $product = ProductFinder::getProductByCode($arFields['TICKET']);
                // созадть заказ
                $result = OrderServiceProvider::provide()->buyOrder($arFields['ID'], $product);
                if (!$result) return;

                $order = $result['order'];
                $payment = $result['payment'];

                // оплатить заказ
                // после оплаты генерится файл билета
                $payment->setPaid("Y");
                $order->save();

                // пометить билет импортированным
                $ticket = \CIblockElement::GetList(
                    ["SORT" => "ASC"],
                    [
                        "PROPERTY_ORDER_ID" => $order->getId(),
                        "IBLOCK_CODE" => 'BARCODE_TICKETS'
                    ]
                )->Fetch();
                CIBlockElement::SetPropertyValuesEx($ticket['ID'], $ticket['IBLOCK_ID'], [
                    'IMPORTED' => 3
                ]);

            } catch (\Exception $e) {
                Debug::dumpToFile($e, 'OnAfterUserAddHandler error', 'local' . DIRECTORY_SEPARATOR . 'add.user.log');
                global $APPLICATION;
                $APPLICATION->throwException($e->getMessage());
                return false;
            }
        }
        return true;
    }

    function OnBeforeUserRegisterHandler(&$arFields)
    {
        $arFields["LOGIN"] = $arFields["EMAIL"];
        $arFields["UF_MODERATION"] = 18;
        if ($arFields["WORK_COMPANY"] == 'google'){
            $GLOBALS['APPLICATION']->ThrowException('Произошла неизвестная ошибка');
            return false;
        }
    }

    function OnSaleOrderPaid(&$order)
    {
        $orderItem = $order->getBasket()->getBasketItems()[0];
        $productId = $orderItem->getField('PRODUCT_ID');
        $arFilter = array('IBLOCK_ID' => 5, 'GLOBAL_ACTIVE' => 'Y', 'UF_SUBSCRIPTION_ID' => $productId);
        $barCodeSectionQuery = CIBlockSection::GetList(
            array("SORT" => "ASC"),
            $arFilter
        );
        $barCodesSection = $barCodeSectionQuery->GetNext();

        $elementQuery = CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => 5,
                'SITE_ID' => SITE_ID,
                'SECTION_ID' => $barCodesSection['ID'],
                'PROPERTY_order_id' => false
            )
        );
        $ticket = $elementQuery->GetNext();

        if ($ticket) {
            CIBlockElement::SetPropertyValues($ticket['ID'], $ticket['IBLOCK_ID'], $order->getId(), 'order_id');
            CIBlockElement::SetPropertyValues($ticket['ID'], $ticket['IBLOCK_ID'], $order->getUserId(), 'user_id');
            $arEventFields = array(
                'TICKET_IMAGE_URL' => CFile::GetPath($ticket['PREVIEW_PICTURE']),
                'EMAIL' => CUser::getById($order->getUserId())->Fetch()['EMAIL'],
                'ORDER_ID' => $order->getField("ID"),
                'PRICE' => $order->getField("PRICE"),
                'ORDER_DATE' => $order->getField("DATE_INSERT"),

            );

            CEvent::Send('SALE_ORDER_TICKET_ASSIGNED', $order->getSiteId(), $arEventFields);
        }
    }

    function OnBeforeUserUpdateHandler(&$arFields)
    {
        // $arCheckingFields = array("UF_MODERATION");
        $rsUser = CUser::GetByID($arFields["ID"]);
        $arUser = $rsUser->Fetch();
        $arUserFields = array(
            "USER_NAME" => $arUser["NAME"],
            "USER_LAST_NAME" => $arUser["LAST_NAME"],
            "USER_EMAIL" => $arUser["EMAIL"],
            "LID" => $arUser["LID"],

        );
        if ($arUser["UF_TYPE"] == 7) {

            if ($arUser["UF_MODERATION"] == 18 && $arFields["UF_MODERATION"] == 19) {
                global $APPLICATION;
                CEvent::Send('STUDENT_MODERATION', $arUserFields['LID'], $arUserFields);
                return;
            } elseif ($arUser["UF_MODERATION"] == 18 && $arFields["UF_MODERATION"] == 20) {
                global $APPLICATION;
                CEvent::Send('STUDENT_MODERATION_NOT_OK', $arUserFields['LID'], $arUserFields);
                return;
            }
        }
        if ($arUser["UF_TYPE"] == 4 || $arUser["UF_TYPE"] == 5) {

            if ($arUser["UF_MODERATION"] == 18 && $arFields["UF_MODERATION"] == 19) {
                global $APPLICATION;
                CEvent::Send('SPEAKER_MODERATION', $arUserFields['LID'], $arUserFields);
                return;
            } elseif ($arUser["UF_MODERATION"] == 18 && $arFields["UF_MODERATION"] == 20) {
                global $APPLICATION;
                CEvent::Send('SPEAKER_MODERATION_NOT_OK', $arUserFields['LID'], $arUserFields);
                return;
            }
        }

    }

    function OnAfterUserUpdateHandler(&$arFields)
    {

        $service = OrderServiceProvider::provide();
        try {
            $order = $service->getOrdersRepo()->load($arFields['ID']);
            if ($order) {
                TicketAssigner::handlePaid(\Bitrix\Sale\Order::load($order->getId()));
            }
        } catch (\Exception $exception){
            $GLOBALS['APPLICATION']->ThrowException($exception->getMessage());
            return false;
        }
    }
}

;

class TicketAssigner
{
    static function handlePaid($order)
    {
        $assigner = new self();
        self::debug($order->getId(), 'order id');
        self::debug($order->getUserId(), 'order user');
        self::debug($order->isPaid(), 'order is paid');

        $assigner->assign($order);
    }

    static function debug($var, $varname)
    {
        Debug::dumpToFile($var, $varname, 'local' . DIRECTORY_SEPARATOR . 'order-status.log');
    }

    function assign($order)
    {
        if (!$order->isPaid()) {
            return;
        }
        $ticket = $this->loadTicket($order);

        if (!$ticket['PROPERTIES']['TICKET_FILE']['VALUE']) {
            $user = CUser::GetById($order->getUserId())->fetch();
            try {

                $image = $this->generateTicketImage($order, $user, $ticket['NAME']);
                $fid = $this->saveFile($image);
                CIBlockElement::SetPropertyValuesEx($ticket['ID'], $ticket['IBLOCK_ID'], [
                    'TICKET_FILE' => CFile::MakeFileArray($fid)
                ]);

                $arEventFields = array(
                    'EMAIL' => $user['EMAIL'],
                    'ATTACHMENTS' => 'Ticket.pdf:' . CFile::GetFileArray($fid)['SRC'],
                    'ORDER_ID' => $order->getField("ID"),
                    'PRICE' => $order->getField("PRICE"),
                    'ORDER_DATE' => $order->getField("DATE_INSERT"),
                );
                CEvent::Send(
                    'SALE_ORDER_TICKET_BARCODE_ASSIGNED',
                    $order->getSiteId(),
                    $arEventFields
                );
            } catch (UserProfileEmptyException $e) {

                $signer = new Signer();
                $packed = $signer->pack([
                    'uid' => $user['ID'],
                    'oid' => $order->getId()
                ]);
                $signedUid = $signer->sign($packed);

                $hash = $signedUid;
                $urlPrefix = '';
                if ($user['LID']) {
                    $site = \CSite::GetById($user['LID'])->fetch();
                    if ('ru' !== strtolower($site['LANGUAGE_ID'])) {
                        $urlPrefix = '/' . strtolower($site['LANGUAGE_ID']);
                    }
                }
                $arEventFields = array(
                    'EMAIL' => $user['EMAIL'],
                    'LINK' => "{$urlPrefix}/profile/end-registration.php?HASH={$hash}",
                );
                CEvent::Send(
                    'SALE_ORDER_TICKET_PROFILE_REQUIRED',
                    $order->getSiteId(),
                    $arEventFields
                );
            }
        }
    }

    protected function generateTicketImage($order, $user, $code)
    {
        if (empty($user['NAME']) || empty($user['LAST_NAME'])) {
            throw new UserProfileEmptyException();
        }
        $orderItem = $order->getBasket()->getBasketItems()[0];
        $productId = $orderItem->getField('PRODUCT_ID');
        // get subscription

        $query = CIBlockElement::GetList(
            [],
            ['ID' => $productId],
            false,
            [],
            ["ID", "IBLOCK_ID", 'NAME', 'PROPERTY_TICKET_TEMPLATE_FILE.PROPERTY_TEMPLATE_FILE']
        );
        if (!$ob = $query->GetNextElement()) {
            die('Template not exists');
        }
        $fields = $ob->getFields();
        $templatePath = CFile::GetPath($fields['PROPERTY_TICKET_TEMPLATE_FILE_PROPERTY_TEMPLATE_FILE_VALUE']);

        $generator = Ticket\generatorFactory([
            'scale' => 4, // масштаб баркода
            'text_size' => 19, // размер текста
            'text_margin' => 20, // отступ между текстом и низом барода
        ]);

        $generator->templatePath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $templatePath;
        $image = $generator->generate($code);

        $userDataGenerator = new  \Ticket\UserDataGenerator();
        $userDataGenerator->leftOffset = 1280;
        $userDataGenerator->topOffset = 1150;
        $userDataGenerator->fontPath = TICKET_FONTS_PATH . 'Roboto-Black.ttf';
        $userDataGenerator->attachTo($user['NAME'] . "\n" . $user['LAST_NAME'], $image);

        $userDataGenerator->topOffset = 1150 + $userDataGenerator->fontSize * 2 + 40;
        $userDataGenerator->fontSize = 40;
        $userDataGenerator->fontPath = TICKET_FONTS_PATH . 'Roboto-Bold.ttf';
        $userDataGenerator->attachTo($user['WORK_COMPANY'] ?? '', $image);

        return $image;
    }

    protected function saveFile($image)
    {
        $tmpImageName = tempnam(sys_get_temp_dir(), 'ticket');
        imagejpeg($image, $tmpImageName);


        $pdf = new Imagick(realpath($tmpImageName));
        $pdf->setImageFormat('pdf');
        $tmpPdfName = tempnam(sys_get_temp_dir(), 'ticket_') . ".pdf";
        $pdf->writeImages($tmpPdfName, true);

        $arFile = CFile::MakeFileArray($tmpPdfName, false, false, '');

        $fid = CFile::SaveFile($arFile, "/tickets", false, false);
        unlink($tmpImageName);
        unlink($tmpPdfName);

        return $fid;
    }


    protected function loadTicket($order)
    {
        $query = CIblockElement::GetList(
            ["SORT" => "ASC"],
            [
                "PROPERTY_ORDER_ID" => $order->getId(),
                "IBLOCK_CODE" => 'BARCODE_TICKETS'
            ]
        );
        $ob = $query->GetNextElement();
        if (!$ob) {
            // найти билет, который никому не назначен
            $query = CIblockElement::GetList(
                ["SORT" => "ASC"],
                [
                    "PROPERTY_USER" => false,
                    "IBLOCK_CODE" => 'BARCODE_TICKETS'
                ]
            );

            $ob = $query->GetNextElement();

            if (!$ob) {
                throw new Exception('no available tickets');
            }
            $ticket = $ob->getFields();
            // назначить билету пользователя и заказ
            CIBlockElement::SetPropertyValuesEx($ticket['ID'], $ticket['IBLOCK_ID'], [
                'USER' => $order->getUserId(),
                'ORDER_ID' => $order->getId()
            ]);
            $ticket['PROPERTIES'] = $ob->getProperties();
        } else {
            $ticket = $ob->getFields();
            $ticket['PROPERTIES'] = $ob->getProperties();
        }
        return $ticket;
    }
}

class OrderServiceProvider
{
    private static $instance;

    static function provide(): Infrastructure\OrderService
    {
        if (!self::$instance) {
            $productsRepo = new Infrastructure\BitrixOrmProductRepository();
            self::$instance = Infrastructure\OrderService::instantiate(
                new Infrastructure\BitrixOrmOrderRepository($productsRepo),
                $productsRepo
            );
        }
        return self::$instance;
    }
}


class ProductFinder
{
    static function getProductByCode($code)
    {
        $arFilter = array("IBLOCK_CODE" => 'services_s1', "CODE" => $code);
        $res = \CIBlockElement::GetList(array(), $arFilter);
        if (!$ob = $res->GetNextElement()) {
            throw new \Exception('not exists');
        }
        $arProductFields = $ob->GetFields();
        $price = \GetCatalogProductPrice(
            $arProductFields['ID'],
            \CCatalogGroup::GetBaseGroup()['ID']
        );
        return [
            'PRODUCT_ID' => $arProductFields['ID'],
            'NAME' => $arProductFields['NAME'],
            'PRICE' => $price['PRICE'],
            'CURRENCY' => $price['CURRENCY']
        ];
    }
}