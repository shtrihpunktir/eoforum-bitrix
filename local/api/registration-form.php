<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;
?>
<div id="popup-form">
    <div class="bootstrap">
        <div class="p-5">
            <?php
            $APPLICATION->IncludeComponent(
                "eoforum:subscription.registration",
                "",
                Array(
                    "AUTH" => "N",
                    "REQUIRED_FIELDS" => array("EMAIL", "NAME", "LAST_NAME", "PERSONAL_CITY", "PERSONAL_PHONE"),
                    "SET_TITLE" => "N",
                    "SHOW_FIELDS" => array("EMAIL"),
                    "SUCCESS_PAGE" => "/signup/?success=Y",
                    "USER_PROPERTY" => array(),
                    "USER_PROPERTY_NAME" => "",
                    "USE_BACKURL" => "N"
                )
            );
            ?>
        </div>
    </div>
</div>
<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>
