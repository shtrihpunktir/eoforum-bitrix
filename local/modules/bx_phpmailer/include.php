<?php
use Bitrix\Main\Diag\Debug;
//IncludeModuleLangFile(__FILE__);
if (!function_exists('custom_mail') && class_exists('PHPMailer')) {
    function custom_mail($to, $subject, $message, $additional_headers, $additional_parameters)
    {
        Debug::writeToFile('Пытаюсь послать');
        $headers = explode("\n", $additional_headers);
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);
        if (COption::GetOptionString('bx_phpmailer', 'isSMTP') === 'Y') {
            $mail->IsSMTP(); // telling the class to use SMTP
        }
        $mail->Host = COption::GetOptionString('bx_phpmailer', 'Host');
        $mail->Port = COption::GetOptionString('bx_phpmailer', 'Port');
        $mail->SMTPSecure = 'ssl';
        if (COption::GetOptionString('bx_phpmailer', 'SMTPAuth') === 'Y') {
            $mail->SMTPAuth = true;
            $mail->Username = COption::GetOptionString('bx_phpmailer', 'Username');
            $mail->Password = COption::GetOptionString('bx_phpmailer', 'Password');
        }

        foreach ($headers as $header) {
            $data = explode(": ", $header);
            if ('BCC' === $data[0]) {
                $mail->addBCC($data[1]);
            };
            if ('BCC_2' === $data[0]) {
                $mail->addCC($data[1]);
            }
            if ('CC' === $data[0]) {
                $mail->addCC($data[1]);
            }
            if ('BX_ATTACHMENTS' === $data[0]) {
                try {
                    $files = explode(';', $data[1]);

                    Debug::writeToFile($files);
                    foreach ($files as $file) {
                        list($title, $path) = explode(':', $file);
                        $mail->AddAttachment(
                            $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $path,
                            $title
                        );
                    }
                } catch (\Exception $e) { }
            }
        }
        //    $message = iconv('cp1251', 'utf8', $message);
        $mail->AddAddress($to);
        $mail->Body = $message;
        $mail->Subject = $subject;
        $mail->From = COption::GetOptionString('bx_phpmailer', 'From');
        $mail->FromName = COption::GetOptionString('bx_phpmailer', 'FromName');

        if (!$mail->send()) {
            Debug::writeToFile(
                'To: ' . $to . PHP_EOL .
                    'Subject: ' . $subject . PHP_EOL .
                    'Message: ' . $message . PHP_EOL .
                    'Headers: ' . $additional_headers . PHP_EOL .
                    'Params: ' . $additional_parameters . PHP_EOL .
                    'ERROR: ' . $mail->ErrorInfo,
                'bx_phpmailer'
            );
            return false;
        } else {
            Debug::writeToFile(
                'To: ' . $to . PHP_EOL .
                    'Subject: ' . $subject . PHP_EOL .
                    'Message: ' . $message . PHP_EOL .
                    'Headers: ' . $additional_headers . PHP_EOL .
                    'Params: ' . $additional_parameters . PHP_EOL .
                    'SENT' . PHP_EOL,
                'bx_phpmailer'
            );
            return true;
        }
    }
}
