<?
$MESS["PHPMAILER_IS_SMPT"] = "Использовать SMTP";
$MESS["PHPMAILER_HOST"] = "Адрес сервера";
$MESS["PHPMAILER_PORT"] = "Порт";
$MESS["PHPMAILER_SMTPAUTH"] = "Использовать авторизацию";
$MESS["PHPMAILER_USERNAME"] = "Пользователь";
$MESS["PHPMAILER_PASSWORD"] = "Пароль";

$MESS['PHPMAILER_FROM'] = 'Отправитель';
$MESS['PHPMAILER_FROMNAME'] = 'Имя отправителя';
?>