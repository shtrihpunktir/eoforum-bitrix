<?php

class bx_phpmailer extends CModule
{
  var $MODULE_NAME = 'Почта PHPMailer';
  var $MODULE_DESCRIPTION;
  var $MODULE_VERSION = '0.0.1';
  var $MODULE_VERSION_DATE;
  var $MODULE_ID = 'bx_phpmailer';
  var $MODULE_SORT = 10000;

  function DoInstall()
  {
    global $APPLICATION;
    RegisterModule($this->MODULE_ID);
    $APPLICATION->IncludeAdminFile("Установка модуля " . $this->MODULE_NAME, __DIR__ . DIRECTORY_SEPARATOR . "step.php");
    return true;
  }

  public function DoUninstall()
  {
    global $APPLICATION;
    UnRegisterModule($this->MODULE_ID);
    $APPLICATION->IncludeAdminFile("Удаление модуля " . $this->MODULE_NAME, __DIR__ . DIRECTORY_SEPARATOR . "unstep.php");
  }
}