<?php
namespace Ticket;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'barcode.php';

interface Generator
{
    function generate(String $code);
}
interface GeneratorWrapper
{
    function __construct(Generator $wrapped);
}

class Composer
{
    private $layers = [];
    function __construct($layers)
    {
        $this->layers = $layers;
    }
}

class TicketGenerator implements Generator, GeneratorWrapper
{
    private $wrapped;
    public $templatePath = __DIR__ . DIRECTORY_SEPARATOR . 'ticket-template-levelup.jpg';
    function __construct(Generator $wrapped)
    {
        $this->wrapped = $wrapped;
    }
    function generate(String $code)
    {
        $image = $this->wrapped->generate($code);
        $tpl = imagecreatefromjpeg($this->templatePath);

        $codewidth = imagesx($image);
        $codeheight = imagesy($image);

        imagecopy(
            $tpl,
            $image,
            1240 + (1240 - $codewidth) / 2,
            1050 + (750 - $codeheight) * .90,
            0,
            0,
            imagesx($image),
            imagesy($image)
        );
        return $tpl;
    }
}

class BarCodeGenerator implements Generator
{
    private $generator;
    private $options;
    function __construct($options)
    {
        $this->generator = new \barcode_generator();
        $this->options = $options;
    }
    private function generateEan13($code)
    {
        if (strlen($code) !== 12) {
            throw new \Exception('Invalid code');
        }

        $ids = str_split($code); // ida = input data array, разбиваем строку на массив
        $step1 = $ids[1] + $ids[3] + $ids[5] + $ids[7] + $ids[9] + $ids[11]; // складываем четные разрядыы
        $step2 = $step1 * 3; // умножаем результат на 3
        $step3 = $ids[0] + $ids[2] + $ids[4] + $ids[6] + $ids[8] + $ids[10]; // складываем нечетные разряды
        $step4 = $step2 + $step3; // складываем 2 и 3 шаги

        if ($step4 % 10 != 0) { // нам нужна последняя цифра, если она не 0
            $controlSum = 10 - $step4 % 10; // находим цифру, которая дополнит её до 10, это и будет контрольная цифра
        } else {
            $controlSum = 0; // если это 0, то он так и остается 0
        }
        return $code . $controlSum;
    }

    function generate(String $code)
    {
        $code = $this->generateEan13($code);
        $symbology = 'ean-13';
        /* Create bitmap image. */
        return $this->generator->render_image($symbology, $code, $this->options);
    }
}

class UserDataGenerator
{
    public $leftOffset = 0;
    public $topOffset = 0;
    public $fontSize = 64;
    public $fontPath = __DIR__ . DIRECTORY_SEPARATOR . 'Roboto-Black.ttf';

    function attachTo($text, $image)
    {
        $black = imagecolorallocate($image, 0, 0, 0);
        imagettftext($image, $this->fontSize, 0, $this->leftOffset, $this->topOffset, $black, $this->fontPath, $text);
        return $image;
    }
}


function generatorFactory($options)
{
    list('scale' => $scale, 'text_size' => $textSize, 'text_margin' => $textMargin) = $options;
    return new TicketGenerator(new BarCodeGenerator([
        'sf' => $scale, // масштаб баркода
        'ts' => $textSize, // размер текста
        'th' => $textMargin, // отступ между текстом и низом барода
    ]));
}
