<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
$page = $APPLICATION->GetCurPage();

?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <!--Сделано на студии WatchIT-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/style.css?v=2135" />
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/font-awesome/css/fontawesome.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/font-awesome/css/brands.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/font-awesome/css/regular.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/font-awesome/css/solid.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/magnific-popup/dist/magnific-popup.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/fancybox/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/slick-carousel/slick/slick-theme.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/assets/css/additional.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=cyrillic" rel="stylesheet">
    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico" type="image/x-icon">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140909715-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-140909715-1');
    </script>
    <!-- VK Pixel -->
    <script type="text/javascript">!function () {
            var t = document.createElement("script");
            t.type = "text/javascript", t.async = !0, t.src = "https://vk.com/js/api/openapi.js?160", t.onload = function () {
                VK.Retargeting.Init("VK-RTRG-372066-63TmJ"), VK.Retargeting.Hit()
            }, document.head.appendChild(t)
        }();</script>
    <noscript><img src="https://vk.com/rtrg?p=VK-RTRG-372066-63TmJ" style="position:fixed; left:-999px;" alt=""/>
    </noscript>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '327186041288818');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=327186041288818&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(53827825, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/53827825" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body <?if ($_GET["pattern"]) {
    switch ($_GET["pattern"]) {
        case 1:
            echo 'style="background: url(/local/templates/eoforum-public-inner/assets/images/medicine_silhouettes.png); background-size: 1000px auto;"';
            break;
        case 2:
            echo 'style="background: url(/local/templates/eoforum-public-inner/assets/images/medicine_silhouettes_02.jpg); background-size: 1000px auto;"';
            break;
        case 3:
            echo 'style="background: url(/local/templates/eoforum-public-inner/assets/images/medicine_silhouettes_03.jpg); background-size: 1000px auto;"';
            break;
        case 4:
            echo 'style="background: url(/local/templates/eoforum-public-inner/assets/images/medicine_silhouettes_04.jpg); background-size: 1000px auto;"';
            break;
    }
}?>>

<div id="panel"><?$APPLICATION->ShowPanel();?></div>

<div class="page-header">
    <div class="container">
        <div class="header-inner">
            <a href=" <?if (strpos($page, 'en/')) echo '/en';?>/" title="На главную"> <div class="logo"></div></a>
            <div class="header-toolbar">
                <nav class="main-nav">

                    <?if (strpos($page, 'en/')) {?>
                    <ul>
                        <li><a href="/en/">Main page</a></li>
                       <!-- <li><a href="#">Выставка</a></li>-->
                        <li <?if ($APPLICATION->GetCurPage() == '/en/about/') echo 'class="active"'?> ><a href="/en/about/">About Forum</a></li>
                        <li <?if ($APPLICATION->GetCurPage() == '/en/exhibition/') echo 'class="active"'?> ><a href="/exhibition/">Exposition</a></li>
                        <li <?if ($APPLICATION->GetCurPage() == '/en/science-programm/') echo 'class="active"'?>><a href="/en/science-programm/">Scientific programme</a></li>
                        <li <?if ($APPLICATION->GetCurPage() == '/en/business-programm/') echo 'class="active"'?>><a href="/en/business-programm/">Business forum</a></li>
                        <li <?if ($APPLICATION->GetCurPage() == '/en/gallery/') echo 'class="active"'?>><a href="/en/gallery/">Photo and video</a></li>
                        <li <?if ($APPLICATION->GetCurPage() == '/en/news/') echo 'class="active"'?>><a href="/en/news/">News</a></li>
                        <li><a href="/"><i class="fas fa-globe-americas"></i>&nbsp;Руc</a></li>
                        <!--<li><a href="#">Контакты</a></li>-->
                    </ul>
                    <?} else {?>
                        <ul>
                            <li><a href="/">Главная</a></li>
                            <!-- <li><a href="#">Выставка</a></li>-->
                            <li <?if ($APPLICATION->GetCurPage() == '/about/') echo 'class="active"'?> ><a href="/about/">О форуме</a></li>
                            <li <?if ($APPLICATION->GetCurPage() == '/exhibition/') echo 'class="active"'?> ><a href="/exhibition/">Выставка</a></li>
                            <li <?if ($APPLICATION->GetCurPage() == '/science-programm/') echo 'class="active"'?>><a href="/science-programm/">Научная программа</a></li>
                            <li <?if ($APPLICATION->GetCurPage() == '/business-programm/') echo 'class="active"'?>><a href="/business-programm/">Бизнес-форум</a></li>
                            <li <?if ($APPLICATION->GetCurPage() == '/gallery/') echo 'class="active"'?>><a href="/gallery/">Фото и видео</a></li>
                            <li <?if ($APPLICATION->GetCurPage() == '/news/') echo 'class="active"'?>><a href="/news/">Новости</a></li>
                            <li><a href="/en/"><i class="fas fa-globe-americas"></i>&nbsp;Eng</a></li>
                            <!--<li><a href="#">Контакты</a></li>-->
                        </ul>
                    <?};?>
                </nav>


                <div class="header-user">
                    <div>
                        <a href="<?if (strpos($page, 'en/')) echo '/en';?>/profile/"><span class="user-hexagon"><span class="userpic" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/assets/images/user-icon.png)"></span></span>
                        </a>
                    </div>

                </div>
            </div>
            <span class="open-mobile-menu"><i></i></span>
        </div>
    </div>
</div>