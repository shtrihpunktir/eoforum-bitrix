<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>




<h1 ><?=$arResult["NAME"]?></h1>

<div class="entry">

    <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
    <div class="entry-image to-left">
        <img style="max-width: 300px;" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>">
    </div>
    <?endif?>

	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
        <p><?echo $arResult["DETAIL_TEXT"];?></p>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>

    <div class="close-entry-nav">
        <? if (is_array($arResult["TOLEFT"])){ ?>
        <a href="<?= $arResult["TOLEFT"]["URL"] ?>" class="link">
            <span class="arrow"></span>
            <span class="title"><?= $arResult["TOLEFT"]["NAME"] ?></span>
        </a>
        <? } else { ?>
        <div class="link empty">
        </div>
        <?};?>


        <? if (is_array($arResult["TORIGHT"])): ?>
        <a href="<?= $arResult["TORIGHT"]["URL"] ?>" class="link">
            <span class="title"><?= $arResult["TORIGHT"]["NAME"] ?></span>
            <span class="arrow"></span>
        </a>
        <? endif ?>
    </div>

	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?>
	<?endforeach;?>
</div>