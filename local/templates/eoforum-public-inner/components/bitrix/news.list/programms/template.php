<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$page = $APPLICATION->GetCurPage();
?>


<?
if (strpos($page, 'en/')) { ?>

    <div class="programm">

        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>


            <div class="programm-event <? if ($arItem["PROPERTIES"]["SEPARATOR"]["VALUE_XML_ID"] == "Y") echo 'separator' ?>"
                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="programm-event-heading">
                    <h3><? echo $arItem["PROPERTIES"]["NAME_EN"]["VALUE"] ?><? if ($arItem["PROPERTIES"]["SUBTITLE_EN"]["VALUE"]) {
                            echo '<br>' . $arItem["PROPERTIES"]["SUBTITLE_EN"]["VALUE"];
                        }; ?></h3>

                    <? if ($arItem["PROPERTIES"]["MODERATORS_EN"]["~VALUE"]["TEXT"] || $arItem["PROPERTIES"]["DETAIL_TEXT_EN"]["~VALUE"]["TEXT"] || $arItem["PROPERTIES"]["PREVIEW_TEXT_EN"]["~VALUE"]["TEXT"]) { ?>
                        <div class="arrow"></div>
                    <? } ?>

                    <? if ($arItem["PROPERTIES"]["SEPARATOR"]["VALUE_XML_ID"] != "Y") { ?>

                        <div class="programm-event-details">
                            <div class="group">
                                <? if ($arItem["PROPERTIES"]["EVENT_DATE_EN"]["VALUE"]) { ?>
                                    <div class="item"><i
                                                class="fa fa-calendar-alt"></i> <? echo $arItem["PROPERTIES"]["EVENT_DATE_EN"]["VALUE"]; ?>
                                    </div>
                                <? }; ?>
                                <? if ($arItem["PROPERTIES"]["EVENT_TIME_EN"]["VALUE"]) { ?>
                                    <div class="item"><i
                                                class="far fa-clock"></i> <? echo $arItem["PROPERTIES"]["EVENT_TIME_EN"]["VALUE"]; ?>
                                    </div>
                                <? }; ?>
                                <? if ($arItem["PROPERTIES"]["EVENT_PALCE_EN"]["VALUE"]) { ?>
                                    <div class="item"><i
                                                class="fa fa-map-marker-alt"></i> <? echo $arItem["PROPERTIES"]["EVENT_PALCE_EN"]["VALUE"]; ?>
                                    </div>
                                <? }; ?>

                            </div>

                        </div>

                    <? } ?>
                </div>


                <div class="programm-event-content">
                    <? if ($arItem["PROPERTIES"]["PREVIEW_TEXT_EN"]["~VALUE"]["TEXT"]) { ?>
                        <? echo $arItem["PROPERTIES"]["PREVIEW_TEXT_EN"]["~VALUE"]["TEXT"]; ?>

                    <? }; ?>
                    <? if ($arItem["PROPERTIES"]["DETAIL_TEXT_EN"]["~VALUE"]["TEXT"]) { ?>
                        <h4>Issues to be discussed: </h4>
                        <? echo $arItem["PROPERTIES"]["DETAIL_TEXT_EN"]["~VALUE"]["TEXT"]; ?>
                        <br>
                    <? }; ?>
                    <? if ($arItem["PROPERTIES"]["MODERATORS_EN"]["~VALUE"]["TEXT"]) { ?>
                        <h4>Moderator<? if ($arItem['PROPERTIES']['HOW_MANY_PEOPLE']['VALUE_XML_ID'] == 'Y') echo 's' ?>
                            :</h4>
                        <? echo $arItem["PROPERTIES"]["MODERATORS_EN"]["~VALUE"]["TEXT"]; ?>
                        <br><br>
                    <? }; ?>


                </div>

            </div>


        <? endforeach; ?>

    </div>

<? } else { ?>

    <div class="programm">

        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>


            <div class="programm-event <? if ($arItem["PROPERTIES"]["SEPARATOR"]["VALUE_XML_ID"] == "Y") echo 'separator' ?>"
                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="programm-event-heading">
                    <h3><? echo $arItem["NAME"] ?><? if ($arItem["PROPERTIES"]["SUBTITLE"]["VALUE"]) {
                            echo '<br>' . $arItem["PROPERTIES"]["SUBTITLE"]["VALUE"];
                        }; ?></h3>

                    <? if ($arItem["PROPERTIES"]["MODERATORS"]["~VALUE"]["TEXT"] || $arItem["DETAIL_TEXT"] || $arItem["PREVIEW_TEXT"]) { ?>
                        <div class="arrow"></div>
                    <? } ?>

                    <? if ($arItem["PROPERTIES"]["SEPARATOR"]["VALUE_XML_ID"] != "Y") { ?>

                        <div class="programm-event-details">
                            <div class="group">
                                <? if ($arItem["PROPERTIES"]["EVENT_DATE"]["VALUE"]) { ?>
                                    <div class="item"><i
                                                class="fa fa-calendar-alt"></i> <? echo $arItem["PROPERTIES"]["EVENT_DATE"]["VALUE"]; ?>
                                    </div>
                                <? }; ?>
                                <? if ($arItem["PROPERTIES"]["EVENT_TIME"]["VALUE"]) { ?>
                                    <div class="item"><i
                                                class="far fa-clock"></i> <? echo $arItem["PROPERTIES"]["EVENT_TIME"]["VALUE"]; ?>
                                    </div>
                                <? }; ?>
                                <? if ($arItem["PROPERTIES"]["EVENT_PALCE"]["VALUE"]) { ?>
                                    <div class="item"><i
                                                class="fa fa-map-marker-alt"></i> <? echo $arItem["PROPERTIES"]["EVENT_PALCE"]["VALUE"]; ?>
                                    </div>
                                <? }; ?>

                            </div>

                        </div>

                    <? } ?>
                </div>


                <div class="programm-event-content">
                    <? if ($arItem["PREVIEW_TEXT"]) { ?>
                        <? echo $arItem["PREVIEW_TEXT"]; ?>

                    <? }; ?>
                    <? if ($arItem["DETAIL_TEXT"]) { ?>
                        <h4>Вопросы для обсуждения:</h4>
                        <? echo $arItem["DETAIL_TEXT"]; ?>
                        <br>
                    <? }; ?>
                    <? if ($arItem["PROPERTIES"]["MODERATORS"]["~VALUE"]["TEXT"]) { ?>
                        <h4>Модератор<? if ($arItem['PROPERTIES']['HOW_MANY_PEOPLE']['VALUE_XML_ID'] == 'Y') echo 'ы' ?>
                            :</h4>
                        <? echo $arItem["PROPERTIES"]["MODERATORS"]["~VALUE"]["TEXT"]; ?>
                        <br><br>
                    <? }; ?>


                </div>

            </div>


        <? endforeach; ?>

    </div>

<? }; ?>





