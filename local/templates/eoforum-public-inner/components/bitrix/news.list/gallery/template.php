<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$page = $APPLICATION->GetCurPage();
if (strpos($page, 'en/')) {
    $msg_video = 'Video';
    $msg_photo = 'Photo';
} else {
    $msg_video = 'Видео';
    $msg_photo = 'Фото';
};
?>
<h1><?= $msg_video; ?></h1>
<div class="gallery-grid">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <? if ($arItem["PROPERTIES"]["VIDEO"]["VALUE"]) { ?>

            <div class="item video">
                <a href="<?= $arItem["PROPERTIES"]["VIDEO"]["VALUE"] ?>" data-fancybox="images">
					<span class="thumb" style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)">
						<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"/>
					</span>
                </a>
            </div>


        <? } ?>
    <? endforeach; ?>
</div>

<h1><?= $msg_photo; ?></h1>
<div class="gallery-grid">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <? if (!$arItem["PROPERTIES"]["VIDEO"]["VALUE"]) { ?>

            <div class="item">
                <a href="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" data-fancybox="images">
					<span class="thumb" style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)">
						<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"/>
					</span>
                </a>
            </div>


        <? } ?>

    <? endforeach; ?>

