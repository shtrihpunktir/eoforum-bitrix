<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['readmore'] = 'Read more';
} else {
    $msg['readmore'] = 'Читать далее';
};
?>


<div class="news-list">

    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div class="item">
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                <div class="inner">
                    <div class="image"
                         style="background-image: url(<? if ($arItem["PREVIEW_PICTURE"]["SRC"]) echo $arItem["PREVIEW_PICTURE"]["SRC"]; else echo '/local/templates/eoforum-public/assets/images/news-1.png'; ?>)"></div>
                    <div class="item-content">
                        <h3><? echo $arItem["NAME"] ?></h3>
                        <div class="more"><?= $msg['readmore'] ?> <i class="fa fa-arrow-right"></i></div>
                    </div>
                </div>
            </a>
        </div>

    <? endforeach; ?>

</div>


<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>

    <?= $arResult["NAV_STRING"] ?>
<? endif; ?>

