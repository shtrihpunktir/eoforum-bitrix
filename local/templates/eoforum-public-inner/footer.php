<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
$page = $APPLICATION->GetCurPage();
if (strpos($page, 'en/')) {
    $msg_copy = '© All rights reserved. The Eurasian Orthopedic Forum 2019';
    $msg_fb = 'We\'re on Facebook';
    $msg_vk = 'We in VK';
    $msg['text'] = 'The site uses cookies and other similar technologies. If, after reading this message, you remain on our site, it means that you do not object to the use of these technologies.';
    $msg['link'] = 'Learn more';
    $msg['button'] = 'Confirm';
} else {
    $msg_copy = '&copy; Все права защищены. Евразийский Ортопедический Форум 2019';
    $msg_fb = 'Мы на Facebook';
    $msg_vk = 'Мы в ВК';
    $msg['text'] = 'На сайте используются cookie-файлы и другие аналогичные технологии. Если, прочитав это сообщение, вы остаетесь на нашем сайте, это означает, что вы не возражаете против использования этих технологий.';
    $msg['link'] = 'Узнайте больше';
    $msg['button'] = 'Подтвердить';
};
?>

<div id="accept-cookie" class="cookie hide">
    <div class="notice">
        <div class="inner">
            <div class="text">
                <?=$msg['text']?><br><a href="assets/cookie-accept.pdf" target="_blank"><?=$msg['link']?></a>
            </div>
            <div class="actions btns-group">
                <button onclick="acceptCookie()" class="btn btn-mini"><?=$msg['button']?></button>
            </div>
        </div>
    </div>
</div>

<div class="page-footer">
    <div class="container">
        <div class="footer-inner">
            <div class="copyright"><?= $msg_copy; ?></div>
            <div class="socials">
                <ul>
                    <li><a href="https://www.facebook.com/eofmoscow/" target="_blank" title="<?= $msg_fb; ?>"><span
                                    class="icon-hex"><i class="fab fa-facebook-f"></i></span></a></li>
                    <li><a href="https://vk.com/eofmoscow" target="_blank" title="<?= $msg_vk; ?>"><span
                                    class="icon-hex"><i class="fab fa-vk"></i></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script src="<?= SITE_TEMPLATE_PATH ?>/assets/jquery/dist/jquery.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/assets/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/assets/fancybox/dist/jquery.fancybox.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/assets/slick-carousel/slick/slick.min.js"></script>




<script>
    if (BX.getCookie('cookie-accept') !== 'yes') {
        $('#accept-cookie').removeClass("hide");
        $('#accept-cookie').addClass("show");
    }

    function acceptCookie() {
        BX.setCookie('cookie-accept', 'yes', {expires: 36000000});
        $('#accept-cookie').removeClass("show");
        $('#accept-cookie').addClass("hide");
    }
</script>


<script>
    $(function () {

        $('.open-mobile-menu').click(function () {
            $('html').toggleClass('nav-on');
        });

        $('.programm-event-heading').click(function(){
            $(this).parent().toggleClass('open');
        });



        $('.sponsors-gallery .main-frame').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            arrows: false,
            asNavFor: '.sponsors-gallery .control-nav',
            /*autoplay:true/*,
            autoplaySpeed:5000*/
        });
        $('.sponsors-gallery .control-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.sponsors-gallery .main-frame',
            infinite: true,
            /*arrows: false,*/
            /*dots: true,*/
            centerMode: true,
            focusOnSelect: true
        });
    });
</script>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function () {
        var widget_id = 'erpnedGVTY';
        var d = document;
        var w = window;

        function l() {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//code.jivosite.com/script/widget/' + widget_id
            ;var ss = document.getElementsByTagName('script')[0];
            ss.parentNode.insertBefore(s, ss);
        }

        if (d.readyState == 'complete') {
            l();
        } else {
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })();
</script>
<!-- {/literal} END JIVOSITE CODE -->


</body>
</html>