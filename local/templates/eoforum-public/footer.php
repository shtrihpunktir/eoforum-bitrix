<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<?
$page= $APPLICATION->GetCurPage();
if (strpos($page, 'en/')) {
    $msg['text'] = 'The site uses cookies and other similar technologies. If, after reading this message, you remain on our site, it means that you do not object to the use of these technologies.';
    $msg['link'] = 'Learn more';
    $msg['button'] = 'Confirm';
} else {
    $msg['text'] = 'На сайте используются cookie-файлы и другие аналогичные технологии. Если, прочитав это сообщение, вы остаетесь на нашем сайте, это означает, что вы не возражаете против использования этих технологий.';
    $msg['link'] = 'Узнайте больше';
    $msg['button'] = 'Подтвердить';
};
?>


<div id="accept-cookie" class="cookie hide">
    <div class="notice">
        <div class="inner">
            <div class="text">
                <?=$msg['text']?><br><a href="assets/cookie-accept.pdf" target="_blank"><?=$msg['link']?></a>
            </div>
            <div class="actions btns-group">
                <button onclick="acceptCookie()" class="btn btn-mini"><?=$msg['button']?></button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/js/slick.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/js/scripts.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/js/additional.js"></script>
<script type="text/javascript" src="/assets/tooltipster/dist/js/tooltipster.bundle.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/magnific-popup/dist/jquery.magnific-popup.min.js"></script>




<script>
    if (BX.getCookie('cookie-accept') !== 'yes') {
        $('#accept-cookie').removeClass("hide");
        $('#accept-cookie').addClass("show");
    }

    function acceptCookie() {
        BX.setCookie('cookie-accept', 'yes', {expires: 36000000});
        $('#accept-cookie').removeClass("show");
        $('#accept-cookie').addClass("hide");
    }
</script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    setTimeout(function () {
    (function(){ var widget_id = 'erpnedGVTY';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
        s.src = '//code.jivosite.com/script/widget/'+widget_id
        ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
        if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
        else{w.addEventListener('load',l,false);}}})();
    }, 5000)
</script>
<!-- {/literal} END JIVOSITE CODE -->


</body>
</html>