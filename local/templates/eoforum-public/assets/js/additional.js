$(document).ready(function () {
    $('.tooltip-img').tooltipster({
        theme: 'tooltipster-shadow',
        animation: 'fade',
        arrow: true,
        trigger: 'custom',
        triggerOpen: {
            mouseenter: true,
            tap: true
        },
        triggerClose: {
            mouseleave: true,
            originClick: true,
            tap: true
        },
        side: 'left'
    });
});

function delayPics(picsArray) {
    /* Это событие происходит каждый раз
        при изменении onreadystatechange */
    document.onreadystatechange = function (e) {
        /* Когда документ загрузился - начинаем
            загрузку изображений: */
        if ("complete" === document.readyState) {
            for (var i = 0; i < picsArray.length; i += 1) {
                /* Просто переписываем путь к изображению из
                    одного атрибута в другой: */
                picsArray[i].src = picsArray[i].dataset.src;
            }
        }
    };
};
delayPics(document.getElementsByClassName("delay"));


$(document).ready(function () {
    $('.tooltip').tooltipster({
        theme: 'tooltipster-shadow',
        animation: 'fade',
        trigger: 'custom',
        triggerOpen: {
            mouseenter: true,
            tap: true
        },
        triggerClose: {
            mouseleave: true,
            originClick: true,
            tap: true
        },
        arrow: true,
        side: 'bottom'
    });
});

$(function () {
    $('.popup-with-form').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name'
    });
});

$(function () {
    $('.input-fl .form-control').on('focus', function () {
        $(this).parent().addClass('not-empty');
    }).on('focusout', function () {
        if ($(this).val() == '') {
            $(this).parent().removeClass('not-empty');
        }
    });
});

function showInfo(that) {
    $("#info").removeClass("hide");
    $("#sponsors").addClass("hide");
    $("#strategic").addClass("hide");
    $("#info").children().slick('setPosition');
    $("#btn-sponsors").removeClass("read-more").addClass("download");
    $("#btn-strategic").removeClass("read-more").addClass("download");
    $("#btn-info").removeClass("download").addClass("read-more");

}

function showSponsors(that) {
    $("#info").addClass("hide");
    $("#sponsors").removeClass("hide");
    $("#strategic").addClass("hide");
    $("#sponsors").children().slick('setPosition');
    $("#btn-sponsors").removeClass("download").addClass("read-more");
    $("#btn-strategic").removeClass("read-more").addClass("download");
    $("#btn-info").removeClass("read-more").addClass("download");
}

function showStrategic(that) {
    $("#info").addClass("hide");
    $("#sponsors").addClass("hide");
    $("#strategic").removeClass("hide");
    $("#strategic").children().slick('setPosition');
    $("#btn-sponsors").removeClass("read-more").addClass("download");
    $("#btn-strategic").removeClass("download").addClass("read-more");
    $("#btn-info").removeClass("read-more").addClass("download");

}





