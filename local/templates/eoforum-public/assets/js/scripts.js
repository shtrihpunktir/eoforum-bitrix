jQuery(document).ready(() => {
  // slides
  const widthPage = $(window).width();

  const partnersList = $('.partners-list');
  const boardMembersList = $('.board-members-list');
  const mainSlider = $('.main-page .main-slider');

  if(partnersList) {
    if(widthPage > 960) {
      partnersList.slick({
        infinite: true,
        slidesToShow: 4,
        rows: 2,
        autoplay: true,
        dots: true,
        responsive: [{
          breakpoint: 1200,
          settings: {
            slidesToShow: 3
          },
        }, {
          breakpoint: 1270,
          settings: {
            slidesToShow: 3
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
          },
        }
      ]
      });
    } else {
      partnersList.slick({
        infinite: true,
        slidesToShow: 2,
        rows: 1,
        autoplay: true,
        responsive: [{
          breakpoint: 782,
          settings: {
            slidesToShow: 1
          },
        },
        {
          breakpoint: 567,
          settings: {
            slidesToShow: 1,
            arrows: false
          },
        }
      ]});
    }
  }

  if(boardMembersList) {
    if(widthPage > 768) {
      boardMembersList.slick({
        infinite: true,
        slidesToShow: 3,
        rows: 2,
        autoplay: true,
        dots: true,
        responsive: [{
          breakpoint: 1245,
          settings: {
            dots: false,
          },
          breakpoint: 1200,
          settings: {
            slidesToShow: 2
          },
        },
      ]
      });
    } else {
      boardMembersList.slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 1
      });
    }
  };

  if(mainSlider) {
    mainSlider.slick({
      infinite: true,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      speed: 1000,
      pauseOnHover: false
    });
  }

  const page = $('html, body');
  $('.nav a[href*="#"]').click(function() {
    if(widthPage <= 992) {
      $('.nav-wrapper').removeClass('show');
      $('.open-mobile-menu').removeClass('close');
      $('body').removeClass('fixed');
      if(!($('.header').hasClass('sticky'))) {
        page.animate({
          scrollTop: $($.attr(this, 'href')).offset().top - 100
        }, 400);
      } else {
        page.animate({
          scrollTop: $($.attr(this, 'href')).offset().top - 10
        }, 400);
      }
    } else {
      page.animate({
        scrollTop: $($.attr(this, 'href')).offset().top
      }, 400);
    }
    return false;
  });
});

const addTransitionClass = () => {
  const newsItem = document.querySelectorAll('.news-section .news-item');
  newsItem.forEach(function(element) {
    element.classList.add('transition');
  });
}

document.addEventListener('DOMContentLoaded', addTransitionClass());

const showBtnMenu = document.querySelector('.open-mobile-menu');
const menu = document.querySelector('.nav-wrapper');
const body = document.body;

showBtnMenu.onclick = () => {
  menu.classList.toggle('show');
  showBtnMenu.classList.toggle('close');
  body.classList.toggle('fixed');
}

const header = document.querySelector('.header');
body.onscroll = () => {
  let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  if (scrollTop > 600) {
    header.classList.add('sticky');
} else {
    header.classList.remove('sticky');
  } 
}
