<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$page= $APPLICATION->GetCurPage();
if (strpos($page, 'en/')) {
    $msg_call = 'Call';
    $msg_message = 'Send a message';
} else {
    $msg_call = 'Позвонить';
    $msg_message = 'Написать';
};
?>


<div class="contacts-list">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div class="manager-box">
            <div class="wrapper">
                <div class="manager-avatar">
                    <div class="avatar">
                        <div class="img">
                            <img class="delay" alt="manager" src="/assets/pixel.png"
                                 data-src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                        </div>
                    </div>
                </div>
                <div class="name">
                    <?if (strpos($page, 'en/')){
                    echo $arItem["PROPERTIES"]["name_en"]["VALUE"];
                    } else {
                     echo $arItem["NAME"] ;
                    }?>
                </div>
                <div class="position">
                    <?

                    if (strpos($page, 'en/')){
                    echo $arItem["PROPERTIES"]["work_en"]["VALUE"];
                    } else {
                        echo $arItem["PROPERTIES"]["work"]["VALUE"];
                    };
                    ?>



                    <? if ($arItem["PROPERTIES"]["email"]["VALUE"] || $arItem["PROPERTIES"]["phone_format"]["VALUE"]){?>
                    <br><br>
                    <? if ($arItem["PROPERTIES"]["email"]["VALUE"]) {echo $arItem["PROPERTIES"]["email"]["VALUE"].'<br><br>';} ?>
                    <? if ($arItem["PROPERTIES"]["phone_format"]["VALUE"]) {echo $arItem["PROPERTIES"]["phone_format"]["VALUE"];} ?>

                    <?}?>
                </div>
            </div>
            <div class="btns-group">
                <? if ($arItem["PROPERTIES"]["email"]["VALUE"]){?>
                <a href="mailto:<? echo $arItem["PROPERTIES"]["email"]["VALUE"] ?>" class="white"><?=$msg_message;?></a>
                <?}?>
                <? if ($arItem["PROPERTIES"]["phone"]["VALUE"]){?>
                <a href="tel:<? echo $arItem["PROPERTIES"]["phone"]["VALUE"] ?>" class="tranparent"><?=$msg_call;?></a>
                <?}?>

            </div>
        </div>
    <? endforeach; ?>
</div>
