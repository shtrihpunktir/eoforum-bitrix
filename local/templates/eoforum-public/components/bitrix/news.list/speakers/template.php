<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$page = $APPLICATION->GetCurPage();
?>
<div class="board-members-list">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>


    <div class="board-member-box" >
        <div class="photo">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="photo">
        </div>




        <div class="name">
            <? if (strpos($page, 'en/')) {
                if ($arItem["PROPERTIES"]["NAME_EN"]["VALUE"]) {
                    echo $arItem["PROPERTIES"]["NAME_EN"]["VALUE"];
                } else {
                    echo $arItem["NAME"];
                }
            } else {
                echo $arItem["NAME"];
            } ?>
        </div>



        <div class="position">
            <? if (strpos($page, 'en/')) {
                if ($arItem["DETAIL_TEXT"]) {
                    echo $arItem["DETAIL_TEXT"];
                } else {
                    echo $arItem["PREVIEW_TEXT"];
                }
            } else {
                echo $arItem["PREVIEW_TEXT"];
            } ?>

        </div>



    </div>

<?endforeach;?>

</div>
