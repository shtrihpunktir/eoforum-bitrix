<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$page = $APPLICATION->GetCurPage();
?>
<div class="partners-list">

    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <div class="wrapper-box" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <div class="partner-box">
                <div class="partner-logo">
                    <img class="delay" data-src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" src="/assets/pixel.png" alt="logo"/>
                </div>
                <div class="wrapper">
                    <div class="partner-description">
                        <? if (strpos($page, 'en/')) {
                            if ($arItem["PROPERTIES"]["NAME_EN"]["VALUE"]) {
                                echo $arItem["PROPERTIES"]["NAME_EN"]["VALUE"];
                            } else {
                                echo $arItem["NAME"];
                            }
                        } else {
                            echo $arItem["NAME"];
                        } ?>

                        <?//= $arItem["NAME"] ?>
                    </div>
                    <div class="partner-link">
                        <a href="<?= $arItem["PROPERTIES"]["PARTNER_LINK"]["VALUE"] ?>"
                           target="_blank"><?= $arItem["PROPERTIES"]["PARTNER_LINK_VIEW"]["VALUE"] ?></a>
                    </div>
                </div>
            </div>
        </div>

    <? endforeach; ?>

</div>

