<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<?if ($GLOBALS["APPLICATION"]->GetCurPage() != "/login/" && $GLOBALS["APPLICATION"]->GetCurPage() != "/signup/"):?>
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function(){ var widget_id = 'erpnedGVTY';var d=document;var w=window;function l(){
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
            s.src = '//code.jivosite.com/script/widget/'+widget_id
            ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
            if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
            else{w.addEventListener('load',l,false);}}})();
    </script>
    <!-- {/literal} END JIVOSITE CODE -->

<? endif;?>
</body>

<script>$(function () {
        $('.input-fl .form-control').on('focus', function () {
            $(this).parent().addClass('not-empty');
        }).on('focusout', function () {
            if ($(this).val() == '') {
                $(this).parent().removeClass('not-empty');
            }
        });
    });</script>
</html>