<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['on_main'] = 'Back to main page';
    $msg['alredy_register'] = 'Already registered?';
    $msg['succeful_register'] = 'You are successfully registered.';
    $msg['send_link'] = 'We have sent a link to confirm your registration to your E-mail.';
    $msg['login'] = 'Login';
    $msg['register'] = 'Registration';
    $msg['select_status'] = 'Select the way of participation';
    $msg['student_info'] = 'Registration as a "student participant" gives you the opportunity to purchase a ticket to the event at a discounted price.';
    $msg['after_moderation'] = 'After moderation, the specified e-mail will be notified, and the ticket will be available for purchase.';
    $msg['speaker_info'] = 'Registration as a "Speaker" gives you the opportunity to get a ticket for free.';
    $msg['after_speaker_moderation'] = 'Apply for a report, after moderation You will be sent a ticket.<br>If You do not pass the moderation, You will be able to buy a ticket as any party.';
    $msg['media_info'] = 'Registration as a "mass media" gives you the opportunity to get a ticket for free.';
    $msg['after_media_moderation'] = 'After moderation, You will be sent a ticket.<br>If You do not pass the moderation, You will be able to buy a ticket as any party. ';
    $msg['name'] = 'First name';
    $msg['last_name'] = 'Family name ';
    $msg['country'] = 'Country';
    $msg['work'] = 'Organization';
    $msg['position'] = 'Position';
    $msg['phone'] = 'Phone number';
    $msg['edu'] = 'Your educational institution';
    $msg['edu_photo'] = 'Attach a photo of Your student card';
    $msg['email'] = 'E-mail';
    $msg['password'] = 'Password';
    $msg['password_again'] = 'Password again';
    $msg['password_not_match'] = 'Passwords do not match';
    $msg['signup'] = 'Sign up';
    $msg['role_1'] = 'Participant';
    $msg['role_2'] = 'Participant - student';
    $msg['role_3'] = 'Speaker';
    $msg['role_4'] = 'Mass media';
    $msg['another'] = 'Other';
    $msg['for_speaker'] = 'for speaker';
    $msg['for_media'] = 'for mass media';

} else {
    $msg['on_main'] = 'На главную';
    $msg['alredy_register'] = 'Уже зарегистрированы?';
    $msg['succeful_register'] = 'Вы успешно зарегистрированны.';
    $msg['send_link'] = 'Мы отправили ссылку для подтверждения регистрации на уазанный Вами E-mail.';
    $msg['login'] = 'Войти';
    $msg['register'] = 'Регистрация';
    $msg['select_status'] = 'Выберите способ участия';
    $msg['student_info'] = 'Регистрация в качестве "участника - студента" дает возможность приобрести билет на мероприятие по льготной цене.';
    $msg['after_moderation'] = 'После модерации на указаный e-mail придет оповещение, а билет станет доступен для покупки.';
    $msg['speaker_info'] = 'Регистрация в качестве "Спикера" дает возможность получить билет бесплатно.';
    $msg['after_speaker_moderation'] = 'Подайте заявку на доклад, после модерации Вам будет отправлен билет.<br>Если Вы не пройдете модерацию, вы сможете купить билет на общих основаниях.';
    $msg['media_info'] = 'Регистрация в качестве "СМИ" дает возможность получить билет бесплатно.';
    $msg['after_media_moderation'] = 'После модерации Вам будет отправлен билет.<br>Если Вы не пройдете модерацию, вы сможете купить билет на общих основаниях.';
    $msg['name'] = 'Имя';
    $msg['last_name'] = 'Фамилия';
    $msg['country'] = 'Страна';
    $msg['work'] = 'Организация';
    $msg['position'] = 'Должность';
    $msg['phone'] = 'Телефон';
    $msg['edu'] = 'Ваше образовательное учреждение';
    $msg['edu_photo'] = 'Прикрепите фотографию Вашего студенческого билета';
    $msg['email'] = 'E-mail';
    $msg['password'] = 'Пароль';
    $msg['password_again'] = 'Пароль повторно';
    $msg['password_not_match'] = 'Пароли не совпадают';
    $msg['signup'] = 'Зарегистрироваться';
    $msg['role_1'] = 'Участник';
    $msg['role_2'] = 'Участник - студент';
    $msg['role_3'] = 'Спикер';
    $msg['role_4'] = 'СМИ';
    $msg['another'] = 'Другое';
    $msg['for_speaker'] = 'спикера';
    $msg['for_media'] = 'СМИ';


};

?>
<? if ($USER->IsAuthorized()): ?>

    <? LocalRedirect("/"); ?>


<? else: ?>


    <nav class="navbar pt-5 flex-shrink-0">
        <div class="container">
            <div></div>
            <div>
                <a href="<? if (strpos($page, 'en/')) echo '/en'; ?>/" title="<?= $msg['on_main']; ?>">
                    <div class="logo"></div>
                </a>
            </div>
            <div></div>
        </div>
    </nav>
    <div class="flex-grow-1 d-flex flex-column justify-content-center pb-3">
        <div class="container">

            <? if ($_GET["success"] != "Y"): ?>
                <div class="text-center py-4"> <?= $msg['alredy_register']; ?> <a
                            href="<? if (strpos($page, 'en/')) echo '/en'; ?>/login"><?= $msg['login']; ?></a></div>
            <? endif; ?>

            <h1 class="text-center mb-5"><?= $msg['register']; ?>
                <? if ($_GET['type'] == 'speaker') {
                    echo $msg['for_speaker'];
                } elseif ($_GET['type'] == 'media') {
                    echo $msg['for_media'];
                } elseif ($_GET['success'] == 'Y') {
                } else {
                    LocalRedirect("/404.php");
                }
                ?></h1>
            <div class="row">
                <div class="col col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12 offset-0">
                    <div class="p-5 bg-white shadow-sm mb-5">


                        <? if (count($arResult["ERRORS"]) > 0):
                            foreach ($arResult["ERRORS"] as $key => $error)
                                if (intval($key) == 0 && $key !== 0)
                                    $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
                            ?>
                            <div class="px-sm-5 pt-5">
                                <? ShowError(implode("<br />", $arResult["ERRORS"])); ?>
                            </div>
                        <? endif ?>


                        <? if ($_GET["success"] == "Y"): ?>
                            <div class="px-sm-5 pt-5">
                                <p><?= $msg['succeful_register'] ?></p>
                                <p><?= $msg['send_link'] ?></p>
                                <p>&nbsp;</p>
                                <div class="form-group">
                                    <form target="_self" action="/login/">
                                        <input type="submit"
                                               class="btn btn-primary btn-lg btn-block"
                                               value="<?= $msg['login'] ?>">
                                    </form>
                                </div>
                            </div>

                        <? else: ?>

                            <div class="px-sm-5 pt-5">
                                <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform"
                                      enctype="multipart/form-data">
                                    <?
                                    if ($arResult["BACKURL"] <> ''):
                                        ?>
                                        <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>

                                    <?
                                    endif;
                                    ?>
                                    <? //Логин. Скрываем, при регистрации подставим в него email через init.php?>
                                    <input size="30" type="hidden" name="REGISTER[LOGIN]"
                                           value="temp_login"/>

                                    <div class="form-group" style="display: none">
                                        <select name="UF_TYPE" onchange="yesnoCheck(this);"
                                                class="form-control form-control-lg">
                                            <!-- <option value="" disabled selected><? /*= $msg['select_status']; */ ?></option>-->

                                            <option value="3">
                                                <?= $msg['role_1'] ?>
                                            </option>

                                            <option value="7">
                                                <?= $msg['role_2'] ?>
                                            </option>

                                            <option <? if ($_GET["type"] == 'speaker') echo 'selected'; ?> value="4">
                                                <?= $msg['role_3'] ?>
                                            </option>

                                            <option <? if ($_GET["type"] == 'media') echo 'selected'; ?> value="5">
                                                <?= $msg['role_4'] ?>
                                            </option>

                                        </select>
                                    </div>
                                    <div class="form-group" id="ifStudentLabel" style="display: none;">
                                        <div class="file-input-label"><?= $msg['student_info']; ?>
                                            <br><br><?= $msg['after_moderation']; ?></div>
                                    </div>
                                    <div class="form-group" id="ifMediaLabel" style="display: none;">
                                        <div class="file-input-label"><?= $msg['media_info']; ?>
                                            <br><br><?= $msg['after_media_moderation']; ?></div>
                                    </div>
                                    <div class="form-group" id="ifSpeakerLabel" style="display: none;">
                                        <div class="file-input-label"><?= $msg['speaker_info']; ?>
                                            <br><br><?= $msg['after_speaker_moderation']; ?></div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-fl <? if ($arResult["VALUES"]["NAME"]) echo 'not-empty'; ?>">
                                                    <input type="text"
                                                           class="form-control"
                                                           placeholder=""
                                                           required
                                                           autocomplete="off"
                                                           name="REGISTER[NAME]"
                                                           id="REGISTER[NAME]"
                                                           value="<?= $arResult["VALUES"]["NAME"] ?>">
                                                    <label for="REGISTER[NAME]" class="f-label"><?= $msg['name']; ?>
                                                        *</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-fl <? if ($arResult["VALUES"]["LAST_NAME"]) echo 'not-empty'; ?>">
                                                    <input type="text"
                                                           name="REGISTER[LAST_NAME]"
                                                           id="REGISTER[LAST_NAME]"
                                                           required
                                                           autocomplete="off"
                                                           value="<?= $arResult["VALUES"]["LAST_NAME"] ?>"
                                                           class="form-control form-control-lg"
                                                           placeholder="">
                                                    <label for="REGISTER[LAST_NAME]"
                                                           class="f-label"><?= $msg['last_name']; ?>*</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-fl <? if ($arResult["VALUES"]["PERSONAL_CITY"]) echo 'not-empty'; ?>">
                                                    <input type="text"
                                                           class="form-control"
                                                           placeholder=""
                                                           required
                                                           autocomplete="off"
                                                           name="REGISTER[PERSONAL_CITY]"
                                                           id="REGISTER[PERSONAL_CITY]"
                                                           value="<?= $arResult["VALUES"]["PERSONAL_CITY"] ?>">
                                                    <label for="REGISTER[PERSONAL_CITY]"
                                                           class="f-label"><?= $msg['country']; ?>*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-fl <? if ($arResult["VALUES"]["PERSONAL_PHONE"]) echo 'not-empty'; ?>">
                                                    <input type="text"
                                                           name="REGISTER[PERSONAL_PHONE]"
                                                           id="REGISTER[PERSONAL_PHONE]"
                                                           required
                                                           autocomplete="off"
                                                           value="<?= $arResult["VALUES"]["PERSONAL_PHONE"] ?>"
                                                           class="form-control form-control-lg"
                                                           placeholder="">
                                                    <label for="REGISTER[PERSONAL_PHONE]"
                                                           class="f-label"><?= $msg['phone']; ?>*</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row" id="ifStudentWork">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-fl <? if ($arResult["VALUES"]["WORK_COMPANY"]) echo 'not-empty'; ?>">
                                                    <input type="text"
                                                           name="REGISTER[WORK_COMPANY]"
                                                           id="REGISTER[WORK_COMPANY]"
                                                           required
                                                           value="<?= $arResult["VALUES"]["WORK_COMPANY"] ?>"
                                                           class="form-control form-control-lg"
                                                           placeholder="">
                                                    <label for="REGISTER[WORK_COMPANY]"
                                                           class="f-label"><?= $msg['work']; ?>*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-fl <? if ($arResult["VALUES"]["WORK_POSITION"]) echo 'not-empty'; ?>">
                                                    <input type="text"
                                                           name="REGISTER[WORK_POSITION]"
                                                           id="REGISTER[WORK_POSITION]"
                                                           required
                                                           value="<?= $arResult["VALUES"]["WORK_POSITION"] ?>"
                                                           class="form-control form-control-lg"
                                                           placeholder="">
                                                    <label for="REGISTER[WORK_POSITION]"
                                                           class="f-label"><?= $msg['position']; ?>*</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="ifStudentEdu" style="display: none;">
                                        <select name="UF_ACADEMIC" onchange="anotherEDUCheck(this);" id="UF_ACADEMIC"
                                                class="form-control form-control-lg">
                                            <option value="" disabled selected><?= $msg['edu']; ?></option>
                                            <?
                                            $rsEnum = CUserFieldEnum::GetList(array("SORT" => "ASC"), array("USER_FIELD_NAME" => "UF_ACADEMIC"));
                                            while ($arEnum = $rsEnum->Fetch()) {
                                                if ($arEnum['ID'] != '21') {
                                                    echo '<option value="' . $arEnum['ID'] . '">' . $arEnum['VALUE'] . '</option>';
                                                }
                                            };
                                            ?>
                                            <option value="21"><?= $msg['another'] ?></option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="ifStudentEduAnother" style="display: none;">
                                        <div class="input-fl <? if ($arResult["VALUES"]["UF_ACADEMIC_ANOTHER"]) echo 'not-empty'; ?>"
                                             id="anotherEduDiv">
                                            <input type="text"
                                                   name="UF_ACADEMIC_ANOTHER"
                                                   id="REGISTER[UF_ACADEMIC_ANOTHER]"
                                                   value="<?= $arResult["VALUES"]["UF_ACADEMIC_ANOTHER"] ?>"
                                                   autocomplete="off"
                                                   class="form-control form-control-lg"
                                                   placeholder="">
                                            <label for="REGISTER[UF_ACADEMIC_ANOTHER]"
                                                   class="f-label"><?= $msg['edu'] ?>*</label>
                                        </div>
                                    </div>
                                    <div class="form-group" id="ifStudentPhoto" style="display: none;">
                                        <label class="file-input-label"><?= $msg['edu_photo']; ?></label>
                                        <input class="file-input-button" id="PERSONAL_PHOTO"
                                               name="REGISTER_FILES_PERSONAL_PHOTO" class="" size="20" type="file">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-fl <? if ($arResult["VALUES"]["EMAIL"]) echo 'not-empty'; ?>">
                                            <input type="email"
                                                   name="REGISTER[EMAIL]"
                                                   id="REGISTER[EMAIL]"
                                                   value="<?= $arResult["VALUES"]["EMAIL"] ?>"
                                                   autocomplete="email"
                                                   required
                                                   autocomplete="off"
                                                   class="form-control form-control-lg"
                                                   placeholder="">
                                            <label for="REGISTER[EMAIL]" class="f-label">E-mail*</label>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-fl <? if ($arResult["VALUES"]["PASSWORD"]) echo 'not-empty'; ?>">
                                                    <input type="password"
                                                           name="REGISTER[PASSWORD]"
                                                           id="password"
                                                           value="<?= $arResult["VALUES"]["PASSWORD"] ?>"
                                                           autocomplete="off"
                                                           required
                                                           class="form-control form-control-lg"
                                                           placeholder="">
                                                    <label for="password" class="f-label"><?= $msg['password']; ?>
                                                        *</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-fl <? if ($arResult["VALUES"]["CONFIRM_PASSWORD"]) echo 'not-empty'; ?>">
                                                    <input type="password"
                                                           name="REGISTER[CONFIRM_PASSWORD]"
                                                           id="confirm_password"
                                                           value="<?= $arResult["VALUES"]["CONFIRM_PASSWORD"] ?>"
                                                           autocomplete="off"
                                                           required
                                                           class="form-control form-control-lg"
                                                           placeholder="">
                                                    <label for="confirm_password"
                                                           class="f-label"><?= $msg['password_again']; ?>*</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="register_submit_button"
                                               class="btn btn-primary btn-lg btn-block"
                                               value="<?= $msg['signup']; ?>">
                                    </div>
                                    <!--<p><? //echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>-->
                                </form>


                            </div>

                            <script>
                                var password = document.getElementById("password")
                                    , confirm_password = document.getElementById("confirm_password");

                                function validatePassword() {
                                    if (password.value != confirm_password.value) {
                                        confirm_password.setCustomValidity("<?=$msg['password_not_match'];?>");
                                    } else {
                                        confirm_password.setCustomValidity('');
                                    }
                                }

                                password.onchange = validatePassword;
                                confirm_password.onkeyup = validatePassword;


                                $(function () {
                                    $('.input-fl .form-control').on('focus', function () {
                                        $(this).parent().addClass('not-empty');
                                    }).on('focusout', function () {
                                        if ($(this).val() == '') {
                                            $(this).parent().removeClass('not-empty');
                                        }
                                    });
                                });

                                function yesnoCheck(that) {
                                    if (that.value == "7") {
                                        document.getElementById("ifStudentEdu").style.display = "block";
                                        document.getElementById("ifStudentPhoto").style.display = "block";
                                        document.getElementById("ifStudentWork").style.display = "none";
                                        document.getElementById("ifStudentLabel").style.display = "block";
                                        document.getElementById("REGISTER[WORK_COMPANY]").required = false;
                                        document.getElementById("REGISTER[WORK_POSITION]").required = false;
                                        document.getElementById("UF_ACADEMIC").required = true;
                                        document.getElementById("PERSONAL_PHOTO").required = true;


                                    } else {
                                        document.getElementById("ifStudentEdu").style.display = "none";
                                        document.getElementById("ifStudentPhoto").style.display = "none";
                                        document.getElementById("ifStudentWork").style.display = "flex";
                                        document.getElementById("ifStudentLabel").style.display = "none";
                                        document.getElementById("REGISTER[WORK_COMPANY]").required = true;
                                        document.getElementById("REGISTER[WORK_POSITION]").required = true;
                                        document.getElementById("UF_ACADEMIC").required = false;
                                        document.getElementById("PERSONAL_PHOTO").required = false;
                                    }

                                    if (that.value == "4") {
                                        document.getElementById("ifSpeakerLabel").style.display = "block";
                                        document.getElementById("ifStudentLabel").style.display = "none";
                                        document.getElementById("ifMediaLabel").style.display = "none";
                                    }

                                    if (that.value == "5") {
                                        document.getElementById("ifSpeakerLabel").style.display = "none";
                                        document.getElementById("ifStudentLabel").style.display = "none";
                                        document.getElementById("ifMediaLabel").style.display = "block";
                                    }

                                    if (that.value == "3") {
                                        document.getElementById("ifSpeakerLabel").style.display = "none";
                                        document.getElementById("ifStudentLabel").style.display = "none";
                                        document.getElementById("ifMediaLabel").style.display = "none";
                                    }


                                }

                                function anotherEDUCheck(that) {
                                    if (that.value == "21") {
                                        document.getElementById("ifStudentEduAnother").style.display = "block";
                                        document.getElementById("REGISTER[UF_ACADEMIC_ANOTHER]").required = true;


                                    } else {
                                        document.getElementById("ifStudentEduAnother").style.display = "none";
                                        document.getElementById("REGISTER[UF_ACADEMIC_ANOTHER]").required = false;
                                        document.getElementById("anotherEduDiv").removeClass = ('not-empty');

                                    }
                                }


                            </script>


                        <? if ($_GET["type"] == "student"){ ?>
                            <script>
                                document.getElementById("ifStudentEdu").style.display = "block";
                                document.getElementById("ifStudentPhoto").style.display = "block";
                                document.getElementById("ifStudentWork").style.display = "none";
                                document.getElementById("ifStudentLabel").style.display = "block";
                                document.getElementById("REGISTER[WORK_COMPANY]").required = false;
                                document.getElementById("REGISTER[WORK_POSITION]").required = false;
                                document.getElementById("UF_ACADEMIC").required = true;
                                document.getElementById("PERSONAL_PHOTO").required = true;
                            </script>


                        <? } ?>
                        <? endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<? endif ?>



