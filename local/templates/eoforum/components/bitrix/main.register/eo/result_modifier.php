<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// меняем порядок следования полей
$arResult['SHOW_FIELDS'] = array(
    'NAME',
    'LAST_NAME',
    'WORK_COMPANY',
    'WORK_POSITION',
    'EMAIL',
    'LOGIN',
    'PASSWORD',
    'CONFIRM_PASSWORD',
);
?>

