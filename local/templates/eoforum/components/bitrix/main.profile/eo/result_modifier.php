<?php
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
$arOrder = Array("ID" => "DESC");
$arFilter = Array(
    "USER_ID" => $USER->GetID(),

);
$arGroupBy = false;
$arNavStartParams = false;
$arSelectFields = Array("ID", "PAYED");

$order = CSaleOrder::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);


while ($orderItem = $order->Fetch()) {
    $ID = $orderItem["ID"];
    $PAYED = $orderItem["PAYED"];
    break;
}

/*
Статусы:
0 - заказов нет
1 - заказ есть, не оплачен
2 - заказ есть, оплачен
*/
if ($ID) {
    if ($PAYED == 'Y') {
        $STATUS = 2;
    } else {
        $STATUS = 1;
    };

    $res = CSaleBasket::GetList(array(), array("ORDER_ID" => $ID)); // ID заказа
    while ($itemParam = $res->Fetch()) {
        $PRODUCT = $itemParam['NAME'];
        $PRODUCT_ID = $itemParam['PRODUCT_ID'];
    }

    $db_props = CIBlockElement::GetProperty(3, $PRODUCT_ID, array("sort" => "asc"), Array("CODE" => "BACKGROUND_GRADIENT_START"));
    while ($ar_props = $db_props->Fetch()) {
        $GRAD_START = $ar_props['VALUE'];

    }
    $db_props = CIBlockElement::GetProperty(3, $PRODUCT_ID, array("sort" => "asc"), Array("CODE" => "BACKGROUND_GRADIENT_END"));
    while ($ar_props = $db_props->Fetch()) {
        $GRAD_END = $ar_props['VALUE'];

    }

    //echo 'Статус - ' . $STATUS . '<br>';
    // echo 'Продукт - ' . $PRODUCT . '<br>';
    //echo $GRAD_START . ' - ' . $GRAD_END;

} else {
    $STATUS = 0;
}

$arResult['STATUS'] = $STATUS;
$arResult['PRODUCT'] = $PRODUCT;
$arResult['GRAD_START'] = $GRAD_START;
$arResult['GRAD_END'] = $GRAD_END;
