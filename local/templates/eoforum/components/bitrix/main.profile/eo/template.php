<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['on_main'] = 'Back to main page';
    $msg['main'] = 'Main page';
    $msg['profile'] = 'Personal account';
    $msg['status'] = 'Status';
    $msg['ticket_not_pick'] = 'The ticket is not chosen';
    $msg['ticket_not_paid'] = 'The ticket is not paid';
    $msg['ticket_not_buyed'] = 'Ticket was purchased';
    $msg['moderation'] = 'Moderation';
    $msg['logout'] = 'Logout';
    $msg['ticket'] = 'Your ticket';
    $msg['success_payed'] = 'Successfully paid';
    $msg['pay'] = 'Pay';
    $msg['get_member'] = 'Become a participant';
    $msg['buy_ticket'] = 'Buy a ticket';
    $msg['upgrade'] = 'Upgrade';
    $msg['get_spiker'] = 'Become a speaker';
    $msg['apply'] = 'Send request';
    $msg['im_media'] = 'I’m Mass media';
    $msg['personal_info'] = 'Personal information';
    $msg['change_pass'] = 'Password change';
    $msg['name'] = 'First name';
    $msg['last_name'] = 'Family name ';
    $msg['country'] = 'Country';
    $msg['work'] = 'Organization';
    $msg['position'] = 'Position';
    $msg['phone'] = 'Phone number';
    $msg['edu'] = 'Your educational institution';
    $msg['edu_photo'] = 'Attach a photo of Your student card';
    $msg['email'] = 'E-mail';
    $msg['new_password'] = 'New password';
    $msg['new_password_again'] = 'Repeat new password ';
    $msg['password_not_match'] = 'Passwords do not match';
    $msg['save_changes'] = 'Apply change';
    $msg['role_1'] = 'Participant';
    $msg['role_2'] = 'Participant - student';
    $msg['role_3'] = 'Speaker';
    $msg['role_4'] = 'Mass media';
    $msg['moder_null'] = 'not done';
    $msg['moder_Y'] = 'is successful';
    $msg['moder_Т'] = 'is not successful';

} else {
    $msg['on_main'] = 'На главную';
    $msg['main'] = 'Главная';
    $msg['profile'] = 'Личный кабинет';
    $msg['status'] = 'Статус на форуме';
    $msg['ticket_not_pick'] = 'Билет не выбран';
    $msg['ticket_not_paid'] = 'Билет не оплачен';
    $msg['ticket_not_buyed'] = 'Билет куплен';
    $msg['moderation'] = 'Модерация';
    $msg['logout'] = 'Выйти из аккаунта';
    $msg['ticket'] = 'Ваш билет';
    $msg['success_payed'] = 'Успешно оплачен';
    $msg['pay'] = 'Оплатить';
    $msg['get_member'] = 'Стать участником';
    $msg['buy_ticket'] = 'Купить билет';
    $msg['upgrade'] = 'Улучшить';
    $msg['get_spiker'] = 'Стать спикером';
    $msg['apply'] = 'Подать заявку';
    $msg['im_media'] = 'Я - СМИ';
    $msg['personal_info'] = 'Персональная информация';
    $msg['change_pass'] = 'Изменение пароля';
    $msg['name'] = 'Имя';
    $msg['last_name'] = 'Фамилия';
    $msg['country'] = 'Страна';
    $msg['work'] = 'Организация';
    $msg['position'] = 'Должность';
    $msg['phone'] = 'Телефон';
    $msg['edu'] = 'Ваше образовательное учреждение';
    $msg['edu_photo'] = 'Прикрепите фотографию Вашего студенческого билета';
    $msg['email'] = 'E-mail';
    $msg['new_password'] = 'Новый пароль';
    $msg['new_password_again'] = 'Новый пароль повторно';
    $msg['password_not_match'] = 'Пароли не совпадают';
    $msg['save_changes'] = 'Сохранить изменения';
    $msg['role_1'] = 'Участник';
    $msg['role_2'] = 'Участник - студент';
    $msg['role_3'] = 'Спикер';
    $msg['role_4'] = 'СМИ';
    $msg['moder_null'] = 'Не проводилась';
    $msg['moder_Y'] = 'Пройдена';
    $msg['moder_Т'] = 'Не пройдена';


};

?>

<script type="text/javascript">
    <!--
    var opened_sections = [<?
        $arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"] . "_user_profile_open"];
        $arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
        if (strlen($arResult["opened"]) > 0) {
            echo "'" . implode("', '", explode(",", $arResult["opened"])) . "'";
        } else {
            $arResult["opened"] = "reg";
            echo "'reg'";
        }
        ?>];
    //-->

    var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>

<nav class="navbar pt-5 mb-5">
    <div class="container">
        <div>
            <a href="<? if (strpos($page, 'en/')) echo '/en'; ?>/" title="<?= $msg['on_main'] ?>">
                <div class="logo"></div>
            </a>
        </div>
    </div>
</nav>
<div class="container">
    <div>
        <div class="breadcrumb mb-5">
            <li class="breadcrumb-item"><a href="<? if (strpos($page, 'en/')) echo '/en'; ?>/"><?= $msg['main'] ?></a>
            </li>
            <li class="breadcrumb-item active"><?= $msg['profile'] ?></li>
        </div>
    </div>
    <div class="d-flex mb-4"><h1><?= $msg['profile'] ?></h1> <? if ($arResult['DATA_SAVED'] == 'Y') { ?>

            <p style="margin: 1.6rem 0 0 2rem; color: green;">
                <? echo GetMessage('PROFILE_DATA_SAVED'); ?>
            </p>

        <? }; ?>
    </div>
    <div>
        <div class="row">
            <div class="col-lg-3 d-flex flex-column pt-lg-4 pb-lg-5">
                <div class="account-summary bg-primary rounded text-white text-center p-1 shadow flex-grow-1 d-flex flex-lg-column justify-content-around align-items-center">
                    <br>
                    <div class="account-summary-user">
                        <div class="userpic mb-2"><img
                                    src="<?= SITE_TEMPLATE_PATH ?>/assets/user-face.png">
                        </div>
                        <h4 class="username text-white pt-2 mb-3"><?= $arResult["arUser"]["NAME"] ?>
                            <br><?= $arResult["arUser"]["LAST_NAME"] ?></h4></div>


                    <? if ($arResult["arUser"]["UF_TYPE"] == 3) { // 3 - участник?>


                        <div class="account-summary-item"> <?= $msg['status'] ?>

                            <div class="value">  <?
                                $rsType = CUserFieldEnum::GetList(array(), array(
                                    "ID" => $arResult["arUser"]["UF_TYPE"],
                                ));
                                if ($arType = $rsType->GetNext()) $arType["ID"];
                                switch ($arType["ID"]) {
                                    case 3:
                                        echo $msg['role_1'];
                                        break;
                                    case 7:
                                        echo $msg['role_2'];
                                        break;
                                    case 4:
                                        echo $msg['role_3'];
                                        break;
                                    case 5:
                                        echo $msg['role_4'];
                                        break;
                                    default:
                                        echo '';
                                }
                                ?>
                            </div>
                            <br>
                            <?= $msg['ticket'] ?>
                            <div class="value">
                                <? switch ($arResult["STATUS"]) {
                                    case 0:
                                        echo $msg['ticket_not_pick'];
                                        break;
                                    case 1:
                                        echo $msg['ticket_not_paid'];
                                        break;
                                    case 2:
                                        echo $msg['ticket_not_buyed'];
                                        break;
                                    default:
                                        echo '';
                                }
                                ?>
                            </div>
                        </div>


                    <? } else { // не участник?>

                        <div class="account-summary-item"> <?= $msg['status'] ?>

                            <div class="value">  <?
                                $rsType = CUserFieldEnum::GetList(array(), array(
                                    "ID" => $arResult["arUser"]["UF_TYPE"],
                                ));
                                if ($arType = $rsType->GetNext()) $arType["ID"];
                                switch ($arType["ID"]) {
                                    case 3:
                                        echo $msg['role_1'];
                                        break;
                                    case 7:
                                        echo $msg['role_2'];
                                        break;
                                    case 4:
                                        echo $msg['role_3'];
                                        break;
                                    case 5:
                                        echo $msg['role_4'];
                                        break;
                                    default:
                                        echo '';
                                }
                                ?>
                            </div>
                            <br>
                            <?= $msg['ticket'] ?>
                            <div class="value">
                                <? switch ($arResult["STATUS"]) {
                                    case 0:
                                        echo $msg['ticket_not_pick'];
                                        break;
                                    case 1:
                                        echo $msg['ticket_not_paid'];
                                        break;
                                    case 2:
                                        echo $msg['ticket_not_buyed'];
                                        break;
                                    default:
                                        echo '';
                                }
                                ?>
                            </div>
                        </div>


                        <div class="account-summary-item"><?= $msg['moderation'] ?>
                            <div class="value" <? if ($arResult["arUser"]["UF_MODERATION"] == 20) {
                                echo 'style="color: #e00000b5; font-weight: 500;"';
                            } elseif ($arResult["arUser"]["UF_MODERATION"] == 19) {
                                echo 'style="color: #00750ac9; font-weight: 500;"';
                            } ?> >
                                <?
                                $rsType = CUserFieldEnum::GetList(array(), array(
                                    "ID" => $arResult["arUser"]["UF_MODERATION"],
                                ));
                                if ($arType = $rsType->GetNext()) $arType["ID"];
                                switch ($arType['ID']) {
                                    case 18:
                                        echo $msg['moder_null'];
                                        break;
                                    case 19:
                                        echo $msg['moder_Y'];
                                        break;
                                    case 20:
                                        echo $msg['moder_N'];
                                        break;

                                }
                                ?></div>
                        </div>

                    <? } ?>
                    <br>
                    <div class="account-summary-exit">
                        <!--
                        <pre>
                        <?=$arResult['STATUS']?>
                        </pre>
                        -->
                        <? if ($arResult["STATUS"] == 0) { // 0 - если билет не куплен?>
                            <a href="/order"
                               class="btn btn-outline-light"><?= $msg['buy_ticket'] ?>
                            </a>
                        <? } elseif ($arResult["STATUS"] == 2) { // 2 - если билен куплен?>

                            <a href="/order"
                               class="btn btn-outline-light"><?= $msg['upgrade'] ?>
                            </a>
                        <? } elseif ($arResult["STATUS"] == 1) { // 1- не оплатил билет?>

                            <a href="/order"
                               class="btn btn-outline-light"><?= $msg['pay'] ?>
                            </a>
                        <? } ?>
                        <br><br>
                        <a href="/?logout=yes"
                           class="btn btn-outline-light"><?= $msg['logout'] ?>
                        </a>
                    </div>
                    <br>
                </div>
            </div>


            <div class="col-lg-9">


                <? if ($arResult["arUser"]["UF_TYPE"] != 7) { ?>
                    <div class="pt-4"><h5 class="mb-4"><?= $msg['personal_info'] ?></h5>
                        <form method="post" name="form2" action="<?= $arResult["FORM_TARGET"] ?>"
                              enctype="multipart/form-data">
                            <?= $arResult["BX_SESSION_CHECK"] ?>
                            <input type="hidden" name="lang" value="<?= LANG ?>"/>
                            <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>"/>

                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="NAME"
                                               maxlength="50"
                                               placeholder="<?= $msg['name'] ?>" required
                                               value="<?= $arResult["arUser"]["NAME"] ?>"/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="LAST_NAME"
                                               maxlength="50" required placeholder="<?= $msg['last_name'] ?>"
                                               value="<?= $arResult["arUser"]["LAST_NAME"] ?>"/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="PERSONAL_PHONE"
                                               placeholder="<?= $msg['phone'] ?>" required maxlength="50"
                                               value="<? echo $arResult["arUser"]["PERSONAL_PHONE"] ?>"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="WORK_COMPANY"
                                               placeholder="<?= $msg['work'] ?>" required maxlength="255"
                                               autocomplete="off"
                                               value="<?= $arResult["arUser"]["WORK_COMPANY"] ?>"/>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="WORK_POSITION"
                                               placeholder="<?= $msg['position'] ?>" required maxlength="255"
                                               autocomplete="off"
                                               value="<?= $arResult["arUser"]["WORK_POSITION"] ?>"/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="PERSONAL_CITY"
                                               placeholder=<?= $msg['country'] ?>" required maxlength=" 50"
                                        value="<? echo $arResult["arUser"]["PERSONAL_CITY"] ?>"
                                        />
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="EMAIL"
                                               placeholder="E-mail" disabled maxlength="50"
                                               value="<? echo $arResult["arUser"]["EMAIL"] ?>"
                                               style="color: #969696;"/>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" name="save"
                                                value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD")) ?>"
                                                class="btn btn-primary btn-lg btn-block"><?= $msg['save_changes'] ?>
                                        </button>
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>
                <? } else { ?>
                    <div class="pt-4"><h5 class="mb-4"><?= $msg['personal_info'] ?></h5>
                        <form method="post" name="form2" action="<?= $arResult["FORM_TARGET"] ?>"
                              enctype="multipart/form-data">
                            <?= $arResult["BX_SESSION_CHECK"] ?>
                            <input type="hidden" name="lang" value="<?= LANG ?>"/>
                            <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>"/>

                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="NAME"
                                               maxlength="50"
                                               placeholder="<?= $msg['name'] ?>" required
                                               value="<?= $arResult["arUser"]["NAME"] ?>"/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="LAST_NAME"
                                               maxlength="50" required placeholder="<?= $msg['last_name'] ?>"
                                               value="<?= $arResult["arUser"]["LAST_NAME"] ?>"/>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="PERSONAL_PHONE"
                                               placeholder="<?= $msg['phone'] ?>" required maxlength="50"
                                               value="<? echo $arResult["arUser"]["PERSONAL_PHONE"] ?>"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-8">
                                    <div class="form-group">
                                        <input type="text" title="<?
                                        $rsType = CUserFieldEnum::GetList(array(), array(
                                            "ID" => $arResult["arUser"]["UF_ACADEMIC"],
                                        ));
                                        if ($arType = $rsType->GetNext()) echo $arType["VALUE"];

                                        ?>" class="form-control form-control-lg" name="WORK_POSITION111"
                                               placeholder="<?= $msg['edu'] ?>" disabled maxlength="255"
                                               autocomplete="off" style="color: #969696; font-size: 1.4rem;"
                                               value="<?
                                               $rsType = CUserFieldEnum::GetList(array(), array(
                                                   "ID" => $arResult["arUser"]["UF_ACADEMIC"],
                                               ));
                                               if ($arType = $rsType->GetNext()) echo $arType["VALUE"];

                                               ?>"/>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="PERSONAL_CITY"
                                               placeholder="<?= $msg['country'] ?>" required maxlength="50"
                                               value="<? echo $arResult["arUser"]["PERSONAL_CITY"] ?>"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-lg" name="EMAIL"
                                               placeholder="E-mail" disabled maxlength="50"
                                               value="<? echo $arResult["arUser"]["EMAIL"] ?>"
                                               style="color: #969696;"/>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" name="save"
                                                value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD")) ?>"
                                                class="btn btn-primary btn-lg btn-block"><?= $msg['save_changes'] ?>
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                <? }; ?>
                <div class="pt-4"><h5 class="mb-4"><?= $msg['change_pass'] ?></h5>
                    <form method="post" name="form3" action="<?= $arResult["FORM_TARGET"] ?>"
                          enctype="multipart/form-data">
                        <?= $arResult["BX_SESSION_CHECK"] ?>
                        <input type="hidden" name="lang" value="<?= LANG ?>"/>
                        <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>"/>
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <input type="password"
                                           class="form-control form-control-lg"
                                           placeholder="<?= $msg['new_password'] ?>*"
                                           name="NEW_PASSWORD"
                                           minlength="6"
                                           value=""
                                           autocomplete="off"
                                           id="password"
                                    />
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <input type="password"
                                           class="form-control form-control-lg"
                                           name="NEW_PASSWORD_CONFIRM"
                                           placeholder="<?= $msg['new_password_again'] ?>*"
                                           maxlength="50"
                                           value=""
                                           minlength="6"
                                           autocomplete="off"
                                           id="confirm_password"
                                    /></div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    <button type="submit" name="save"
                                            value="<?= (($arResult["ID"] > 0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD")) ?>"
                                            class="btn btn-primary btn-lg btn-block"><?= $msg['save_changes'] ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="application/javascript">
    var password = document.getElementById("password")
        , confirm_password = document.getElementById("confirm_password");

    function validatePassword() {
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("<?=$msg['password_not_match']?>");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

    var media_input = document.getElementById("media-input");
    var check_media_input = document.getElementById("check-media-input");

    function changeStatus() {
        if (check_media_input.checked == false) {
            media_input.value = "Нет";
            check_media_input.setAttribute("checked", "true");
        } else {
            media_input.value = "Да";
            check_media_input.setAttribute("checked", "false");
        }
        ;

        document.getElementById('save-media-status').click();
        console.log(check_media_input.checked);
    }


</script>

