<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['on_main'] = 'Back to main page';
    $msg['login'] = 'Login';
    $msg['repair'] = 'Password recovery';
    $msg['repair_mess'] = 'Forgot your password? Enter your login or e-mail.<br />The password change instructions and your registration data will be sent to your e-mail.';
    $msg['send_repair_link'] = 'Get repair link';
    $msg['lost_link'] = 'Perhaps Your email with the activation link is lost. Enter your e-mail so we can send it again.';

} else {
    $msg['on_main'] = 'На главную';
    $msg['login'] = 'Авторизоваться';
    $msg['repair'] = 'Восстановление доступа';
    $msg['repair_mess'] = 'Если вы забыли пароль, введите E-Mail.<br />Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы указанный адрес.';
    $msg['send_repair_link'] = 'Восстановить';
    $msg['lost_link'] = 'Возможно Ваше письмо со ссылкой для активации потерялось. Введите свой e-mail чтобы мы выслали её заново.';

};


?>

<nav class="navbar pt-5 flex-shrink-0">
    <div class="container">
        <div></div>
        <div>
            <a href="<?if (strpos($page, 'en/')) echo '/en';?>/" title="<?=$msg['on_main']?>"> <div class="logo"></div></a>
        </div>
        <div></div>
    </div>
</nav>
<div class="flex-grow-1 d-flex flex-column justify-content-center pb-3">
    <div class="container">
        <h1 class="text-center mb-5"><?=$msg['repair']?></h1>
        <div class="row">
            <div class="col col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12 offset-0">
                <div class="p-5 bg-white shadow-sm mb-5">


                    <div class="px-sm-5 pt-5">

                        <? if ($arParams["~AUTH_RESULT"]) {
                            ShowMessage($arParams["~AUTH_RESULT"]);
                            ?>
                            <p>&nbsp;</p>
                        <? } elseif ($_GET["activation"] == "repair") { ?>
                            <p><?=$msg['lost_link']?></p>
                            <p>&nbsp;</p>
                        <? } else {
                            ?>

                            <p><?= $msg['repair_mess'] ?></p>

                            <p>&nbsp;</p>

                            <?
                        }
                        ?>
                        <form name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
                            <?
                            if (strlen($arResult["BACKURL"]) > 0) {
                                ?>
                                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                                <?
                            }
                            ?>
                            <input type="hidden" name="AUTH_FORM" value="Y">
                            <input type="hidden" name="TYPE" value="SEND_PWD">


                            <div class="form-group"><input class="form-control form-control-lg" autocomplete="email"
                                                           type="text" name="USER_EMAIL" maxlength="255"
                                                           placeholder="E-mail*"/></div>


                            <div class="form-group"><input class="btn btn-primary btn-lg btn-block" type="submit"
                                                           name="send_account_info"
                                                           value="<?= $msg['send_repair_link'] ?>"/></div>

                            <noindex>
                                <div class="text-right pt-1 mb-5">
                                    <a href="<?= $arResult["AUTH_AUTH_URL"] ?>"
                                       rel="nofollow"><?=$msg['login']?></a>
                                </div>
                            </noindex>


                        </form>
                        <script type="text/javascript">
                            document.bform.USER_LOGIN.focus();
                        </script>
                        <!--<p><? //echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>-->
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>






