<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['on_main'] = 'Back to main page';
    $msg['login'] = 'Login';
    $msg['repair'] = 'Password recovery';
    $msg['repair_mess'] = 'Forgot your password? Enter your login or e-mail.<br />The password change instructions and your registration data will be sent to your e-mail.';
    $msg['send_repair_link'] = 'Get repair link';
    $msg['lost_link'] = 'Perhaps Your email with the activation link is lost. Enter your e-mail so we can send it again.';
    $msg['control_code'] = 'Control code';
    $msg['new_password'] = 'New password';
    $msg['new_password_again'] = 'Repeat new password ';
    $msg['change_pass'] = 'Change password';

} else {
    $msg['on_main'] = 'На главную';
    $msg['login'] = 'Авторизоваться';
    $msg['repair'] = 'Восстановление доступа';
    $msg['repair_mess'] = 'Если вы забыли пароль, введите E-Mail.<br />Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы указанный адрес.';
    $msg['send_repair_link'] = 'Восстановить';
    $msg['lost_link'] = 'Возможно Ваше письмо со ссылкой для активации потерялось. Введите свой e-mail чтобы мы выслали её заново.';
    $msg['control_code'] = 'Контрольный код';
    $msg['new_password'] = 'Новый пароль';
    $msg['new_password_again'] = 'Новый пароль повторно';
    $msg['change_pass'] = 'Сменить пароль';

};


?>

<nav class="navbar pt-5 flex-shrink-0">
    <div class="container">
        <div></div>
        <div>
            <a href="<?if (strpos($page, 'en/')) echo '/en';?>/" title="<?=$msg['on_main']?>"> <div class="logo"></div></a>
        </div>
        <div></div>
    </div>
</nav>
<div class="flex-grow-1 d-flex flex-column justify-content-center pb-3">
    <div class="container">
        <h1 class="text-center mb-5"><?=$msg['repair']?></h1>
        <div class="row">
            <div class="col col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12 offset-0">
                <div class="p-5 bg-white shadow-sm mb-5">


                    <div class="px-sm-5 pt-5">

                        <? if ($arParams["AUTH_RESULT"]["TYPE"] == "OK"){ ShowMessage($arParams["~AUTH_RESULT"]);?>


                            <noindex>
                                <div class="text-right pt-1 mb-5">
                                    <a href="<?= $arResult["AUTH_AUTH_URL"] ?>"
                                       rel="nofollow"><?= $msg['login'] ?></a>
                                </div>
                            </noindex>

                        <?} else {?>
                        <?
                        ShowMessage($arParams["~AUTH_RESULT"]);
                        ?>


                        <form method="post" action="<?= $arResult["AUTH_FORM"] ?>" name="bform">
                            <? if (strlen($arResult["BACKURL"]) > 0): ?>
                                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                            <? endif ?>
                            <input type="hidden" name="AUTH_FORM" value="Y">
                            <input type="hidden" name="TYPE" value="CHANGE_PWD">


                            <div class="form-group">
                                <div class="input-fl not-empty">
                                    <input type="text"
                                           class="form-control form-control-lg "
                                           name="USER_LOGIN"
                                           id="USER_LOGIN"
                                           maxlength="50"
                                           value="<?= $arResult["LAST_LOGIN"] ?>"/>
                                    <label for="USER_LOGIN" class="f-label">E-mail*</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-fl not-empty">
                                    <input type="text"
                                           class="form-control form-control-lg "
                                           name="USER_CHECKWORD"
                                           id="USER_CHECKWORD"
                                           maxlength="50"
                                           value="<?= $arResult["USER_CHECKWORD"] ?>"/>
                                    <label for="USER_CHECKWORD" class="f-label"><?=$msg['control_code']?>*</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-fl <? if ($arResult["USER_PASSWORD"]) echo 'not-empty'; ?>">
                                    <input type="password"
                                           class="form-control form-control-lg"
                                           name="USER_PASSWORD"
                                           id="USER_PASSWORD"
                                           placeholder=""
                                           maxlength="50"
                                           value="<?= $arResult["USER_PASSWORD"] ?>"/>
                                    <label for="USER_PASSWORD" class="f-label"><?=$msg['new_password']?>*</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-fl <? if ($arResult["USER_CONFIRM_PASSWORD"]) echo 'not-empty'; ?>">
                                    <input type="password"
                                           class="form-control form-control-lg"
                                           name="USER_CONFIRM_PASSWORD"
                                           id="USER_CONFIRM_PASSWORD"
                                           placeholder=""
                                           maxlength="50"
                                           value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>"/>
                                    <label for="USER_CONFIRM_PASSWORD" class="f-label"><?=$msg['new_password_again']?>*</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <input class="btn btn-primary btn-lg btn-block"
                                       type="submit"
                                       name="change_pwd"
                                       value="<?= $msg['change_pass'] ?>"/>

                            </div>

                            <noindex>
                                <div class="text-right pt-1 mb-5">
                                    <a href="<?= $arResult["AUTH_AUTH_URL"] ?>"
                                       rel="nofollow"><?= $msg['login'] ?></a>
                                </div>
                            </noindex>


                        </form>
                        <script type="text/javascript">
                            document.bform.USER_LOGIN.focus();
                        </script>

                        </form>

                        <?};?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

