<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?//here you can place your own messages
	switch($arResult["MESSAGE_CODE"])
	{
	case "E01":
		?><? //When user not found
		break;
	case "E02":
		?><? //User was successfully authorized after confirmation
		break;
	case "E03":
		?><? //User already confirm his registration
		break;
	case "E04":
		?><? //Missed confirmation code
		break;
	case "E05":
		?><? //Confirmation code provided does not match stored one
		break;
	case "E06":
		?><? //Confirmation was successfull
		break;
	case "E07":
		?><? //Some error occured during confirmation
		break;
	}
?>


<?
$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['on_main'] = 'Back to main page';
    $msg['confirm'] = 'Registration confirmation';
    $msg['login'] = 'Login';


} else {
    $msg['on_main'] = 'На главную';
    $msg['confirm'] = 'Подтверждение регистрации';
    $msg['login'] = 'Войти';



};

?>

<nav class="navbar pt-5 flex-shrink-0">
    <div class="container">
        <div></div>
        <div>
            <a href="<?if (strpos($page, 'en/')) echo '/en';?>/" title="<?=$msg['on_main']?>"> <div class="logo"></div></a>
        </div>
        <div></div>
    </div>
</nav>
<div class="flex-grow-1 d-flex flex-column justify-content-center pb-3">
    <div class="container">


        <h1 class="text-center mb-5"><?=$msg['confirm']?></h1>
        <div class="row">
            <div class="col col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12 offset-0">
                <div class="p-5 bg-white shadow-sm mb-5">




                    <div class="px-sm-5 pt-5">
                        <?echo $arResult["MESSAGE_TEXT"]?>
                        <p>&nbsp;</p>
                        <div class="form-group">

                            <form target="_self" action="<?if (strpos($page, 'en/')) echo '/en';?>/login/">
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="<?=$msg['login']?>">
                            </form>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

