<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['on_main'] = 'Back to main page';
    $msg['login'] = 'Login';
    $msg['with_us'] = 'Not with us yet?';
    $msg['new_member'] = 'New registration';
    $msg['password'] = 'Password';
    $msg['forgot_pass'] = 'Forgot password?';
    $msg['restore'] = 'To restore access';

} else {
    $msg['on_main'] = 'На главную';
    $msg['login'] = 'Вход';
    $msg['with_us'] = 'Еще не с нами?';
    $msg['new_member'] = 'Регистрация нового участника';
    $msg['password'] = 'Пароль';
    $msg['forgot_pass'] = 'Забыли пароль?';
    $msg['restore'] = 'Восстановить доступ';
};


?>

<nav class="navbar pt-5 flex-shrink-0">
    <div class="container">
        <div></div>
        <div>
            <a href="<?if (strpos($page, 'en/')) echo '/en';?>/" title="<?=$msg['on_main'];?>"><div class="logo"></div></a>
        </div>
        <div></div>
    </div>
</nav>
<div class="flex-grow-1 d-flex flex-column justify-content-center pb-3">
    <div class="container">
        <h1 class="text-center mb-5"><?=$msg['login'];?></h1>
        <div class="row">
            <div class="col col-lg-6 offset-lg-3 col-md-10 offset-md-1 col-12 offset-0">
                <div class="p-5 bg-white shadow-sm mb-5">
                    <div class="px-sm-4 pt-3">

                        <form name="form_auth" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">

                            <?
                            //ShowMessage($arParams["~AUTH_RESULT"]);
                            //ShowMessage($arResult['ERROR_MESSAGE']);

                            ?>

                            <input type="hidden" name="AUTH_FORM" value="Y"/>
                            <input type="hidden" name="TYPE" value="AUTH"/>
                            <? if (strlen($arResult["BACKURL"]) > 0): ?>
                                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                            <? endif ?>
                            <?
                            foreach ($arResult["POST"] as $key => $value) {
                                ?>
                                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
                                <?
                            }
                            ?>

                            <!--<noindex>
                            <div class="text-center pt-4 mb-5"><?/*=$msg['with_us'];*/?> <a
                                        href="<?/*if (strpos($page, 'en/')) echo '/en';*/?><?/*= $arResult["AUTH_REGISTER_URL"];*/?>" rel="nofollow"><?/*=$msg['new_member'];*/?></a>
                            </div>
                            </noindex>-->
                            <div class="form-group">
                                <div class="input-fl">
                                <input type="email"
                                       id="email"
                                       autocomplete="email"
                                       name="USER_LOGIN"
                                       class="form-control <? if ($arParams["AUTH_RESULT"]["TYPE"]== "ERROR") echo "is-invalid";?>"
                                       placeholder=""
                                       maxlength="50"
                                       value="<?//= $arResult["LAST_LOGIN"] ?>">
                                    <label for="email" class="f-label">E-mail</label>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-fl">
                                <input type="password"
                                       id="password"
                                       name="USER_PASSWORD"
                                       maxlength="50"
                                       minlength="6"
                                       class="form-control <? if ($arParams["AUTH_RESULT"]["TYPE"]== "ERROR") echo "is-invalid";?>"
                                       placeholder="">
                                    <label for="password" class="f-label"><?=$msg['password']?></label>
                                    <? if ($arParams["AUTH_RESULT"]["TYPE"] == "ERROR"){?>
                                        <div id="invalid-password" class="invalid-feedback"><?ShowMessage($arParams["~AUTH_RESULT"]);?></div>

                                    <?}?>
                                </div>
                            </div>
                            <noindex>
                            <div class="text-right pt-1 mb-5"><?=$msg['forgot_pass'];?> <a
                                        href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"
                                        rel="nofollow"><?=$msg['restore']?></a>
                            </div>
                            </noindex>
                            <div class="form-group">
                                <button type="submit" name="Login" class="btn btn-primary btn-lg btn-block"><?=$msg['login']?>
                                </button>
                            </div>

                        </form>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

