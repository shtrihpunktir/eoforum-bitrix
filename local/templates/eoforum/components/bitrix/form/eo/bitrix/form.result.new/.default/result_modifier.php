<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (is_array($arResult["FORM_ERRORS"])){
    foreach($arResult["FORM_ERRORS"] as $FIELD_SID => $BITRIX_ERROR_TEXT){
        $arQuestion = $arResult["QUESTIONS"][$FIELD_SID];
        if(strpos($arQuestion['HTML'],'class') !== false){
            $arQuestion['HTML'] = str_replace('class="','class="error ',$arQuestion['HTML']);
        }
        else{
            $arQuestion['HTML'] = str_replace('name=',' class="error" name=',$arQuestion['HTML']);
        }
        $arResult["QUESTIONS"][$FIELD_SID] = $arQuestion;
    }
}
?>

