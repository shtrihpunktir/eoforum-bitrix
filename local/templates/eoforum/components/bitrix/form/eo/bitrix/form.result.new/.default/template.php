<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<?

/*
global $USER;
$FORM_ID = 1;
$user_id = $USER->GetID();

$checkFields[] = array(
    "USER_ID" => $user_id,                  // код поля по которому фильтруем
    "EXACT_MATCH" => "Y"                              // ищем точное совпадение
);


$checkFilter = array(
    "FIELDS" => $arFields
);
*/
// получаем последнюю заявку пользователя
/*
$checkResults = CFormResult::GetList(
    $FORM_ID,
    $by = "s_id",
    $order = "desc",
    $checkFilter,
    $is_filtered,
    "Y",
    10);
*/
$status = 0;
// "$status = 0" если заявок от пользователя небыло
// "$status = 1" если последняя заявка в статусе NEW
// "$status = 2" если последняя заявка в статусе ACCEPTED
// "$status = 3" если последняя заявка в статусе REJECTED

/*
while ($checkResult = $checkResults->Fetch()) {
    //echo "<pre>";
    //print_r($checkResult);
    //echo "</pre>";
    switch ($checkResult["STATUS_TITLE"]) {
        case "NEW":
            $status = 1;
            break;
        case "ACCEPTED":
            $status = 2;
            break;
        case  "REJECTED":
            $status = 3;
            break;
        default:
            $status = 0;
    }
}
*/

$page = $APPLICATION->GetCurPage();
$msg = array();
if (strpos($page, 'en/')) {
    $msg['on_main'] = 'Back to main page';
    $msg['main'] = 'Main page';
    $msg['profile'] = 'Personal account';
    $msg['request'] = 'Speaker\'s application';
    $msg['back'] = 'Back';
    $msg['rules'] = 'Application rules:';
    $msg['rules_1'] = 'Applications are accepted until may 13, 2019 inclusive.';
    $msg['rules_2'] = 'Notification of acceptance or rejection of the application is sent by the organizing Committee of EOF 2019 to the e-mail specified in the application no later than June 3, 2019.';
    $msg['rules_3'] = 'The organizing Committee reserves the right not to include the declared lecture in the scientific program of EOF 2019 without explanation.';
    $msg['form'] = 'Application form';
    $msg['ps'] = 'Please note that the collection of abstracts of the Eurasian orthopedic forum will not be accepted and printed.';


} else {
    $msg['on_main'] = 'На главную';
    $msg['main'] = 'Главная';
    $msg['profile'] = 'Личный кабинет';
    $msg['request'] = 'Заявка на доклад';
    $msg['back'] = 'Назад';
    $msg['rules'] = 'Правила подачи заявки на доклад:';
    $msg['rules_1'] = 'Заявки на доклад принимаются до 13 мая 2019 года включительно.';
    $msg['rules_2'] = 'Уведомление о приеме или отклонении заявки отправляется организационным комитетом ЕОФ 2019 на почту, указанную в заявке, не позднее 3 июня 2019 года.';
    $msg['rules_3'] = 'Организационный комитет оставляет за собой право не включать заявленный доклад в научную программу ЕОФ 2019 без объяснения причин.';
    $msg['form'] = 'Форма заявки';
    $msg['ps'] = 'Обращаем Ваше внимание на то, что прием и печать сборника тезисов Евразийского ортопедического форума осуществляться не будет.';

};
?>


<nav class="navbar pt-5 mb-5">
    <div class="container">
        <div>
            <a href="<? if (strpos($page, 'en/')) echo '/en'; ?>/" title="<?= $msg['on_main']; ?>">
                <div class="logo"></div>
            </a>
        </div>
    </div>
</nav>
<div class="container">
    <div>
        <div class="breadcrumb mb-5">
            <li class="breadcrumb-item"><a href="<? if (strpos($page, 'en/')) echo '/en'; ?>/"><?= $msg['main'] ?></a>
            </li>
            <li class="breadcrumb-item"><a
                        href="<? if (strpos($page, 'en/')) echo '/en'; ?>/profile"><?= $msg['profile'] ?></a></li>
            <li class="breadcrumb-item active"><?= $msg['request'] ?></li>
        </div>
    </div>
    <div class="d-flex mb-4">
        <div class="pr-5 pt-1"><a href="<? if (strpos($page, 'en/')) echo '/en'; ?>/profile"
                                  class="btn btn-outline-secondary"><i
                        class="fa fa-long-arrow-left"></i>
                &nbsp; <?= $msg['back'] ?></a></div>
        <h1><?= $msg['request'] ?></h1></div>
    <div class="pt-5 pb-2">
        <div class="row">
            <? if ($status == 0 || $status == 3) { ?>
                <div class="col col-lg-8 offset-lg-2 col-md-12 offset-md-0 col-12 offset-0">
                    <div class="p-5 bg-white shadow-sm mb-5">
                        <div class="px-sm-5 pt-3">
                            <p style="text-align: center; font-weight: 600;"><?= $msg['rules'] ?></p>
                            <ol>
                                <li><?= $msg['rules_1'] ?></li>
                                <li><?= $msg['rules_2'] ?></li>
                                <li><?= $msg['rules_3'] ?></li>
                            </ol>
                            <p>&nbsp;</p>
                            <p style="text-align: center; font-weight: 600;"><?= $msg['form'] ?></p>
                            <p>&nbsp;</p>


                            <? if ($arResult["isFormErrors"] == "Y"): ?><? //= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>


                            <? if (strpos($page, 'en/')) { ?>

                                <form name="SIMPLE_FORM_1" action="/en/registration-of-speakers/" method="POST"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="sessid" id="sessid"
                                                                           value="<?=str_replace('sessid=', '' , bitrix_sessid_get()); ?>"><input
                                            type="hidden" name="WEB_FORM_ID" value="1">
                                    <div class="form-group ">
                                        <input type="text" placeholder="Full name*" class="form-control form-control-lg"
                                               name="form_text_1" value="" size="0"></div>


                                    <div class="form-group ">
                                        <input type="text" placeholder="Organization, position*" class="form-control form-control-lg"
                                               name="form_text_6" value="" size="0"></div>


                                    <div class="form-group ">
                                        <input type="text" placeholder="Phone number*"
                                               class="form-control form-control-lg"
                                               name="form_text_4" value="" size="0"></div>


                                    <div class="form-group ">
                                        <input type="text" placeholder="E-mail*"
                                               class="form-control form-control-lg" name="form_email_3" value=""
                                               size="0">
                                    </div>


                                    <div class="form-group ">
                                        <input type="text"
                                               placeholder="Section of the scientific program of the EOF, to which the lecture is submitted*"
                                               class="form-control form-control-lg" name="form_text_2" value=""
                                               size="0">
                                    </div>


                                    <div class="form-group ">
                                        <input type="text" placeholder="Title of the lecture*"
                                               class="form-control form-control-lg" name="form_text_7" value=""
                                               size="0">
                                    </div>


                                    <div class="form-group ">
                                    <textarea name="form_textarea_9" cols="40" rows="5"
                                              placeholder="Theses (not more than 500 characters including spaces)*"
                                              class="form-control form-control-lg" maxlength="500"></textarea></div>


                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="10"
                                                   name="form_checkbox_AGREEMENT[]" value="10">
                                            <label class="custom-control-label" for="10">I agree to the processing of my personal data</label>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="12"
                                                   name="form_checkbox_AGREEMENT_2[]" value="12">
                                            <label class="custom-control-label" for="12">I hereby confirm that I have read and agree to the  <a href="#">terms of the policy</a></label>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input class="btn btn-primary btn-lg btn-block" type="submit"
                                               name="web_form_submit"
                                                 <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?>
                                               value="Send">
                                    </div>
                                </form>

                            <? } else { ?>
                                <?= $arResult["FORM_HEADER"] ?>
                                <? foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                                    ?>

                                    <? if ($arQuestion["STRUCTURE"][0]["FIELD_TYPE"] == "checkbox") { ?>
                                        <div class="form-group ">
                                            <div class="custom-control custom-checkbox <? if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])): ?> error <? endif; ?>">
                                                <input type="checkbox"
                                                       class="custom-control-input"
                                                       id="<?= $arQuestion["STRUCTURE"][0]["ID"] ?>"
                                                       name="form_checkbox_<?= $arQuestion["STRUCTURE"][0]["VALUE"] ?>[]"
                                                       value="<?= $arQuestion["STRUCTURE"][0]["ID"] ?>">
                                                <label class="custom-control-label"
                                                       for="<?= $arQuestion["STRUCTURE"][0]["ID"] ?>"><?= $arQuestion["STRUCTURE"][0]["MESSAGE"] ?></label>

                                            </div>
                                        </div>
                                    <? } else { ?>
                                        <div class="form-group <? if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])): ?> error <? endif; ?>">
                                            <?= $arQuestion["HTML_CODE"] ?>
                                        </div>
                                    <? }; ?>

                                    <?
                                } //end foreach
                                ?>


                                <div class="form-group">
                                    <input class="btn btn-primary btn-lg btn-block" <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?>
                                           type="submit" name="web_form_submit"
                                           value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"/>
                                    <?= $arResult["FORM_FOOTER"] ?>
                                </div>

                            <? }; ?>

                            <p style="font-weight: 600; font-style: italic; text-align: center;"><?= $msg['ps']; ?></p>
                        </div>
                    </div>
                </div>
            <? } elseif ($status == 1) { ?>
                <p>Ваша заявка получена и находится на рассмотрении.</p>
            <? } elseif ($status == 2) { ?>
                <p>Поздравляем! Ваша заявка принята.</p>
            <? }; ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.form-group').click(function () {
            $(this).removeClass('error');
        });
    });
    $(document).ready(function () {
        $('.form-group').click(function () {
            $(this).children('.custom-control').removeClass('error');
        });
    });
</script>